/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./routes";

const AppRouter = () => {

  
  const routesComponent = routes.map((route, idx) => (
    <Route exact {...route} key={idx} />
  ));


  return (
    <Router>
      <Switch>{routesComponent}</Switch>
    </Router>
  );
};
export default AppRouter;