import React from "react";

const Dashboard = React.lazy(() => import("../../components/pages/dashboard"));
const Login = React.lazy(() => import("../../components/auth/Login/Login1"));

const listesDesContribuables = React.lazy(() =>
  import("../../components/pages/ListeDesContribuables/listesDesContribuables")
);
const DetailDeLexpertComptable = React.lazy(() =>
  import("../../components/pages/DetailDeLexpertComptable")
);
const AjouteUnExpertComptable = React.lazy(() =>
  import("../../components/pages/ajouteExpertComptable/AjouteUnExpertComptable")
);
const LandingPage = React.lazy(() =>
  import("../../components/pages/landing_page/LandingPage")
);
const NousContacter = React.lazy(() =>
  import("../../components/layouts/NousContacter")
);
const ChriffreDaffaire = React.lazy(() =>
  import("../../components/pages/chriffreDaffaire/ChriffreDaffaire")
);
const DetailDuContribuable = React.lazy(() =>
  import("../../components/pages/DetailDuContribuable")
);
const Profile = React.lazy(() => import("../../components/pages/Profile"));

const ListeDesExpertsComptables = React.lazy(() =>
  import("../../components/pages/ListeDesExpertsComptables")
);

const GestionDesAcces = React.lazy(() =>
  import("../../components/pages/gestion des acces/GestionDesAcces")
);

const GestionDesAccesTable = React.lazy(() =>
  import("../../components/pages/gestion des acces/GestionDesAccesTable")
);

const ContribuableDashboard = React.lazy(() =>
  import("../../components/pages/DashboardContribuable/ContribuableDashboard")
);

const CommissariatDashboard = React.lazy(() =>
  import(
    "../../components/pages/DashboardSeuilDeCommissariat/CommissariatDashboard"
  )
);

const ListeDesImportationsDashboard = React.lazy(() =>
  import(
    "../../components/pages/dashboardListeDesImportations/ListeDesImportationsDashboard"
  )
);

const ModificationDesImport = React.lazy(() =>
  import(
    "../../components/pages/dashboardListeDesImportations/ModificationDesImport/ModificationDesImport"
  )
);

const ListeDesAttributions = React.lazy(() =>
  import(
    "../../components/pages/ListeDesAttributionDashboard/ListeDesAttribution"
  )
);

const ModifierUnContribuable = React.lazy(() =>
  import("../../components/pages/ModifierUnContribuable")
);

const ModificationDesCampagnes = React.lazy(() =>
  import(
    "../../components/pages/GestionDesCampagne/ModificationDesCampagnes/ModificationDesCampagnes"
  )
);
const AjouteUneCampagne = React.lazy(() =>
  import(
    "../../components/pages/GestionDesCampagne/AjouteUneCampagne/AjouteUneCampagne"
  )
);
const AjouterUneDirection = React.lazy(() =>
  import("../../components/pages/GestionDesDirections/AjouterUneDirection")
);
const ModifierUneDirection = React.lazy(() =>
  import("../../components/pages/GestionDesDirections/ModifierUneDirection")
);
const ListeDesDirections = React.lazy(() =>
  import("../../components/pages/GestionDesDirections/ListeDesDirection")
);
const ListeDesOffresVendues = React.lazy(() =>
  import("../../components/pages/GestionDesServices/ListeDesOffresVendues")
);

const GestionDesCampagneDash = React.lazy(() =>
  import("../../components/pages/GestionDesCampagne/GestionDesCampagneDash")
);

const ListeDesArbitrage = React.lazy(() =>
  import("../../components/pages/ListeDesArbitrage")
);
const ListesDesServices = React.lazy(()=>import("../../components/pages/GestionDesServices/ListeDesServices"));
const ListeDesOffres =React.lazy(()=>import("../../components/pages/GestionDesServices/ListeDesOffres"));
const ListeDesExpertsComptablesAbonne=React.lazy(()=>import("../../components/pages/GestionDesServices/ListeDesExpertsComptablesAbonne/listeDesExpertsComptablesAbonne"))
const GestionDesPaiements =React.lazy(()=>import ("../../components/pages/DashboardDesPaiements/GestionDesPaiements"))
const Historique =React.lazy(()=>import("../../components/pages/DashboardDesPaiements/Historique"))
const ResolutionDuConflit= React.lazy(()=>import("../../components/pages/ResolutionDuConflit"))
const GestionDesArbitrages = React.lazy(()=>import("../../components/pages/gestionDesArbitrages/GestionDesArbitrages"))
const StatistiquesContribuables = React.lazy(()=>import("../../components/pages/Statistiques/StatistiquesContribuables"))
const AjouterUneOffre =React.lazy(()=>import("../../components/pages/AjouterUneOffre"))
const Facture =React.lazy(()=>import("../../components/pages/GestionDesServices/facture/Facture"))
const RecuperationPassword = React.lazy(()=>import("../../components/layouts/RecuperationPassword"))

const routes = [
  {
    path: "/",
    component: LandingPage,
  },
  {
    path: "/Login",
    component: Login,
  },
  {
    path: "/nous-contacter",
    component: NousContacter,
  },
  {
    path: "/tableau-de-bord",
    component: Dashboard,
  },
  {
    path: "/liste-des-contibuables",
    component: listesDesContribuables,
  },
  {
    path: "/details-expert-comptable",
    component: DetailDeLexpertComptable,
  },
  {
    path: "/ajoute-expert-comptable",
    component: AjouteUnExpertComptable,
  },
  {
    path: "/chiffre-daffaire",
    component: ChriffreDaffaire,
  },
  {
    path: "/details-du-contribuable",
    component: DetailDuContribuable,
  },
  {
    path: "/profil",
    component: Profile,
  },
  {
    path: "/liste-des-experts-comptables",
    component: ListeDesExpertsComptables,
  },
  {
    path: "/gestion-des-acces",
    component: GestionDesAcces,
  },
  {
    path: "/gestion-des-acces-table",
    component: GestionDesAccesTable,
  },
  {
    path: "/tableau-de-bord-contribuable",
    component: ContribuableDashboard,
  },
  {
    path: "/tableau-de-bord-commissariat",
    component: CommissariatDashboard,
  },
  {
    path: "/tableau-de-bord-liste-des-importations",
    component: ListeDesImportationsDashboard,
  },
  {
    path: "/modification-des-imports",
    component: ModificationDesImport,
  },
  {
    path: "/Liste-des-attributions",
    component: ListeDesAttributions,
  },
  {
    path: "/modifier-un-contribuable",
    component: ModifierUnContribuable,
  },
  {
    path: "/modification-des-campagnes",
    component: ModificationDesCampagnes,
  },
  {
    path: "/Ajoute-de-campagne",
    component: AjouteUneCampagne,
  },
  {
    path: "/ajoute-de-direction",
    component: AjouterUneDirection,
  },
  {
    path: "/modifier-une-direction",
    component: ModifierUneDirection,
  },
  {
    path: "/Liste-des-directions",
    component: ListeDesDirections,
  },
  {
    path: "/Liste-des-offres-vendues",
    component: ListeDesOffresVendues,
  },
  {
    path: "/tableau-de-bord-de-gestion-des-campagnes",
    component: GestionDesCampagneDash,
  },
  {
    path: "/Liste-des-arbitrages",
    component: ListeDesArbitrage,
  },
  {
    path:"/tableau-de-bord-gestion-de service",
    component:ListesDesServices
  },
  {
    path:"/liste-des-offres",
    component:ListeDesOffres
  },
  {
    path:"/liste-des-experts-comptables-abonnes",
    component:ListeDesExpertsComptablesAbonne
  },
  {
    path:"/tableau-de-bord-de-gestion-de-paiement",
    component:GestionDesPaiements
  },
  {
    path:'/tableau-de-bord-de-gestion-de-paiement/histotique',
    component:Historique
  },
  {
    path:"/resolution-de-conflit",
    component:ResolutionDuConflit
  },
  {
    path:"/gestion-des-arbitrages",
    component:GestionDesArbitrages
  },
  {
    path:"/statistiques",
    component:StatistiquesContribuables
  },
  {
    path:"/ajoute-une-offre",
    component:AjouterUneOffre
  },
  {
    path:"/facture",
    component:Facture
  },
  {
    path:"/recuperation",
    component:RecuperationPassword
  }
];
export default routes;
