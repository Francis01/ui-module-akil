import { red } from "@material-ui/core/colors";

export const DataChartBar = {
  labels: [2019, 2018, 2016, 2015, 2014, 2013, 2012],

  datasets: [
    {
      label: "",
      backgroundColor: "#FF7A42",
      borderWidth: 1,
      stack: 1 /* 
        barPercentage: 0.5, */,
      barThickness: 18,
      maxBarThickness: 50,
      minBarLength: 0,

      data: [35, 37, 22, 73, 63, 35, 54, 67],
    },
    {
      label: "",
      backgroundColor: "#FF7A4235",
      borderWidth: 1,
      stack: 1 /* 
        barPercentage: 50, */,
      barThickness: 18,
      maxBarThickness: 50,
      minBarLength: 0,
      data: [89, 89, 89, 89, 89, 89, 89, 89],
    },
  ],

  DataChartBarOption: {
    title: {
      display: true,
      text: `Volume d\'affaire au format SMT 2000 Contribuable`,
      fontSize: 15,
      position: "top",
    },
    legend: {
      display: false,
    },

    scales: {
      yAxes: [
        {
          display: false,
        },
      ],
    },
  },
};

export const DataChartBarExpertComptable = {
  labels: [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
  ],

  datasets: [
    {
      label: "2017",
      backgroundColor: "#3CC9AF",
      
      borderWidth: 1,
      stack: 1 /* 
        barPercentage: 0.5, */,
      barThickness: 18,
      maxBarThickness: 50,
      minBarLength: 0,

      data: [
        35,
        37,
        22,
        73,
        63,
        35,
        54,
        67,
        35,
        37,
        22,
        73,
        63,
        35,
        54,
        67,
        54,
        67,
      ],
    },
    {
      label: "2018",
      backgroundColor: "#4339F2",
      borderWidth: 1,
      stack: 1 /* 
        barPercentage: 50, */,
      barThickness: 18,
      maxBarThickness: 50,
      minBarLength: 0,
      data: [
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        89,
        54,
        67,
      ],
    },
    {
      label: "2019",
      backgroundColor: "#FFB200",
      borderWidth: 1,
      stack: 1 /* 
        barPercentage: 50, */,
      barThickness: 18,
      maxBarThickness: 50,
      minBarLength: 0,
      data: [
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        40,
        54,
        67,
      ],
    },
  ],

  DataChartBarExpertComptableOption: {
    title: {
      display: false,
      text: "Chriffre d'affaire de la profession entre 2017-2019",
      fontSize: 15,
    },
    legend: {
      display: false,
      labels: {
        usePointStyle: true,
      },
    },
    scales: {
      yAxes: [
        {
          display: true,
          gridLines: {
            drawOnChartArea: false,
          },
        },
      ],
      xAxes: [
        {
          display: true,
          gridLines: {
            drawOnChartArea: false,
          },
        },
      ],
    },
  },
};

export const DataChartLine = {
  labels: [2019, 2018, 2016, 2015, 2014, 2013, 2012],

  datasets: [
    {
      label: "Liasses transmises",
      background:
        "transparent linear-gradient(180deg, #4AAC8A 0%, #1FB1B11D 100%) 0% 0% no-repeat padding-box",
      opacity: 1,
      borderWidth: 1,
      pointRadius: 0,
      borderCapStyle: "butt",
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointHitRadius: 10,
      borderColor: "#0B6913",

      stack: 1,
      data: [400, 600, 809, 500, 700, 900, 1000],
    },
    {
      label: "Liasses rejetées",
      backgroundColor: "",
      borderCapStyle: "butt",
      borderColor: "#FF7A42",
      pointRadius: 0,
      borderWidth: 1,
      stack: 1,
      data: [500, 1000, 1500, 900, 1700, 1000, 700],
    },
  ],
  DataChartLineOption: {
    title: {
      display: true,
      text: "Transmission des liasses",
      fontSize: 20,
      position: "left",
    },

    legend: {
      display: true,
      position: "top",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMin: 500,
            suggestedMax: 2000,
          },
          gridLines: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          gridLines: {
            display: false,
          },
        },
      ],
    },
  },
};

export const DataChartPie = {
  labels: [
    "Assurance vie",
    "Système bancaire",
    "Assurance non vie",
    "OHADA",
    "Autres",
  ],

  datasets: [
    {
      backgroundColor: ["#FF6F00", "#399C8F", "#F4CD25", "#8A1927", "#17C5CC"],
      borderWidth: 0.5,
      stack: 1,
      data: [15, 25, 10, 15, 35],

    },
  ],
  DataChartPieOption: {
    title: {
      display: true,
      text: "Répartition des contribuables par référentiel",
      fontSize: 15,
    },
    legend: {
      display: true,
      position: "right",
      
      
    },
    
  },
};



export const DataChartPieNombreDeContribuable = {
  labels: [
    "Région 1",
    "Région 2",
    "Région 3",
  ],

  datasets: [
    {
      backgroundColor: ["#FFB200", "#FF3A29", "#4339F2"],
      borderWidth: 0.5,
      stack: 1,
      data: [35, 30, 35],

    },
  ],
  DataChartPieOptionNombreDeContribuable: {
    title: {
      display: true,
      text: "Nombre de contribuables par direction régionale",
      fontSize: 15,
    },
    legend: {
      display: true,
      position: "right",
      
      
    },
    
  },
};





export const DataChartPieNombreDeContribuableParDirectionRegionale = {
  labels: [
    "Région 1",
    "Région 2",
    "Région 3",
  ],

  datasets: [
    {
      backgroundColor: ["#FF7A42", "#319441", "#0C88BF"],
      borderWidth: 0.5,
      stack: 1,
      data: [35, 30, 35],

    },
  ],
  DataChartPieOptionNombreDeContribuableParDirectionRegionale: {
    title: {
      display: false,
      text: "Nombre de contribuables par direction régionale",
      fontSize: 15,
    },
    legend: {
      display: false,
      position: "bottom",
      
      
    },
    
  },
};





export const DataChartPieNombreEtatFinancier = {
  labels: [
    "Région 1",
    "Région 2",
    "Région 3",
    "Région 2",
    "Région 3",
  ],

  datasets: [
    {
      backgroundColor: ["#319441", "#F4CD25", "#8A1927","#0C88BF","#FF6F00"],
      borderWidth: 0.5,
      stack: 5,
      data: [30, 10, 25,30,5],

    },
  ],
  DataChartPieOptionDataChartPieNombreEtatFinancier: {
    title: {
      display: true,
      text: "Nombre d'états financiers déposé par direction régionale",
      fontSize: 15,
    },
    legend: {
      display: true,
      position: "right",
      labels: {
        usePointStyle: true,
      },
      
      
    },
    
  },
};






export const DataChartPieDirectionRegional = {
  labels: [
    "Région 1",
    "Région 2",
    "Région 3",
    "Région 2",
    "Région 3",
  ],

  datasets: [
    {
      backgroundColor: ["#319441", "#F4CD25", "#8A1927","#0C88BF","#FF6F00"],
      borderWidth: 0.5,
      stack: 1,
      data: [30, 10, 25,30,5],

    },
  ],
  DataChartPieOptionDirectionRegional: {
    title: {
      display: true,
      text: "Nombre d'états financiers non déposé par direction régionale",
      fontSize: 15,
    },
    legend: {
      display: true,
      position: "right",
      labels: {
        usePointStyle: true,
      },
      
    }, 
  },
};




export const DataChartPie2 = {
  labels: ["Impôt synthetique", "Réel", "Réel normal"],

  datasets: [
    {
      backgroundColor: ["#FF7A42", "#A3C936", "#267D3A"],
      borderWidth: 0.5,

      labels: {
        enabled: false,
      },
      stack: 1,
      data: [25, 35, 45],
    },
  ],
  DataChartPie2Option: {
    title: {
      display: true,
      text: "Répartition par régime fiscale",
      fontSize: 15,
    },
    legend: {
      display: true,
      position: "right",
    },
  },
};

export const DataChartPie3 = {
  labels: ["Régime IS"],

  datasets: [
    {
      backgroundColor: "#34B53A",
      color: "#fff",
      borderWidth: 6,
      data: [67],
    },
  ],
  DataChartPie3Option: {
    /* title: {
            display: false,
            text: 'Répartition des contribuable par referentiel',
            fontSize: 15,
        }, */
    legend: {
      display: true,
      position: "bottom",
    },
  },
};
