import "./App.css";
import AppRouter from "./@core/router/router";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Suspense } from "react";
import { ThemeProvider } from "@material-ui/core/styles";

function App() {
  return (
    <>
      <ThemeProvider>
        <Suspense
          fallback={<div className="loading">{<CircularProgress />}</div>}
        >
          <AppRouter />
        </Suspense>
      </ThemeProvider>
    </>
  );
}

export default App;
