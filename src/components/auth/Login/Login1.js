import React from "react";
import "./Login1.css";
import image from "../../../assert/Groupe de masques 6.png";
import { Checkbox,Button, FormControlLabel, Grid, IconButton, InputAdornment, TextField } from "@material-ui/core";
import {Link} from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Boutton from "../../layouts/boutton/Boutton";
import {UseStylesBoutton} from "../../layouts/boutton/Boutton.style"

function Login1() {
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <div className="containerRoot">
      <div className="block1">
        <div className="image">
          <img src={image} className="img-fluid" />
        </div>
        <div className="content">
          <div className="textContent">
            <div className="content1">Ensemble construisons l'innovation</div>

            <div className="content2">
              <small
                style={{ font: "normal normal 300 34px/45px Roboto Slab",fontSize:"18px " }}
              >
                au cœur du métier{" "}
              </small>{" "}
              <br />{" "}
              <strong style={{ fontWeight: "bold" }}>
                de l'expert-comptable
              </strong>
            </div>
          </div>
        </div>
      </div>

      <div className="block2">
        <div className="bouton">
          
            <Link
              to="/"
              style={{ textDecorationLine: "none", color: "#fff",textTransform:"none" }}
            >
              <Boutton name="Retour à l’accueil" style={UseStylesBoutton().orangeBtn2NoBorder} startIcon={<ArrowBackIcon/>}/>
            </Link>
        </div>
        <div className="form-container">
          <form className="form" noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              id="Email"
              label="Email"
              name="email"
              autoComplete="email"
              autoFocus
              className="TextField"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="password"
              label="Mot de passe"
              onChange={handleChange("password")}
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              id="Mot de passe"
              autoComplete="current-password"
              className="TextField"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
            <FormControlLabel
              style={{ color: "#707070" }}
              control={
                <Checkbox
                  value="remember"
                  style={{ color: "#FF7A42", textColor: "#FF7A42" }}
                />
              }
              label="Se souvenir de moi"
            />
            <Boutton name="Connexion" style={UseStylesBoutton().greenBtnConnexion}/>
            <Grid
              container
              style={{ textAlign: "center", marginTop: "4%", color: "#616161" }}
            >
              <Grid item md={12} xs={12} style={{ color: "#FF7A42" }}>
                <Link to="/recuperation" style={{color:"inherit",textDecoration:"none"}}><div> Récupérer mon mot de passe</div></Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login1;
