import Boutton from "../boutton/Boutton";
import { UseStylesBoutton } from "../boutton/Boutton.style";
import Groupe from "../../../assert/Groupe 12252.png";
import { Grid } from "@material-ui/core";
import { UseStyleDialogContent } from "./DialogContent.style";
const DialogContent = (props) => {
  const classes = UseStyleDialogContent();

  return (
    <div className={classes.root}>
      <Grid item md={12} xs={12} alignItems="center" align="center">
        <div className={classes.imageCard}>
          <img src={props.image} width='100%' height="100%" />
          <div className={classes.texte}>
            {props.imageText}
          </div>
        </div>
      </Grid>
      <Grid
        item
        md={12}
        xs={12}
        alignContent="center"
        alignItems="center"
        justify='center'
        style={{
          fontSize: "10px",
          width: "70%",
          marginLeft: "auto",
          marginRight: "auto",
          color: "#757575",
          font: "normal normal normal 20px/35px Roboto",
          padding:"1em"
        }}
      >
        {props.textInterrogative}
      </Grid>

      <Grid item align="center">
        <Grid item md={12} xs={12}>
          {props.button1}
        </Grid>
        <Grid item md={12} xs={12} style={{ marginTop: "2%" }}>
         {props.button2}
        </Grid>
      </Grid>
    </div>
  );
};
export default DialogContent;
