import { makeStyles } from "@material-ui/core";
export const UseStyleDialogContent = makeStyles((theme) => ({
  texte: {
    position: "absolute",
    top: "25%",
    left:"0px",
    width:"60%",
    height:"100%",
    color: "#757575",
    font: "normal normal normal 25px/35px Roboto",
    margin:"10px 0px 0px 20px",
    textAlign:"left",

  },
  imageCard: {
    position: "relative",
    width: "100%",
    height: "100%",
    justifyContent:"center",
    alignItem:"center"
  },
}));
