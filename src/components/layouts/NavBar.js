import React, { useState } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Img from "../../assert/download";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Dashboard, NotificationsNone } from "@material-ui/icons";
import MenuIcon from "@material-ui/icons/Menu";
import {
  AppBar,
  Avatar,
  Badge,
  Grid,
  Paper,
  Toolbar,
  Drawer,
  List,
  ListItem,
  Divider,
  MenuItem,
  Menu,
  Button,
  Tab,
  Tabs,
  MenuList,
} from "@material-ui/core";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import red from "@material-ui/core/colors/red";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import KeyboardArrowDownSharpIcon from "@material-ui/icons/KeyboardArrowDownSharp";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useStyleNavBar } from "./Navbar.style";
import ListesDesContribuables from "../pages/ListeDesContribuables/listesDesContribuables";
import { NavLink } from "react-router-dom";
import NoPath from "../../assert/NoPath - Copie (3).png";
function NavBar() {
  const classes = useStyleNavBar();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  const [open, setOpen] = useState(false);
  const handleDrawer = () => {
    setOpen(true);
  };

  const [value, setValue] = React.useState(0);

  const handleChangeTabs = (event, value) => {
    console.log(value);
    setValue(value);
  };
  const [anchorElContribuable, setAnchorElContribuable] = React.useState(null);
  const [anchorElDgi, setAnchorElDgi] = React.useState(null);
  const [anchorElExpert, setAnchorElExpert] = React.useState(null);

  const handleClickContribuable = (event) => {
    setAnchorElContribuable(event.currentTarget);
  };
  const handleClickDgi = (event) => {
    setAnchorElDgi(event.currentTarget);
  };
  const handleClickExpert = (event) => {
    setAnchorElExpert(event.currentTarget);
  };

  const handleCloseContribuable = () => {
    setAnchorElContribuable(null);
  };

  const handleCloseDgi = () => {
    setAnchorElDgi(null);
  };
  const handleCloseExpert = () => {
    setAnchorElExpert(null);
  };


  return (
    <div className={classes.root}>
      <AppBar position="static" color="inherit">
        <Toolbar>
          <Typography className={classes.childTypo} color="inherit">
            {isMobile ? (
              <Grid className={classes.rootToggle} container>
                <Grid item xs={6} align="left">
                  <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    onClick={handleDrawer}
                    className={classes.menuToggle}
                    style={{ outline: "none" }}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>

                <Grid item xs={6} align="right">
                  <IconButton
                    style={{ outline: "none" }}
                    aria-label="show 4 new notifications"
                    color="inherit"
                    edge="right"
                    className={classes.menuToggle}
                  >
                    <Badge badgeContent={4} color="secondary">
                      <NotificationsNone />
                    </Badge>
                  </IconButton>
                </Grid>
              </Grid>
            ) : (
              <>
                <Typography className={classes.title}>
                  <img src={NoPath} alt="logo Oecci" width="71" />
                </Typography>
                <Typography className={classes.title}>
                  <NavLink exact  to="/tableau-de-bord" activeClassName={classes.mainNavActive} className={classes.mainNav}>Accueil</NavLink>
                  </Typography>
                <Typography
                  className={classes.title}
                  onClick={handleClickContribuable}
                >
                  Contribuable

                </Typography>
                <Menu
                  className={classes.select}
                  open={Boolean(anchorElContribuable)}
                  id="simple-menu"
                  anchorEl={anchorElContribuable}
                  keepMounted
                  onClose={handleCloseContribuable}
                >
                  <MenuItem onClick={handleCloseContribuable}>
                    Contribuable
                  </MenuItem>
                  <MenuItem onClick={handleCloseContribuable}>
                    <Typography>
                      <NavLink exact
                        to="/liste-des-contibuables"
                        style={{ textDecoration: "none", color: "inherit" }}
                      >
                        Liste des contribuables
                      </NavLink>
                    </Typography>
                  </MenuItem>

                  <MenuItem onClick={handleCloseContribuable}>
                    <Typography>
                      <NavLink exact
                        to="/gestion-des-arbitrages"
                        style={{ textDecoration: "none", color: "inherit" }}
                      >
                        Arbitrage
                      </NavLink>
                    </Typography>
                  </MenuItem>
                </Menu>

                <Typography className={classes.title} onClick={handleClickDgi}>
                  {" "}
                 DGI
                </Typography>
                <Menu
                  className={classes.select}
                  open={Boolean(anchorElDgi)}
                  id="simple-menu"
                  anchorEl={anchorElDgi}
                  keepMounted
                  onClose={handleCloseDgi}
                >
                  <MenuItem onClick={handleCloseDgi}>DGI</MenuItem>

                  <MenuItem onClick={handleCloseDgi}>
                    <NavLink exact
                      to="/tableau-de-bord-de-gestion-des-campagnes"
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      Campagnes
                    </NavLink>
                  </MenuItem>

                  <MenuItem onClick={handleCloseDgi}>
                    <NavLink exact
                      to="/Liste-des-directions"
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      Directions régionale
                    </NavLink>
                  </MenuItem>
                  <MenuItem onClick={handleCloseDgi}>
                    Services des impôts
                  </MenuItem>
                  <MenuItem onClick={handleCloseDgi}>Statistiques</MenuItem>
                </Menu>

                <Typography className={classes.title}>
                  <NavLink exact
                    to="/liste-des-experts-comptables" activeClassName={classes.mainNavActive} className={classes.mainNav}
                    
                  >
                    Experts-compables
                  </NavLink>
                </Typography>
                <Typography className={classes.title}>
                  <NavLink exact
                    to="/gestion-des-acces-table"
                    activeClassName={classes.mainNavActive} className={classes.mainNav}
                    
                  >
                    Accès
                  </NavLink>
                </Typography>

                <Typography className={classes.title}>
                  <NavLink exact 
                    to="/tableau-de-bord-de-gestion-de-paiement" activeClassName={classes.mainNavActive} className={classes.mainNav}
                  
                  >
                    Paiement
                  </NavLink>
                </Typography>
                <Typography className={classes.title}>
                  {" "}
                  <NavLink exact
                    to="/liste-des-offres" activeClassName={classes.mainNavActive} className={classes.mainNav}
         
                  >
                    Offres
                  </NavLink>
                </Typography>

                <Typography className={classes.title} color="inherit">
                  <select size="0" className={classes.deroulant}>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                  </select>
                </Typography>
                <Typography className={classes.title} color="inherit">
                  <IconButton
                    style={{ outline: "none" }}
                    aria-label="show 4 new notifications"
                    color="inherit"
                    className={classes.title}
                  >
                    <Badge badgeContent={4} color="secondary">
                      <NotificationsNone />
                    </Badge>
                  </IconButton>
                </Typography>
                <Typography className={classes.title}>
                  <Avatar alt="image" src={Img} />
                </Typography>
                <Typography
                  component="small"
                  style={{ fontSize: "12px", color: "gray" }}
                >
                  <strong>Guei Dessekane</strong>
                  <br />
                  <small>problem solver</small>
                </Typography>

                <Typography className={classes.title} color="inherit">
                  <IconButton
                    style={{ outline: "none" }}
                    color="inherit"
                    className={classes.title}
                  >
                    <PowerSettingsNewIcon style={{ color: "#ff3d00" }} />
                  </IconButton>
                </Typography>
              </>
            )}
          </Typography>
        </Toolbar>
      </AppBar>

      {/* Selected tableau */}

      

      {/* Drawer navBar */}
      <Drawer
        anchor="left"
        open={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <div className={classes.drowerContainer}>
          <List className={classes.drawerFont}>
            <ListItem>
              <ListItemIcon>
                <Avatar alt="image" src={Img} />
              </ListItemIcon>
              <ListItemText>Guei Dessekane problem solver</ListItemText>
              <ListItemIcon>
                <IconButton
                  style={{ outline: "none" }}
                  aria-label="show 4 new notifications"
                  color="inherit"
                >
                  <PowerSettingsNewIcon style={{ color: "#ff3d00" }} />
                </IconButton>
              </ListItemIcon>
            </ListItem>
            <Divider />
            <div
              style={{
                marginLeft: "20%",
                color: "#757575",
                font: "roboto stlab",
              }}
            >
              <ListItem>
                <ListItemText>Contribuable</ListItemText>
              </ListItem>

              <ListItem>
                <ListItemText>DGI</ListItemText>
              </ListItem>

              <ListItem>
                <ListItemText>Experts-comptables</ListItemText>
              </ListItem>

              <ListItem>
                <ListItemText>Accès</ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>Paiement</ListItemText>
              </ListItem>

              <ListItem>
                <ListItemText>Offres</ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText>Experts-comptables</ListItemText>
              </ListItem>
            </div>
          </List>
        </div>
      </Drawer>
    </div>
  );
}

export default NavBar;
