import { makeStyles, useTheme } from "@material-ui/core/styles";
export const useStyleNavBar = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    color: "#757575",
    fontSize: "20px",
    [theme.breakpoints.down("sm")]: {
      align: "left",
      color: "#757575",
    },
  },
  title: {
    marginLeft: theme.spacing(1),
    flexGrow: 1,
    fontSize: "15px",
    cursor: "grab",
    justifyContent: "center",
    alignItems: "center",

    [theme.breakpoints.down("sm")]: {
      align: "left",
      cursor: "grab",
    },
  },
  childTypo: {
    flexGrow: 1,

    display: "flex",
    margin: "auto",
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: theme.spacing(0),
    [theme.breakpoints.down("sm")]: {
      flexGrow: 1,
      marginLeft: theme.spacing(-50),
    },
  },
  deroulant: {
    borderRadius: "15px",
    borderColor: "#ff3d00",
    backgroundColor: "white",
    width: "100%",
    outline: "none",
    color: "#757575",
  },
  formControl: {
    width: "150px",
  },
  selectEmpty: {
    maxWidth: "100px",
    outline: "none",
    color: "#757575",
  },
  drowerContainer: {
    maxWidth: "300px",
  },
  toggle: {
    flexFlow: 1,
  },
  rootToggle: {
    flexGrow: 1,
  },
  menuToggle: {
    marginLeft: theme.spacing(50),
    color: "#757575",
    cursor: "pointer",
  },
  drawerFont: {
    fontStyle: "Apple Chancery, cursive",
  },
  badgeContentColor: {
    backgroundColor: "#FF8A48",
  },
  tab: {
    textTransform: "capitalize !important",
    minWidth: "100px !important",
  },
  indicator: {
    top: "0px",
    backgroundColor: "#FF7A42",
  },
  select: {
    border: "none",
    background: "none",
    width: "90%",
    outline: "none",
    color: "#757575",
  },
  mainNavActive:{
    borderTop:"4px solid #FF7A42",
    textDecoration:"none",
    color:"#FF7A42",
    "&:hover":{
      color:"#FF7A42",
      textDecoration:"none",
    }
    
  },
  mainNav:{
    textTransform:"none",
    textDecoration:"none",
    color:"#757575",
    "&:hover":{
      textDecoration:"none",
      color:"#757575"
    }
  }
}));
