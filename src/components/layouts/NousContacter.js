import React from "react";
import "./NousContacter.css";
import image from "../../assert/Groupe de masques 6.png";
import { TextField, Grid, Button, Typography } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import RoomIcon from "@material-ui/icons/Room";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PhoneIcon from "@material-ui/icons/Phone";
import Boutton from "./boutton/Boutton";
import { UseStylesBoutton } from "./boutton/Boutton.style";

function NousContacter() {
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <div className="containerRoot">
      <div className="block1">
        <div className="image">
          <img src={image} className="img-fluid" />
        </div>
        {/* <div className="content">
          <div className="content-text">
            <div className="item">
              <div style={{fontWeight:"bold",fontSize:"18px"}}>Nous contacter</div>
              <div className="textNous">
                au cœur du métier Lorem ipsum dolor assis amet, élite adipisice
                consectetur. Impedit dolores ad molestiae rem repudiandae
              </div>

              <div className="contacteContainer">
                <ul className="ul">
                  <li>
                    <RoomIcon />
                  </li>
                  <li>
                    <ul className="ulli">
                      <li>No 40 Plateau Rue 133/2</li>
                      <li>Abidjan , Plateau , Cote d'Ivoire</li>
                    </ul>
                  </li>
                </ul>
                <ul className="ul">
                  <li>
                    <MailOutlineIcon />
                  </li>
                  <li>
                    <ul className="ulli">
                      <li>Email</li>
                      <li>information@oecci.com</li>
                    </ul>
                  </li>
                </ul>
                <ul className="ul">
                  <li>
                    <PhoneIcon />
                  </li>
                  <li>
                    <ul className="ulli">
                      <li>(000) 123 456 789</li>
                      <li>(000) 324 644 676</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div> */}
        <Grid className="content" spacing={2}>
          <Grid item md={12} xs={12}>
            <Typography component="div" variant="h4">
              Nous contacter
            </Typography>
          </Grid>
          <Grid item md={12} xs={12}>
            <div
              style={{
                padding: "1em 0em 0em 0em",
                textAlign: "justify",
                font: "normal normal normal 18px/25px Roboto",
                letterSpacing: "0px",
                color: "#fff",
                opacity: 1,
                fontSize: "15px",
              }}
            >
              au cœur du métier Lorem ipsum dolor assis amet, élite adipisice
              consectetur. Impedit dolores ad molestiae rem repudiandae
            </div>
          </Grid>
          <Grid item md={12} xs={12} container className="grid">
            <Grid item md={2}>
              <RoomIcon fontSize="large" />
            </Grid>
            <Grid item md={10}>
              <Grid item md={12} xs={12}>
                No 40 Plateau Rue 133/2
              </Grid>
              <Grid item md={12} xs={12}>
                Abidjan , Plateau , Cote d'Ivoire
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={12} xs={12} container className="grid">
            <Grid item md={2}>
              <MailOutlineIcon fontSize="large" />
            </Grid>
            <Grid item md={10}>
              <Grid item md={12} xs={12}>
                <strong>Email</strong>
              </Grid>
              <Grid
                item
                md={12}
                xs={12}
                style={{ textDecoration: "underline" }}
              >
                information@oecci.com
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={12} xs={12} container className="grid">
            <Grid item md={2}>
              <PhoneIcon fontSize="large" />
            </Grid>
            <Grid item md={10}>
              <Grid item md={12} xs={12}>
                (000) 123 456 789
              </Grid>
              <Grid item md={12} xs={12}>
                (000) 324 644 676
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>

      <div className="block2">
        <div className="bouton">
          <Link
            href="/"
            style={{
              textDecorationLine: "none",
              color: "#fff",
              textTransform: "none",
            }}
          >
            <Boutton
              name="Retour à l’accueil"
              style={UseStylesBoutton().orangeBtn2NoBorder}
              startIcon={<ArrowBackIcon />}
            />
          </Link>
        </div>
        <div className="text">
          <p className="title">Envoyez-nous un message</p>
          <small>
            Lorem ipsum dolor assis amet, élite adipisice consectetur
          </small>
        </div>
        <div className="form-container">
          <form className="form" noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="Nom"
              label="Nom"
              type="text"
              id="Nom"
              autoComplete="current-nom"
              className="TextField"
            />

            <TextField
              variant="outlined"
              margin="normal"
              required
              name="email"
              label="Email"
              type="mail"
              id="email"
              autoComplete="current-email"
              className="TextField"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="Objet"
              label="Objet"
              type="text"
              id="Objet"
              autoComplete="current-Objet"
              className="TextField"
            />
            <TextField
              variant="outlined"
              required
              name="Message"
              multiline
              col={5}
              label="Message"
              id="Message"
              className="TextField"
            />

            <Grid item md={12} xs={12}>
            <Boutton name="Envoyer" style={UseStylesBoutton().greenBtnConnexion}/>
            </Grid>

            <Grid
              item
              md={12}
              align="right"
              style={{
                font: "normal normal normal 18px/25px Roboto",
                letterSpacing: "0px",
                fontSize: "15px",
                marginTop: "15px",
              }}
            >
              <Link to="#" style={{color:"#757575"}}>Besoin d’aide ?</Link>
            </Grid>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NousContacter;
