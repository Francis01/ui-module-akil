import React from 'react';
import "./RecuperationPassword.css";
import image from '../../assert/Groupe de masques 6.png'
import { Checkbox,Button, FormControlLabel, Grid, IconButton, InputAdornment, TextField } from "@material-ui/core";
import {Link} from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Boutton from './boutton/Boutton';
import { UseStylesBoutton } from './boutton/Boutton.style';
function RecuperationPassword() {



    const [values, setValues] = React.useState({
        password: "",
        showPassword: false,
      });
    
      const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
      };
    
      const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
    
      return (
        <div className="containerRoot">
          <div className="block1">
            <div className="image">
              <img src={image} className="img-fluid" />
            </div>
            <div className="content">
              <div className="textContent">
                <div className="content1">Ensemble construisons l'innovation</div>
    
                <div className="content2">
                  <small
                    style={{ font: "normal normal 300 34px/45px Roboto Slab",fontSize:"18px " }}
                  >
                    au cœur du métier{" "}
                  </small>{" "}
                  <br />{" "}
                  <strong style={{ fontWeight: "bold" }}>
                    de l'expert-comptable
                  </strong>
                </div>
              </div>
            </div>
          </div>
    
          <div className="block2">
            <div className="bouton">
              
              {/*   <Link
                  to="/"
                  style={{ textDecorationLine: "none", color: "#fff",textTransform:"none" }}
                >
                  <Boutton name="Retour à l’accueil" style={UseStylesBoutton().orangeBtn2NoBorder} startIcon={<ArrowBackIcon/>}/>
                </Link> */}
            </div>
            <div className="form-container">
              <form className="form" noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  id="Email"
                  label="Email"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  className="TextField"
                />
            
                <Boutton name="Récupérer votre mot de passe" style={UseStylesBoutton().greenBtnConnexion}/>
                
              </form>
            </div>
          </div>
        </div>
      );
    }

export default RecuperationPassword;