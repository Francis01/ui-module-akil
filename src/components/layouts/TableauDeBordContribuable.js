import React, { useState } from 'react';
import { Button, withStyles, Grid, makeStyles, Paper, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import "./TableauDeBordContribuable.css";
import LinearProgress from '@material-ui/core/LinearProgress';
import { DataChartBar, DataChartLine, DataChartPie, DataChartPie2, DataChartPie3 } from '../../@store/DataChart'
import { Line, Bar, Pie, Doughnut } from 'react-chartjs-2'
import CircularProgress from '@material-ui/core/CircularProgress';


const BorderLinearProgress = withStyles((theme) => ({
    root: {
        flexGrow: 1,
        height: 10,
        borderRadius: 5,
    },
    colorPrimary: {
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
        borderRadius: 5,
        backgroundColor: '#ff5722',
    },
}))(LinearProgress);


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: '50px'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        borderRadius: "15px",
        /* height: "170px" */
        minHeight: "170px",
    },
    paper2: {
        borderRadius: "15px",
        backgroundColor: "#FF7A42",



    },
    btn1: {
        fontSize: "10px",
        marginTop: "20px",
        backgroundColor: "#2DCE98",
        borderRadius: "15px",
        height: "20px",
        width: "50px",
        color: "#fff",
        marginLeft: theme.spacing(2)



    },
    table: {
        minHeight: "100%",

    }
}))

function TableauDeBordContribuable() {
    const classes = useStyles()

    const [Progress, setProgress] = useState(15);

    return (
        <div className={classes.root}>


            {/* block1 */}

            <Grid container spacing={4}>

                <Grid item xs={12} md={4} sm={12}>

                    <Grid container spacing={2}>

                        <Grid item xs={12} md={12}>
                            <Paper className={classes.paper2}>

                                <div>Seuil de commissariat</div>
                                <div>7000</div>
                            </Paper>
                            <Paper className={classes.paper}>
                                Nombre de contribuable qui <br /> doivent changer de régime de contrôle
                            </Paper>
                        </Grid>
                    </Grid>



                    <Grid container spacing={2}>
                        <Grid item xs={12} md={12}>
                            <Paper style={{ borderRadius: "18px" }}>
                                <div>Nombre de contribuable ayant dépassé le seuil CAC par régime</div>
                            </Paper>
                            <Paper className={classes.paper}>

                                <Grid container spacing={4}>
                                    <Grid item xs={4}>
                                        <p>Régime IS</p>
                                        <small>1000000</small>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <p>Régime RSI</p>
                                        <small>300000</small>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <p>Régime RN</p>
                                        <small>100000</small>

                                    </Grid>
                                </Grid>

                            </Paper>
                        </Grid>
                    </Grid>

                </Grid>

                {/* Block de tableau */}

                <Grid item xs={12} md={8} >
                    <Paper className={classes.table}>
                        <Typography align="left" >Liste des contribuables ayant dépassé le seuil</Typography>
                        <Typography align="right" > <Button contained style={{ backgroundColor: "#FF7A42", borderRadius: "30px" }}>Voir plus</Button> </Typography>
                        <Typography align="left" >Total contribuables : 7000</Typography>


                        <Table aria-label="a dense table" style={{ fontSize: "5px", justifyItems: "center" }}>
                            <TableHead style={{ backgroundColor: "#257163", align: "left" }}>
                                <TableRow style={{ color: "white", borderRadius: "50px" }}>
                                    <TableCell align="left">Forme juridique </TableCell>
                                    <TableCell align="left">IDU</TableCell>
                                    <TableCell align="left">Numéro du contribuable</TableCell>
                                    <TableCell align="left">Raison sociale</TableCell>
                                    <TableCell align="left">Email</TableCell>
                                    <TableCell align="left">Expert comptable</TableCell>
                                    <TableCell align="left">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>

                                    <TableCell align="left">SARL</TableCell>
                                    <TableCell align="center">25968954</TableCell>
                                    <TableCell align="right">128556U</TableCell>
                                    <TableCell align="right">Diaud Corp</TableCell>
                                    <TableCell align="right">info@gmail.com</TableCell>
                                    <TableCell align="right">Strataige SA</TableCell>
                                    <TableCell align="right">Relancer</TableCell>

                                </TableRow>

                                <TableRow>

                                    <TableCell align="left">SA</TableCell>
                                    <TableCell align="center">25968954</TableCell>
                                    <TableCell align="right">128556U</TableCell>
                                    <TableCell align="right">Diaud Corp</TableCell>
                                    <TableCell align="right">info@gmail.com</TableCell>
                                    <TableCell align="right">Strataige SA</TableCell>
                                    <TableCell align="right">Relancer</TableCell>
                                </TableRow>

                                <TableRow>

                                    <TableCell align="left">SAS</TableCell>
                                    <TableCell align="center">25968954</TableCell>
                                    <TableCell align="right">128556U</TableCell>
                                    <TableCell align="right">Diaud Corp</TableCell>
                                    <TableCell align="right">info@gmail.com</TableCell>
                                    <TableCell align="right">Strataige SA</TableCell>
                                    <TableCell align="right">Relancer</TableCell>
                                </TableRow>



                                <TableRow>

                                    <TableCell align="left">SCOOP</TableCell>
                                    <TableCell align="center">25968954</TableCell>
                                    <TableCell align="right">128556U</TableCell>
                                    <TableCell align="right">Diaud Corp</TableCell>
                                    <TableCell align="right">info@gmail.com</TableCell>
                                    <TableCell align="right">Strataige SA</TableCell>
                                    <TableCell align="right">Relancer</TableCell>
                                </TableRow>

                                <TableRow>

                                    <TableCell align="left">GIE</TableCell>
                                    <TableCell align="center">25968954</TableCell>
                                    <TableCell align="right">128556U</TableCell>
                                    <TableCell align="right">Diaud Corp</TableCell>
                                    <TableCell align="right">info@gmail.com</TableCell>
                                    <TableCell align="right">Strataige SA</TableCell>
                                    <TableCell align="right">Relancer</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Paper>



                </Grid>

                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <Grid container>
                            <Typography >Nombre de contribuable ayant dépassé le seuil CAC par régime (%)</Typography>
                            <Grid item xs={4}>
                                <Doughnut
                                    data={{
                                        labels: ["Régime IS"],

                                        datasets: [{
                                            backgroundColor: "#34B53A",
                                            color: "#fff",
                                            borderWidth: 6,
                                            data: [67]
                                        },

                                        ],
                                    }}
                                    options={{
                                        legend: {
                                            display: true,
                                            position: 'bottom'
                                        },
                                    }}

                                />
                            </Grid>

                            <Grid item xs={4}>
                                <Doughnut
                                    data={{
                                        labels: ["Régime IS"],

                                        datasets: [{
                                            backgroundColor: "#4339F2",
                                            color: "#fff",
                                            borderWidth: 6,
                                            data: [67]
                                        },

                                        ],
                                    }}
                                    options={{
                                        legend: {
                                            display: true,
                                            position: 'bottom'
                                        },
                                    }}

                                />
                            </Grid>

                            <Grid item xs={4}>

                                <CircularProgress variant="static" size={45} thickness={3.6} value={Progress} color="primary" />
                            </Grid>

                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={8}>
                    <Paper className={classes.paper}>
                        <Grid spacing={2} container>
                            <Grid item xs={4}>



                                <Grid container style={{ color: "#757575" }}>
                                    <Grid item align='left' md={10} xs={6}>SARL</Grid>
                                    <Grid item align="right" md={2} xs={6}>100</Grid>
                                </Grid>
                                <BorderLinearProgress variant="determinate" value={25} />

                                <br />

                                <Grid container style={{ color: "#757575" }} >
                                    <Grid item align='left' md={10} xs={6}>SA</Grid>
                                    <Grid item align="right" md={2} xs={6}>120</Grid>
                                </Grid>
                                <BorderLinearProgress variant="determinate" value={35} />
                                <br />


                                <Grid container style={{ color: "#757575" }} >
                                    <Grid item align='left' md={10} xs={6}>SAS</Grid>
                                    <Grid item align="right" md={2} xs={6}>98</Grid>
                                </Grid>

                                <BorderLinearProgress variant="determinate" value={50} />


                            </Grid>

                            <Grid item xs={4} style={{ color: "#757575" }} >

                                <Grid container >
                                    <Grid item align='left' md={10} xs={6}>SNC</Grid>
                                    <Grid item align="right" md={2} xs={6}>500</Grid>
                                </Grid>

                                <BorderLinearProgress variant="determinate" value={70} />
                                <br />

                                <Grid container >
                                    <Grid item align='left' md={10} xs={6}>SCOOP</Grid>
                                    <Grid item align="right" md={2} xs={6}>625</Grid>
                                </Grid>

                                <BorderLinearProgress variant="determinate" value={70} />
                                <br />
                                <Grid container style={{ color: "#757575" }} >
                                    <Grid item align='left' md={10} xs={6}>GIE</Grid>
                                    <Grid item align="right" md={2} xs={6}>23</Grid>
                                </Grid>
                                <BorderLinearProgress variant="determinate" value={11} />

                            </Grid>


                            <Grid item xs={4} style={{ color: "#757575" }} >

                                <Grid container >
                                    <Grid item align='left' md={10} xs={6}>Individuel</Grid>
                                    <Grid item align="right" md={2} xs={6}>821</Grid>
                                </Grid>
                                <BorderLinearProgress variant="determinate" value={90} />
                                <br />

                                <Grid container >
                                    <Grid item align='left' md={10} xs={6}>Autres</Grid>
                                    <Grid item align="right" md={2} xs={6}>3</Grid>
                                </Grid>

                                <BorderLinearProgress variant="determinate" value={5} />

                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>

        </div >
    )

}

export default TableauDeBordContribuable;




