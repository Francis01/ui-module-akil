import React from "react";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import { UseStyleAnnuler } from "./Annuler.style";

const Annuler = (props) => {
  const classes = UseStyleAnnuler();
  const preventDefault = (event) => event.preventDefault();
  return (
    <div className={classes.root}>
      <Button className={classes.btn}>
          {props.title}
      </Button>
    </div>
  );
};

export default Annuler;
