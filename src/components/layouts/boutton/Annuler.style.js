import { makeStyles } from "@material-ui/core/styles";
export const UseStyleAnnuler = makeStyles((theme) => ({
   root:{
        flexGrow:1,
        outlineStyle:'none',
    },
  btn: {
    background: "#fff",
    borderRadius: "27px",
    opacity: 1,
    border:"1px solid #FF7A42",
    color:'#FF7A42',
    width: "150px",
    outlineStyle:'none',
    height: "40px",
    textTransform: "capitalize",
    marginTop:theme.spacing(2),
    "&:hover": {
      background: "#FFF",
      outlineStyle:'none',
     
    },
  },

}));
