import { Button } from "@material-ui/core";
const Boutton = (props) => {
  return (
    <div style={{ overflow: "hidden" }}>
      <Button
        style={{ outline: "none" }}
        className={props.style}
        type={props.type}
        startIcon={props.startIcon}
        variant={props.variant}
        onClick={props.click}
      >
        {props.name}
      </Button>
    </div>
  );
};

export default Boutton;
