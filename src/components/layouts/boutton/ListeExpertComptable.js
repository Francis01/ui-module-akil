import {UseStylesListeExpertComptable} from './ListeExpertComptable.style'
import { Button } from '@material-ui/core';

const ListeExpertComptable= ()=>{
    const classes=UseStylesListeExpertComptable()
    return(
        <div className={classes.root}>
            <Button className={classes.btnListe} >Listes des experts comptables</Button>
        </div>
    )
}
export default ListeExpertComptable;