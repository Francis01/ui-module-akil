import {makeStyles} from '@material-ui/core';

export const UseStylesListeExpertComptable=makeStyles((theme)=>({
    root:{
        flexGrow:1,
    },
    btnListe:{
        width:'100%',
        height:'40px',
        borderRadius:'10px 10px 10px 10px',
        color:'#fff',
        border:'1px solid #FF7A42',
        textTransform:'capitalize',
        background:'#FF7A42',
        borderRadius:'18px',
        fontSize:'10px',
        '&:hover':{
            background:'#FF7A42',
        }
    }
}))