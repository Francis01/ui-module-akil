import {UseStylesPrecedent} from './Precedent.style'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Button } from '@material-ui/core';

const Precedent =(props)=>{
    const classes=UseStylesPrecedent()
    return(
        <div className={classes.root}>
            <Button className={props.style} startIcon={props.startIcon}>{props.title}</Button>
        </div>
    )
}

export default Precedent;