import {makeStyles} from '@material-ui/core/';

export const UseStylesPrecedent  = makeStyles((theme)=>({
    root:{
        flexGrow:1,
    },
    btnPriv:{
        color:'#fff',
        border:'1px solid white',
        textTransform:'capitalize',
        borderRadius:'18px',
        width:'150px'
    },
}))