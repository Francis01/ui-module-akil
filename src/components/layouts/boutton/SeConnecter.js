import React from "react";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import { UseStyleSeConnecter } from "./SeConnecter.style";

const SeConnecter = () => {
  const classes = UseStyleSeConnecter();
  const preventDefault = (event) => event.preventDefault();
  return (
    <div className={classes.root}>
      <Button className={classes.btn}>
        <Link
          href="/Login"
          style={{ textDecorationLine: "none", color: "#fff" }}
        >
          Se connecter{" "}
        </Link>
      </Button>
    </div>
  );
};

export default SeConnecter;
