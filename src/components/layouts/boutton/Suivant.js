import React from "react";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import { UseStyleSuivant } from "./Suivant.style";

const Suivant = (props) => {
  const classes = UseStyleSuivant();
  const preventDefault = (event) => event.preventDefault();
  return (
    <div className={classes.root}>
      <Button className={classes.btn}>
          {props.title}
       
      </Button>
    </div>
  );
};

export default Suivant;
