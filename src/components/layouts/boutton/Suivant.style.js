import { makeStyles } from "@material-ui/core/styles";

export const UseStyleSuivant = makeStyles((theme) => ({
  /*  root:{
        flexGrow:1
    }, */
  btn: {
    background: "#FF7A42 0% 0% no-repeat padding-box",
    boxShadow: " 0px 7px 13px #FF7A4259",
    borderRadius: "27px",
    opacity: 1,
    width: "150px",
    height: "40px",
    outlineStyle:'none',
    textTransform: "capitalize",
    marginTop:theme.spacing(2),
    color: "#fff",
    "&:hover": {
      background: "#FF7A42 0% 0% no-repeat padding-box",
      color: "#fff",
    },
  },
  link: {
    textDecorationLine: "none",
    color: "#fff",
    textDecoration: "none",
  },
}));
