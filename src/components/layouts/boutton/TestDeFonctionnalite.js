import { Button } from "@material-ui/core";
import React from "react";
import { UseStylesTestDeFonctionnalite } from "./TestDeFonctionnalite.style";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
const TestDeFonctionnalite = () => {
  const classes = UseStylesTestDeFonctionnalite();
  return (
    <div className={classes.root}>
      <Button
        style={{ outline: "none", textTransform: "none" }}
        variant="contained"
        endIcon={<ArrowForwardIcon />}
        className={classes.btn}
      >
        Testez cette fonctionnalité
      </Button>
    </div>
  );
};

export default TestDeFonctionnalite;
