import { makeStyles } from "@material-ui/core/styles";

export const UseStylesTestDeFonctionnalite = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  btn: {
    borderRadius: "24px",
    textTransform: "none",
    color: "#fff",
    background: "#FF7A42",
    width: "60%",
    height: "50px",
    "&:hover": {
      background: "#FF7A42" /* 
            boxShadow:'5px 5px 5px gray' */,
    },
  },
}));
