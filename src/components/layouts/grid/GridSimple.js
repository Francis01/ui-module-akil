import { Grid, makeStyles } from '@material-ui/core'

const UseGridSimpleStyle=makeStyles((theme)=>({
    GridBorder:{
        borderRadius:'18px',
        border:'1px solid gray',
        minHeight:"251px",
        padding:'1em',
        background:'red'
    }
}))
const GridSimple =(props)=>{
    const classes=UseGridSimpleStyle()
    return(
        <>
        
            <Grid  className={props.style}>

                <Grid item xs={6} md={12}>
                    Classification par régime RN
                </Grid>

                <Grid item xs={12} md={12} style={{marginTop:'5%'}}>
                    <Grid item md={6}>
                        Contribuables
                    </Grid>
                    <Grid item md={6}>
                    <small> 50000</small>
                    </Grid>
                </Grid>

                <Grid item xs={6} md={12}>
                    Classification par régime RN
                </Grid>

                <Grid item xs={12} md={12} >
                    <Grid item md={6}>
                        Contribuables
                    </Grid>
                    <Grid item md={6}>
                        <small> 50000</small>
                       
                    </Grid>
                </Grid>

            </Grid>

            
        </>
       
           
    )
}
export default GridSimple;