import {makeStyles} from '@material-ui/core'


export const UseGridSimpleStyle =makeStyles((theme)=>({
    borderGreen:{
        border:'1px solid #34B53A',
        borderRadius:'18px',
        boxShadow:'0px 6px 22px #0000000D',
        background:'#E2FBD745 0% 0% no-repeat padding-box',
        minHeight:"251px",
        padding:'1em',
    },
    borderBlue:{
        border:'1px solid #4339F2',
        borderRadius:'18px',
        boxShadow:'0px 6px 22px #0000000D',
        background:'#DAD7FE24 0% 0% no-repeat padding-box',
        minHeight:"251px",
        padding:'1em',
    },
    borderOrange:{
        border:'1px solid #FF3A29',
        borderRadius:'18px',
        boxShadow:'0px 6px 22px #0000000D',
        background:'#FFE5D324 0% 0% no-repeat padding-box',
        minHeight:"251px",
        padding:'1em',
    },
    paper:{
        flexGrow:1,
        borderRadius:'18px',
        backgroundColor:"#fff",
        minHeight:"251px",
        alignItems:"center",
        textAlign:'center',
        padding: '1em',
        boxShadow:'0px 0px 5px 0px gray',
    }
}))