import { Grid, Paper } from "@material-ui/core";
/* import { UseStaticStyles} from '../../pages/Static.style' */
const GridPaper = (props) => {
  return (
    <Paper elevation={4} className={props.style.paper2}>
      <Grid item md={12} xs={12} className={props.style.headerGridWhite}>
        <Grid
          item
          md={12}
          xs={12}
          align="left"
          className={props.style.titleHeaderOrange}
        >
          {props.headerTitle}
        </Grid>
        <Grid
          item
          md={12}
          xs={12}
          align="left"
          className={props.style.prixHeaderOrange}
        >
          {props.headerTitlePrix}
        </Grid>
      </Grid>

      <Grid container item md={12} xs={12} className={props.style.bodyGrid}>
        <Grid item xs={8} md={8}>
          {props.content1}
        </Grid>
        <Grid
          item
          xs={4}
          align="right"
          md={4}
          align="right"
          className={props.style.prix}
        >
          {props.content1Prix}
        </Grid>
        <Grid item xs={8} md={8}>
          {props.content2}
        </Grid>
        <Grid
          item
          xs={4}
          align="right"
          md={4}
          align="right"
          className={props.style.prix}
        >
          {props.content2Prix}
        </Grid>
        <Grid item xs={8} md={8}>
          {props.content3}
        </Grid>
        <Grid
          item
          xs={4}
          align="right"
          md={4}
          align="right"
          className={props.style.prix}
        >
          {props.content3Prix}
        </Grid>
        <Grid item xs={8} md={8}>
          {props.content4}
        </Grid>
        <Grid
          item
          xs={4}
          align="right"
          md={4}
          align="right"
          className={props.style.prix}
        >
          {" "}
          {props.content4Prix}
        </Grid>
      </Grid>
    </Paper>
  );
};

export default GridPaper;
