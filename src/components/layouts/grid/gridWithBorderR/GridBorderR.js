import { Grid } from "@material-ui/core";

const GridBorderR = (props) => {
  return (
    <Grid container className={props.style.paper}>
      <Grid
        md={12}
        style={{
          display: "flex",
          justifyContent: "space-between",
          boxShadow: "0px 5px 3px #0000000D",
          width: "auto",
        }}
        container
      >
        <Grid md={12} align="left">
          Nombre de contribuables ayant dépassé le seuil CAC par régime
        </Grid>
      </Grid>

      <Grid container md={12}>
        <Grid item md={4} xs={4} className={props.style.borderR}>
          <div
            style={{
              height: "100px",
              marginTop: "10%",
              marginBottom: "10%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Grid item md={12}>
              Régime IS
            </Grid>
            <Grid item md={12}>
              1 000000
            </Grid>
          </div>
        </Grid>
        <Grid item md={4} xs={4} className={props.style.borderR}>
          <div
            style={{
              height: "100px",
              marginTop: "10%",
              marginBottom: "10%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Grid item md={12}>
              Régime RSI
            </Grid>
            <Grid item md={12}>
              3 000000
            </Grid>
          </div>
        </Grid>
        <Grid item md={4} xs={4} className={props.style.borderR}>
          <div
            style={{
              height: "100px",
              marginTop: "10%",
              marginBottom: "10%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Grid item md={12}>
              Régime RN
            </Grid>
            <Grid item md={12}>
              1 000000
            </Grid>
          </div>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default GridBorderR;
