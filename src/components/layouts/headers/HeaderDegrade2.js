import { UseStylesHeaderDegrade2 } from "./HeaderDegrade2.style";
import Boutton from "../boutton/Boutton";
import { UseStylesBoutton } from "../boutton/Boutton.style";
import InputBases from "../input/InputBase";
import { UseStylesInputBases } from "../input/InputBase.style";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Grid } from "@material-ui/core";
const HeaderDegrade2 = (props) => {
  const classes = UseStylesHeaderDegrade2();
  return (
    <div className={classes.header}>
      {props.icons3?(
        <>

      <Grid container justify="space-between" alignItems="center" style={{color:"#fff",marginTop:'2%' }}>
        <Grid item md={4} alignContent="center" style={{ fontSize: "20px", fontWeight: "bold",color:"#fff" }}>
          {props.title2}
        </Grid>
        <Grid item md={8} container>
          <Grid item md={4}>
            <InputBases
              placeholder="Filtre par"
              style={UseStylesInputBases().inpt}
            />
          </Grid>

          <Grid item md={4}>
            <InputBases
              placeholder="Faire vos recherche ici"
              style={UseStylesInputBases().propertyStyle}
            />
          </Grid>
          <Grid item md={4} align="right" justify="right" >{props.icons3}</Grid>
        </Grid>
      </Grid>
        
        </>
      ):(
      <>
       <Grid
        item
        container
        alignItems="center"
        justify="center"
        alignContent="center"
      >
        <Grid item md={6} style={{color:"#fff" ,fontSize:"15px"}}>
          {props.title1}
        </Grid>

        <Grid
          container
          item
          md={6}
          align="right"
          justify="space-between"
          alignItems="center"
          alignContent="center"
        >
          <Grid item> {props.boutton1} </Grid>
          <Grid item> {props.boutton2} </Grid>
          <Grid item> {props.boutton3} </Grid>
        </Grid>
      </Grid>

      <Grid container justify="space-between" style={{color:"#fff",marginTop:'2%' }}>
        <Grid item md={4} style={{ fontSize: "30px", fontWeight: "bold",color:"#fff" }}>
          {props.title2}
        </Grid>
        <Grid item md={8} container>
          <Grid item md={4}>
            <InputBases
              placeholder="Filtre par"
              style={UseStylesInputBases().inpt}
            />
          </Grid>

          <Grid item md={6}>
            <InputBases
              placeholder="Faire vos recherche ici"
              style={UseStylesInputBases().propertyStyle}
            />
          </Grid>
          <Grid item md={1} align="left" >{props.icons1}</Grid>
          <Grid item md={1} align="left">{props.icons2}</Grid>
        </Grid>
      </Grid>
      </>)}
     
    </div>
  );
};

export default HeaderDegrade2;
