import {makeStyles} from '@material-ui/core';


export const UseStylesHeaderDegrade2= makeStyles((theme)=>({
  
    header:{
        background:'linear-gradient(30deg, #267466, #267466 60%,#378D7D 15px)',
        height:'200px',
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        width:'100%',
        justifyContent:'center',
        alignContent:'center',
        padding:'2em'

    },
    ul:{
        display:'flex',
        flexDirection:'row',
        width:'100%',
        listStyle:'none',
        justifyContent:'space-between',
        width:'95%',
        marginLeft:'auto',
        marginRight:'auto',
        color:"#fff",
        fontSize:'15px',
        alignContent:'center',
        alignItems:'center'
    },
    ul2:{
        display:'flex',
        flexDirection:'row',
        listStyle:'none',
        width:'100%',
        justifyContent:'space-between',
        alignItems:'center'
    },
    ull:{
        display:'flex',
        justifyContent:'space-between',
        listStyle:'none',
        color:'#fff',
        width:'95%',
        marginLeft:'auto',
        marginRight:'auto',
        alignContent:'center',
        alignItems:'center'

    },
    ull2:{
        display:'flex',
        flexDirection:'row',
        listStyle:'none',
        width:'60vw',
        justifyContent:'space-between',
    }
  
}))