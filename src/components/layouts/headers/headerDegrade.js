import { Button, Grid } from "@material-ui/core";
import { UseStylesHeaderDegrade } from "./headerDegrade.style";

const headerDegrade = (props) => {
  const classes = UseStylesHeaderDegrade();
  return (
    <div className={classes.header}>
      <Grid
        container
        alignItems="center"
        style={{ width: "95%", marginLeft: "auto", marginRight: "auto" }}
      >
        <Grid md={4} item alignItems="left" alignContent="left">
          <Grid align="left" className={classes.txt}>{props.title}</Grid>
        </Grid>

        <Grid item md={8} align="right" style={{ display: "flex" }} alignItems="center" alignContent="center" justify="center">
        <Grid item md={4}>
            {props.button3}
          </Grid>
          <Grid item md={4}>
            {props.listeExpertComptable}
          </Grid>

          <Grid item md={4}>
            {props.precedent}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default headerDegrade;
