import { makeStyles } from "@material-ui/core";

export const UseStylesHeaderDegrade=makeStyles((theme)=>({
 
    header:{
        background:'linear-gradient(30deg, #267466, #267466 60%,#378D7D 15px)',
        minHeight:'80px',
        display:'flex',
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        
        
    },
    headerContainer:{
        flexFlow:1,
        width:'90%',
        marginLeft:'auto',
        marginRight:'auto',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        
    },
    txt:{
        font:' normal normal medium 17px/20px Roboto',
        color:'#fff',
        fontSize:'15px'
    },
    row1Txt1:{
        fontWeight:'bold',
        font:' normal normal medium 17px/20px Roboto'
    },
    row1Txt2:{
        fontWeight:'small',
        color: '#686868',
        font:'normal normal medium 18px/22px Roboto'
    },
    btnPriv:{
        borderRadius:'10px 10px 10px 10px',
        color:'#fff',
        border:'1px solid white',
        textTransform:'capitalize',
    },
    
    headerBtn:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        width:'auto',
        alignItems:'center',
        justifyItem:'center'

    },
    content:{
        marginTop:theme.spacing(1),
        justifyContent:'space-between',
        background:'#00000057',
        
    },
    btnPreviews:{
        marginLeft:'2%'
    }


}))