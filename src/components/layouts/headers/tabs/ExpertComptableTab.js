import React from "react";
import { UseStylesExpertComptableTab } from "./ExpertComptableTab.style";
import { Tab, Tabs, AppBar } from "@material-ui/core";
/* import Tableau from '../../table/Table' */
const ExpertComptableTab = (props) => {
  const classes = UseStylesExpertComptableTab();

  const [value, setValue] = React.useState(0);
  const handleTabs = (e, val) => {
    e.preventDefault();
    setValue(val);
  };

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <Tabs
          onChange={handleTabs}
          value={value}
          style={{ outline: "none" }}
          classes={{
            indicator: classes.indicator,
          }}
        >
          <Tab
            className={classes.line}
            label={props.titre1}
            style={{ outline: "none" }}
          />
          <Tab
            className={classes.line}
            label={props.titre2}
            style={{ outline: "none" }}
          />
          <Tab
            className={classes.line}
            label={props.titre3}
            style={{ outline: "none" }}
          />
        </Tabs>

        <TabPanel value={value} index={0} style={{ outline: "none" }}>
          {" "}
          {props.table}{" "}
        </TabPanel>
        <TabPanel value={value} index={1} style={{ outline: "none" }}>
          {" "}
          {props.table2}{" "}
        </TabPanel>
        <TabPanel value={value} index={2} style={{ outline: "none" }}>
          {" "}
          {props.table3}{" "}
        </TabPanel>
      </div>
    </div>
  );
};

export default ExpertComptableTab;

function TabPanel(props) {
  const { children, value, index } = props;
  return (
    <div style={{ outline: "none" }}>
      {value === index && <div style={{ outline: "none" }}>{children}</div>}
    </div>
  );
}
