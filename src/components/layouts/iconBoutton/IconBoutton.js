import React from "react";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import IconButton from "@material-ui/core/IconButton";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import { UseStylesIconBoutton } from "./IconBoutton.style";
//Actif icoButton
export const Actif = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (props) => {
    setAnchorEl(null);
  };
  const classes = UseStylesIconBoutton();
  return (
    <div>
      <IconButton
        className={classes.icoBtn}
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        open={open}
        onClick={handleClick}
      >
        <MoreHorizIcon className={classes.icons} onClick={handleClick} />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.eyesIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.detail}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.pointIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.desactive}
            </Typography>
          </Link>
        </MenuItem>
      </Menu>
    </div>
  );
};

// En cours IconButton

export const EnCours = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = UseStylesIconBoutton();
  return (
    <div>
      <IconButton
        className={classes.icoBtn}
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        open={open}
        onClick={handleClick}
      >
        <MoreHorizIcon className={classes.icons} onClick={handleClick} />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <Link
            to="/profile"
            style={{ textDecoration: "none", color: "#757575" }}
          >
            <ListItemIcon>
              {" "}
              <img src={props.eyesIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.detail}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.relanceIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.relance}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.editeIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.edite}
            </Typography>
          </Link>
        </MenuItem>
      </Menu>
    </div>
  );
};

//innactif iconButton

export const InnActif = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = UseStylesIconBoutton();
  return (
    <div>
      <IconButton
        className={classes.icoBtn}
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        open={open}
        onClick={handleClick}
      >
        <MoreHorizIcon className={classes.icons} onClick={handleClick} />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.eyesIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.detail}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.pointIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.activer}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.editeIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.edite}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.supprimerIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.supprimer}
            </Typography>
          </Link>
        </MenuItem>
      </Menu>
    </div>
  );
};

export const Bloquer = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = UseStylesIconBoutton();
  return (
    <div>
      <IconButton
        className={classes.icoBtn}
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        open={open}
        onClick={handleClick}
      >
        <MoreHorizIcon className={classes.icons} onClick={handleClick} />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.eyesIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.detail}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.pointIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.debloque}
            </Typography>
          </Link>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Link to="#" style={{ textDecoration: "none", color: "#757575" }}>
            <ListItemIcon>
              {" "}
              <img src={props.editeIcon} />{" "}
            </ListItemIcon>
            <Typography variant="inherit" noWrap>
              {props.edite}
            </Typography>
          </Link>
        </MenuItem>
      </Menu>
    </div>
  );
};
