import {makeStyles} from '@material-ui/core';

export const UseStylesIconBoutton=makeStyles((theme)=>({
    root:{
        flexGrow:1,
        color:'#757575',
        outline:'none'
    },

    icons:{
        backgrounColor:'#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow:'0px 3px 6px #0000001A',
        opacity:1,
        borderRadius:'18px',
        outline:'none'

    },
    icoBtn:{
        outline:'none',
    }
}))