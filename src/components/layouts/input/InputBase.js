
import { InputAdornment } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
const InputBases= (props)=>{
    return(
        <div>
            <InputBase 
                className={props.style}
                variant={props.variant}
                placeholder={props.placeholder}
                type={props.type}
                InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        {props.endIcon}
                      </InputAdornment>
                    ),
                  }}
              
              />

        </div>
    )
}

export default InputBases;