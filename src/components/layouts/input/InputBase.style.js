import { makeStyles} from '@material-ui/core';

export const UseStylesInputBases=makeStyles((theme)=>({
    root:{
        flexGrow:1
    },
   
    propertyStyle:{ 
    width:'90%',
    color:'#fff',
    borderBottom:'1px solid #fff',
    fontStyle:'italic',
    opacity:0.5,
    fontSize:"13px"
},
    inpt:{
    border:'1px solid #fff',
    borderRadius:'18px',
    color:'#fff',
    padding:'5px',
    height:'35px',
    fontSize:"13px"
   
},
icons:{

}
}))