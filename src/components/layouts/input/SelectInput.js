import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {UseStylesSelectInput} from './SelectInput.style'

const SelectInput=({data})=>{
const classes=UseStylesSelectInput()


    const [role, setRole] = React.useState('');

    const handleChange = (event) => {
    setRole(event.target.value);
    };

    console.log("select data",data);


    return(
        <div className={classes.root}>

    <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="demo-simple-select-outlined-label">{data[0].title}</InputLabel>
       
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={10}
          onChange={handleChange}
          label={data[0].title}
        >
          {/* <MenuItem value="">  </MenuItem>
          <MenuItem value={10}>Membre du conseil</MenuItem>
          <MenuItem value={20}>Administration DGI</MenuItem>
          <MenuItem value={30}>Administration Expert comptable</MenuItem>
          <MenuItem value={40}>Administration comtribuable</MenuItem>
          <MenuItem value={50}>Trésorier</MenuItem>
          <MenuItem value={60}>Assistant</MenuItem> */}
           {data.map(el=><MenuItem value={el.id} >{el.text}</MenuItem>)}
        </Select>
      </FormControl>

        </div>
    )
}

export default SelectInput