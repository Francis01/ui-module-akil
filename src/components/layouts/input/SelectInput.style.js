import {makeStyles}  from '@material-ui/core';

export const UseStylesSelectInput=makeStyles((theme)=>({
    root:{
        flexGrow:1
    },
    formControl: {
        margin: theme.spacing(1),
        width: '25vw',
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
}))