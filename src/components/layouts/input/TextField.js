/* import {UseStylesTextField} from './TextField.style'; */
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
  } from '@material-ui/core/styles';
import { InputAdornment } from '@material-ui/core';

const TextFields = (props)=>{
    /* const classes=UseStylesTextField() */
    return(
        <div className={props.style}>
        <TextField
          type={props.type}
          className={props.style}
          placeholder={props.placeholder}
          variant={props.variant}
          label={props.label}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                {props.endIcon}
              </InputAdornment>
            ),
          }}
          

        />
      
        </div>
    )
}


export default TextFields;