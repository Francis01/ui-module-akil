import { makeStyles } from "@material-ui/core";

export const UseStylesTextField=makeStyles((theme)=>({
    root:{
        flexGrow:1
    },
    margin: {
        width:'25vw',
        background:"#FCFCFC"

        
       
      },
      formControl: {
        margin: theme.spacing(1),
        width: '25vw',
      },

      marginModif: {
        width:'25vw',
        marginTop:theme.spacing(0),
        background:"#FCFCFC"

        
       
      },
      
      property:{
          color:"#fff",
          textDecorationStyle:'italic',
          borderColor:'#fff',
          width:'200px'
      },

      profile:{
          width:'25vw',
          borderRadius:'18px',
          marginTop:theme.spacing(2)

          
      },
      form:{
        marginTop:theme.spacing(6),
        width:'60%',
        height:'auto'
      }
}))