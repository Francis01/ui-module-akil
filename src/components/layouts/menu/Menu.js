import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { IconButton, ListItemIcon, Typography } from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityIcon from "@material-ui/icons/Visibility";
import TelegramIcon from "@material-ui/icons/Telegram";
import { Link } from "react-router-dom";
const Menus = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link
          to="/DetailDuContribuable"
          style={{ textDecoration: "none" }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<VisibilityIcon />}</ListItemIcon>
            <Typography>Détails</Typography>
          </MenuItem>
        </Link>
        <MenuItem onClick={handleClose}>
          <ListItemIcon>{<TelegramIcon />}</ListItemIcon>
          <Typography>Relancer</Typography>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default Menus;
