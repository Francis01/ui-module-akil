import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Pagination } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
  color:{
    color: "#FF7A42 0% 0% no-repeat padding-box",


}
}));

 const PaginationRounded=()=> {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Pagination className={classes.color} count={10} variant="outlined" shape="rounded" />
    </div>
  );

}

export default PaginationRounded