import {makeStyles} from '@material-ui/core';

export const UseStylesCircular=makeStyles(({
    greenBackgroundColor:{
        background:'#3CC9AF30 0% 0% no-repeat padding-box',
        width:'85%',
        height:'85%',
        borderRadius:'50px',
        marginLeft:'auto',
        marginTop:'auto',
        marginBottom:'auto',
        marginRight:'auto',
        opacity:1
    },
    bleuBackgroundColor:{
        background:'#DAD7FE 0% 0% no-repeat padding-box',
        width:'85%',
        height:'85%',
        borderRadius:'50px',
        marginLeft:'auto',
        marginTop:'auto',
        marginBottom:'auto',
        marginRight:'auto',
        opacity:1
    },
    orangeBackgroundColor:{
        background:'#FFE5D3 0% 0% no-repeat padding-box',
        width:'85%',
        height:'85%',
        borderRadius:'50px',
        marginLeft:'auto',
        marginTop:'auto',
        marginBottom:'auto',
        marginRight:'auto',
        opacity:1
    },
    greenCircul:{
        background:'#3CC9AF',
        borderRadius:'100%',
        width:'14px',
        height:'14px',
        margin:'auto'
    },
    bleuCircul:{
        background:'#4339F2',
        borderRadius:'100%',
        width:'14px',
        height:'14px',
        margin:'auto'
    },
    orangeCircul:{
        background:'#FFB200',
        borderRadius:'100%',
        width:'14px',
        height:'14px',
        margin:'auto'
    },

}))