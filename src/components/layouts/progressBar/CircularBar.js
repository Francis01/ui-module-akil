import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {UseStylesCircular} from './Circular.style'
import { Grid } from '@material-ui/core';
import red from '@material-ui/core/colors/red';

function CircularProgressWithLabel1(props) {
    const primary = red[500];
    const classes=UseStylesCircular()
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" {...props}   style={{width:'100px',height:'100px',opacity:1 ,color:'#3CC9AF'}}/>
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
        className={classes.greenBackgroundColor}
      >
        <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

function CircularProgressWithLabel2(props) {
    const classes=UseStylesCircular()
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props}   style={{width:'100px',height:'100px',opacity:1,color:'#4339F2'}}/>
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
          className={classes.bleuBackgroundColor}  
        >
          <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
  }



function CircularProgressWithLabel3(props) {
    const classes=UseStylesCircular()
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props}  style={{width:'100px',height:'100px',opacity:1,color:'#FFB200'}}/>
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
          className={classes.orangeBackgroundColor}
  
        >
          <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
  }
/* */
export default function CircularBar(props) {
    const classes=UseStylesCircular()
  return (
      <Grid container spacing={2}  style={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
          <Grid xs={12}>{props.title} <span style={{color:'#267466'}}>{props.titlePrix}</span></Grid>
          <Grid item xs={4}>
              <CircularProgressWithLabel1 value={props.value1} />
              <Grid container style={{justifyContent:'center',alignItems:'center',textAlign:'center',margin:'auto'}}>
                    <Grid item xs={12}><div className={props.style.dashContribualePrix}>{props.prix1}</div></Grid>
                    <Grid item xs={2} style={{margin:'auto'}}><div className={classes.greenCircul}></div ></Grid>
                    <Grid item xs={10}><div>{props.legend1}</div></Grid>
              </Grid>
              

        </Grid>
          <Grid item xs={4}>
              <CircularProgressWithLabel2 value={props.value2} />

              <Grid container style={{justifyContent:'center',alignItems:'center',textAlign:'center',margin:'auto'}}>
                    <Grid item xs={12}><div className={props.style.dashContribualePrix}>{props.prix2}</div></Grid>
                    <Grid item xs={2} style={{margin:'auto'}}><div className={classes.bleuCircul}></div ></Grid>
                    <Grid item xs={10}><div>{props.legend2}</div></Grid>
              </Grid>
        </Grid>
          <Grid item xs={4}>
              <CircularProgressWithLabel3 value={props.value3} />
              <Grid container style={{justifyContent:'center',alignItems:'center',textAlign:'center',margin:'auto'}}>
                    <Grid item xs={12}><div className={props.style.dashContribualePrix}>{props.prix3}</div></Grid>
                    <Grid item xs={2} style={{margin:'auto'}}><div className={classes.orangeCircul}></div ></Grid>
                    <Grid item xs={10}><div>{props.legend3}</div></Grid>
              </Grid>
        </Grid>
          
        
        
        
      </Grid>
  
  );
}
