import { Grid } from "@material-ui/core";
import { useState } from "react";

const LegendAndTitle=(props)=>{
    const [isBackground,setIsBackground]=useState(true)
    return(
        <Grid container style={{padding:'1em'}}>
        
        {isBackground && !props.verif ?(
            <>
            <Grid item md={8} xs={10} align='left' style={{alignItems:'center'}}>{props.title}</Grid>
             <Grid item md={4} xs={4} container align='right' spacing={2}>
             <Grid item md={4} xs={4} container style={{alignItems:'center'}} >
                 <Grid item    className={props.style.BluelegendStyle}></Grid>
                 <Grid item md={8} className={props.style.legendTile}>{props.legend1}</Grid>
             </Grid>
             <Grid item md={4} xs={4} container style={{alignItems:'center'}}>
                 <Grid item    className={props.style.GreenlegendStyle}></Grid>
                 <Grid item md={8} className={props.style.legendTile}>{props.legend2}</Grid>  
             </Grid>
             <Grid item md={4} xs={4} container style={{alignItems:'center'}}>
                 <Grid item  className={props.style.OrangelegendStyle}></Grid>
                 <Grid item md={8} className={props.style.legendTile}>{props.legend3}</Grid>
             </Grid>
         </Grid>
         </>
        ):(
            <>
            <Grid item md={6} xs={10} align='left' style={{alignItems:'center'}}>{props.title}</Grid>
            <Grid item md={6} xs={4} container align='right' spacing={2} style={{background:'#FF7A421A'}}>

            <Grid item md={4} xs={4} container style={{alignItems:'center'}} >
                <Grid item    className={props.style.BluelegendStyle}></Grid>
                <Grid item md={6} className={props.style.legendTile}>{props.legend1}</Grid>
            </Grid>
            <Grid item md={4} xs={4} container style={{alignItems:'center'}} onClick={(e)=>{console.log('je suis')}}>
                <Grid item    className={props.style.GreenlegendStyle}></Grid>
                <Grid item md={6} className={props.style.legendTile}>{props.legend2}</Grid>  
            </Grid>
            <Grid item md={4} xs={4} container style={{alignItems:'center'}}>
                <Grid item  className={props.style.OrangelegendStyle}></Grid>
                <Grid item md={6} className={props.style.legendTile}>{props.legend3}</Grid>
            </Grid>
        </Grid>
            </>
            
        )}
       
    </Grid>
    )
}

export default LegendAndTitle;