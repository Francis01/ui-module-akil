import { makeStyles } from "@material-ui/core";

export const UseLegendAndTitleStyle= makeStyles (()=>({
    BluelegendStyle:{
        width:'15px',
        height:'15px',
        background:'#4339F2',
        borderRadius:'100%',
    },
    GreenlegendStyle:{
        width:'15px',
        height:'15px',
        background:'#3CC9AF',
        borderRadius:'100%',
    },
    OrangelegendStyle:{
        width:'15px',
        height:'15px',
        background:'#FFB200',
        borderRadius:'100%',
    },
    legendTile:{
        fontSize:'10px'
    }

}))