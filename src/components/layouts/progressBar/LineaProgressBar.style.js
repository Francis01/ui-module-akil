import {withStyles} from '@material-ui/core'
import LinearProgress from '@material-ui/core/LinearProgress';


export const BorderLinearProgress = withStyles((theme) => ({
    root: {
        flexGrow: 1,
        height: 10,
        borderRadius: 5,
    },
     colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
    bar: {
        borderRadius: 5,
        backgroundColor: '#ff5722',
    },
}))(LinearProgress);
