import { Table } from "react-bootstrap";
import Menus from "../menu/Menu";
import { UseStylesTable } from "./Table.style";
const DynamicTable = ({data,MuiIcons,txt}) => {
  return (
    <div style={{marginTop:"2%"}}>
      <Table striped style={{color:"#636363",fontSize:"10px"}}>
        <thead className={UseStylesTable().colorGreen}>
            {data.map(headerGroup=>(
                <tr {...headerGroup.headers}>
                    {headerGroup.headers.map(column=>(
                       <th {...column}  >{column}</th> 
                    ))}
                </tr>
            ))}
          <tr>
            
          </tr>

        
        </thead>
        <tbody >
        {data.map(headerGroup=>(
                <tr {...headerGroup.bodyData}>
                    {headerGroup.bodyData.map(column=>(
                       <th {...column} >{column} </th> 
                    ))}
                   <Menus MuiIcons={MuiIcons} txt={txt}/>
                </tr>
            ))}
          
        </tbody>
      </Table>
    </div>
  );
};

export default DynamicTable;
