import React from "react";
import { Table } from "react-bootstrap";
import Boutton from "../boutton/Boutton";
import { UseStylesBoutton } from "../boutton/Boutton.style";
import { Actif, InnActif, Bloquer, EnCours } from "../iconBoutton/IconBoutton";
import { UseStylesTable } from "./Table.style";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import eyesIcon from "../../../assert/interface (5).png";
import pointIcon from "../../../assert/Icon material-fiber-manual-record.png";
import pointActiverIcon from "../../../assert/Icon material-fiber-manual-record.png";
import editeIcon from "../../../assert/Icon awesome-pen.png";
import supprimerIcon from "../../../assert/rubbish-can.png";
import relanceIcon from "../../../assert/Icon open-reload.png";
const Tableau = (props) => {
  const classes = UseStylesTable();

  const { dataHeader, dataBody } = props;
  const H = dataHeader.map((el) => <th>{el}</th>);
  const B = dataBody.map((el) => <td>{el}</td>);
  return (
    <div>
      <Table striped bordered hover>
        <thead className={classes.theader}>
          <tr>
            <th>{<input type="checkbox" />}</th>
            {H}
          </tr>
        </thead>
        <tbody className={classes.tbody}>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
              {" "}
              <Actif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                desactive="Désactiver"
                pointIcon={pointIcon}
              />
            </td>
          </tr>

          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
              <Actif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                desactive="Désactiver"
                pointIcon={pointIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton
                style={UseStylesBoutton().bleuBtn}
                name="En cours"
              />{" "}
            </td>
            <td>
              <EnCours
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                relance="Relancer"
                relanceIcon={relanceIcon}
                edite="Editer"
                editeIcon={editeIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().bleuBtn} name="En cours" />
            </td>
            <td>
              <EnCours
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                relance="Relancer"
                relanceIcon={relanceIcon}
                edite="Editer"
                editeIcon={editeIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton
                style={UseStylesBoutton().orangeBtnTab}
                name="Inactif"
              />{" "}
            </td>
            <td>
              <InnActif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                activer="Activer"
                activerIcon={pointActiverIcon}
                edite="Editer"
                editeIcon={editeIcon}
                supprimer="Supprimer"
                supprimerIcon={supprimerIcon}
                pointIcon={pointIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton
                style={UseStylesBoutton().orangeBtnTab}
                name="Inactif"
              />{" "}
            </td>
            <td>
              <InnActif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                activer="Activer"
                activerIcon={pointActiverIcon}
                edite="Editer"
                editeIcon={editeIcon}
                supprimer="Supprimer"
                supprimerIcon={supprimerIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>
            {B}
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().redBtn} name="Bloqué" />{" "}
            </td>

            <td>
              <Bloquer
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                debloque="Débloquer"
                pointIcon={pointIcon}
                edite="Editer"
                editeIcon={editeIcon}
              />
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};
export default Tableau;
