import React from "react";
import { Table } from "react-bootstrap";
import Boutton from "../boutton/Boutton";
import { UseStylesBoutton } from "../boutton/Boutton.style";
import { Actif, InnActif } from "../iconBoutton/IconBoutton";
import { UseStylesTable } from "./Table.style";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import eyesIcon from "../../../assert/interface (5).png";
import pointIcon from "../../../assert/Icon material-fiber-manual-record.png";
import pointIcon1 from "../../../assert/Icon material-fiber-manual-record.png";
import pointActiverIcon from "../../../assert/Icon material-fiber-manual-record.png";
import editeIcon from "../../../assert/Icon awesome-pen.png";
import supprimerIcon from "../../../assert/rubbish-can.png";
import relanceIcon from "../../../assert/Icon open-reload.png";
const Tableau2 = (props) => {
  const classes = UseStylesTable();

  return (
    <div>
      <Table striped bordered hover>
        <thead className={classes.theader}>
          <tr>
            <th>{<input type="checkbox" />}</th>
            <th>IDU</th>
            <th>Matricule</th>
            <th>Raison sociale</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody className={classes.tbody}>
          <tr>
            <td>{<input type="checkbox" />}</td>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Akilcab</td>
            <td>infos@diaudes.com</td>
            <td>+225 00 000 000</td>
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
              {" "}
              <Actif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                desactive="Désactiver"
                pointIcon={pointIcon}
              />
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />}</td>

            <td>25968954</td>
            <td>1559082Z</td>
            <td>Strataige</td>
            <td>infos@diaudes.com</td>
            <td>+225 00 000 000</td>
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
              <Actif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                desactive="Désactiver"
                pointIcon={pointIcon}
              />
            </td>
          </tr>

          <tr>
            <td>{<input type="checkbox" />}</td>

            <td>25968954</td>
            <td>1559082Z</td>
            <td>Raouda</td>
            <td>infos@diaudes.com</td>
            <td>+225 00 000 000</td>
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
              <Actif
                style={{ outline: "none" }}
                detail="Détails"
                eyesIcon={eyesIcon}
                desactive="Désactiver"
                pointIcon={pointIcon}
              />
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};
export default Tableau2;
