import {Grid, Typography,InputBase ,Button, OutlinedInput} from '@material-ui/core'
import {useStyleAjouteDeContribuable} from './AjouteDeContribuable.style'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import clsx from 'clsx';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const AjouteDeContribuable=()=>{
    const classes=useStyleAjouteDeContribuable()
    return(
        <div className={classes.container}>
            <Grid container className={classes.menu} >
                <Grid item xs={6} className={classes.submenu}>
                    <Typography component='h6' variant='h6'>Tableau de bord / ajouter un contribuable</Typography>
                </Grid>
                <Grid item xs={6} className={classes.btncontainer}>
                    <Button startIcon={<ArrowBackIcon/>} className={classes.btn} > Précédent</Button>
                </Grid>

            </Grid>
            <Typography style={{marginLeft:'12%',color:'#757575',font:'28px/37px Roboto Slab',marginTop:'1%'}}>Ajouter un contribuable</Typography>
            <div className={classes.form}>
                <Grid container spacing={0}>

                    <Grid item xs={4}>
                     
                            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <FormHelperText id="outlined-weight-helper-text">NCC</FormHelperText>
                            <OutlinedInput
                                id="outlined-adornment-weight"
                                aria-describedby="outlined-weight-helper-text"
                                value='1111111B'
                                labelWidth={0}
                            />
                            </FormControl>

                    </Grid>

                    <Grid item xs={4}>
                     
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                <FormHelperText id="outlined-weight-helper-text">Régime fiscal</FormHelperText>
                                <OutlinedInput
                                    id="outlined-adornment-weight"
                                    aria-describedby="outlined-weight-helper-text"
                                    value='RSI'
                                    labelWidth={0}
                                />
                                </FormControl>
                        
                                 </Grid>

                                 <Grid item xs={4}>
                     
                                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <FormHelperText id="outlined-weight-helper-text">Email</FormHelperText>
                                    <OutlinedInput
                                        id="outlined-adornment-weight"
                                        aria-describedby="outlined-weight-helper-text"
                                        value='cont@gmail.com'
                                        labelWidth={0}
                                    />
                                    </FormControl>
             
                                 </Grid>
                </Grid>

                {/* block 2 */}


                <Grid container spacing={0}>

                    <Grid item xs={4}>
                     
                            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <FormHelperText id="outlined-weight-helper-text">RCCM</FormHelperText>
                            <OutlinedInput
                                id="outlined-adornment-weight"
                                aria-describedby="outlined-weight-helper-text"
                                value='BREXIT DUR'
                                labelWidth={0}
                            />
                            </FormControl>

                    </Grid>

                    <Grid item xs={4}>
                     
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                <FormHelperText id="outlined-weight-helper-text">Direction régionale</FormHelperText>
                                <OutlinedInput
                                    id="outlined-adornment-weight"
                                    aria-describedby="outlined-weight-helper-text"
                                    value='Sélectionner un régime'
                                    labelWidth={0}
                                />
                                </FormControl>
                        
                                 </Grid>

                                 <Grid item xs={4}>
                     
                                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <FormHelperText id="outlined-weight-helper-text">Statut</FormHelperText>
                                    <OutlinedInput
                                        id="outlined-adornment-weight"
                                        aria-describedby="outlined-weight-helper-text"
                                        value='CI-YAM-2020-B-2020'
                                        labelWidth={0}
                                    />
                                    </FormControl>
             
                                 </Grid>
                </Grid>

                {/* block3 */}

                <Grid container spacing={0}>

                    <Grid item xs={4}>
                     
                            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <FormHelperText id="outlined-weight-helper-text">IDU</FormHelperText>
                            <OutlinedInput
                                id="outlined-adornment-weight"
                                aria-describedby="outlined-weight-helper-text"
                                value='Sélectionner une région'
                                labelWidth={0}
                            />
                            </FormControl>

                    </Grid>

                    <Grid item xs={4}>
                     
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                <FormHelperText id="outlined-weight-helper-text">Services des impôts</FormHelperText>
                                <OutlinedInput
                                    id="outlined-adornment-weight"
                                    aria-describedby="outlined-weight-helper-text"
                                    value='+225 00 000 000'
                                    labelWidth={0}
                                />
                                </FormControl>
                        
                                 </Grid>

                                 <Grid item xs={4}>
                     
                                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <FormHelperText id="outlined-weight-helper-text">Adhérent CGA</FormHelperText>
                                    <OutlinedInput
                                        id="outlined-adornment-weight"
                                        aria-describedby="outlined-weight-helper-text"
                                        value='+225 00 000 000'
                                        labelWidth={0}
                                    />
                                    </FormControl>
             
                                 </Grid>
                </Grid>


                {/* block4 */}



                <Grid container spacing={0}>

                    <Grid item xs={4}>
                     
                            <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <FormHelperText id="outlined-weight-helper-text">Raison sociale</FormHelperText>
                            <OutlinedInput
                                id="outlined-adornment-weight"
                                aria-describedby="outlined-weight-helper-text"
                                value='has@gmail.com'
                                labelWidth={0}
                            />
                            </FormControl>

                    </Grid>

                    <Grid item xs={4}>
                     
                                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                <FormHelperText id="outlined-weight-helper-text">Contact mobile du dirigeant</FormHelperText>
                                <OutlinedInput
                                    id="outlined-adornment-weight"
                                    aria-describedby="outlined-weight-helper-text"
                                    value='Sélectionner un service'
                                    labelWidth={0}
                                />
                                </FormControl>
                        
                                 </Grid>

                                 <Grid item xs={4}>
                     
                                    <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                    <FormHelperText id="outlined-weight-helper-text">Type de mission</FormHelperText>
                                    <OutlinedInput
                                        id="outlined-adornment-weight"
                                        aria-describedby="outlined-weight-helper-text"
                                        value='+225 00 000 000'
                                        labelWidth={0}
                                    />
                                    </FormControl>
             
                                 </Grid>
                </Grid>

                <Grid container spacing={8} syle={{marginTop:'15%'}}>
                    <Grid item xs={6}>
                        <Button className={classes.btn1}>Annuler</Button>
                    </Grid>
                    <Grid item xs={6} style={{display:'flex', flexDirection:'row-reverse'}}>
                    <Button className={classes.btn2}>Suivant</Button>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export default AjouteDeContribuable;