import { makeStyles,withStyles } from '@material-ui/core/styles';

export const useStyleAjouteDeContribuable=makeStyles((theme)=>({

    container:{
        flexGrow:1,

    },
    menu:{
        width:'1920',
        height:'103px',
        backgroundColor:'#267466',
        position:'relative',
        
    },
    submenu:{
        fontSize:'18px',
        display:'flex',
        flexDirection:'column',
        color:'#fff',
        fontFamily:'roboto',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn:{
        border:'1px solid white',
        borderRadius:'24px',
        textTransform:'capitalize',
        color:'white',
        display:' flex',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'center',
        width:'150px'

    },
    btncontainer:{
        alignItems: 'center',
        justifyContent: 'center',
        display:' flex',
        flexDirection: 'row-reverse',
        
    },
    form:{
        position:'absolute',
        top:'160px',
        left:'140px',
        right:'155px',
        bottom:'137',
      /*   border:'0.5px solid #707070',
        borderRadius:'20px', */
        maxHeight:'470px',
        maxWidth:'1100px',
        color:'#f9f9f9'
    },
    margin: {
        margin: theme.spacing(1),
      },
      withoutLabel: {
        marginTop: theme.spacing(3),
      },
      textField: {
        width: '35ch',
        borderRadius:'11px',
        backgroundColor:'#fcfcfc'
      },
      btn1:{
          border:'1px solid #FF7A42',
          borderRadius:'35px',
          opacity:'1px',
          color:'#FF7A42',
          textTransform:'capitalize',
          width:'200px',
          height:' 50px',
          marginLeft:'4%'
      },
      btn2:{
          backgroundColor:'#FF7A42',
        border:'1px solid #fff',
        borderRadius:'35px',
        opacity:'1px',
        color:'#fff',
        textTransform:'capitalize',
        width:'200px',
        height:' 50px',
        marginRight:'4%',
        
        '&:hover':{
            backgroundColor:'#FF7A42'
        }
    }
}))