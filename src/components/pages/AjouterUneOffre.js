import {
  Grid,
  Typography,
  InputBase,
  Button,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Avatar,
  Badge,
} from "@material-ui/core";
import { useStyleAjouterUneOffre } from "./AjouterUneOffre.style";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import NavBar from "../layouts/NavBar";
import HeaderDegrade from "../layouts/headers/headerDegrade";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";

const AjouterUneOffre = () => {
  const classes = useStyleAjouterUneOffre();
  return (
    <div className={classes.container}>
      <NavBar />
      <HeaderDegrade
        title="Tableau de bord / Gestion des services"
        precedent={
          <Boutton
            name="Précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid container  style={{width:"95%",marginLeft:"auto",marginRight:"auto"}}>
        <Grid item md={12} xs={12}>
          {" "}
          Ajouter une offre
        </Grid>
        <Grid item md={12} xs={12} container>
          <Grid item md={4} xs={4}>
            <Grid item xs={6}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Libellé
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  placeholder="Libellé"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Prix (unité)
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  placeholder="Prix (unité)"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid item md={8} xs={8}>
            <Grid item xs={12}>
              <FormControl
                className={clsx(classes.margin, classes.textField2)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Libellé
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  placeholder="Libellé"
                  labelWidth={0}
                  rows={15}
                  multiline={true}
                />
              </FormControl>
            </Grid>
          </Grid>
        </Grid>

        <Grid container style={{ marginTop: "1%" }}>
          <Grid item xs={4}>
            <Button className={classes.btn1}>Annuler</Button>
          </Grid>
          <Grid item xs={4}>
            <Button className={classes.btn2}>Suivant</Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default AjouterUneOffre;
