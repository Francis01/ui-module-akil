import React from 'react';
import * as Mui from '@material-ui/core'
import {useStyleAttributionAutomatique} from './AttributionAutomatique.style'
import groupe11389 from '../../assert/Groupe 11389.png'

function AttributionAutomatique(){
    const classes=useStyleAttributionAutomatique()
    return(
        <div className={classes.container}>
            <Mui.Grid justify='center' >
                <Mui.Paper className={classes.paper}>

                <Mui.Grid item xs={12} >
              <Mui.Grid container alignItems='center' alignContent='center' justify='center' className={classes.inp}>
                  <Mui.Grid item xs={4} alignItems='center' className={classes.text} >Attribution automatique</Mui.Grid>
                  <Mui.Grid item xs={8} style={{maxWidth:'29%'}} container>
                      <img src={groupe11389} className={classes.img} />
                  </Mui.Grid>
              </Mui.Grid>
          </Mui.Grid>
          <Mui.Grid spacing={4} className={classes.text2}>
              <Mui.Grid item xs={6}>
                  <Mui.Typography  alignItems='center' justify='center'>
                  Voulez-vous faire une attribution <br/>automatique ?
                  </Mui.Typography>
              </Mui.Grid>
          </Mui.Grid>

          <Mui.Grid spacing={4} justify='center' alignContent='center' alignItems='center' className={classes.btnContainer}>
              <Mui.Grid item xs={6} spacing={4}>
              <Mui.Button variant='contained' className={classes.btn1}>Oui</Mui.Button>
              </Mui.Grid>
              <Mui.Grid item xs={6} spacing={4}>
              <Mui.Button variant='contained' className={classes.btn2}>Non</Mui.Button>
              </Mui.Grid>
              
             
          </Mui.Grid>

                </Mui.Paper>

            </Mui.Grid>
        </div>
    )
}

export default AttributionAutomatique;