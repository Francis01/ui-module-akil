import { makeStyles } from '@material-ui/core/styles';
export const useStyleAttributionAutomatique=makeStyles((theme)=>({

    container:{
        flexGrow:1,
        height:'100vh',
        backgroundColor:'gray',
        alignItems:'center',
        display:'flex',
        justifyContent: 'center',
        
    },
    paper:{
        width: '446px',
        maxHeight: '663px',
        boxShadow:'0px 10px 23px #00000029',
        borderRadius:'15px',
        display:'flex',
        flexDirection:'column'
     },
     text:{
         font:'normal normal medium 25px/30px Roboto',
         color:"#757575"
     },
 
    inp:{
        marginTop:theme.spacing(8),
        maxWidth: '826px',
        height: '123px',
        border:'0.5px solid #FF7A42',
        borderRadius:'11px',
        justifyContent:'center',
        alignItems:'center'
    },
    img:{
        marginTop:theme.spacing(-2)
    },
    text2:{display:"flex",flexDirection:'column' ,
    alignItems:'center',marginTop:'50px',textAlign:'center',
    color:'#757575',font:'normal normal normal 29px/35px Roboto'},

    btnContainer:{
        display:' flex',
        flexDirection: 'column',
        marginTop: '45px'
    },
    btn1:{
        backgroundColor:'#FF7A42',
        color:'#fff',
        textTransform:'capitalize',
        borderRadius:'35px',
        width:' 274px',
        height: '70px',
        '&:hover':{
            backgroundColor:'#FF7A42',
        }
    },
    btn2:{
        border:' 1px solid #FF7A42',
        color:'#FF7A42',
        textTransform:'capitalize',
        borderRadius:'35px',
        width:' 274px',
        height: '70px',
        marginTop:'40px',
        '&:hover':{
            backgroundColor:'#fff',
        }
    }

}))