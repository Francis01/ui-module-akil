import { makeStyles } from "@material-ui/core";
export const useStylesChart = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "50px",
    width: "95%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    borderRadius: "15px",
    height: "250px",
    position: "relation",
  },
  btn1: {
    fontSize: "10px",
    marginTop: "20px",
    backgroundColor: "#2DCE98",
    borderRadius: "15px",
    height: "20px",
    width: "50px",
    color: "#fff",
    marginLeft: theme.spacing(2),
    textAlign: "center",
    align: "center",
    justifyItems: "center",
    justifyContent: "center",
  },
  btn2: {
    backgroundColor: "#2DCE98 !important",
    borderRadius: "15px",
    height: "20px",
    width: "80px",
    color: "#fff",
    font: "normal normal bold 7px/9px Roboto",
    borderColor: "#2DCE98",
  },
  table: {
    minHeight: "100%",
    width: "100%",
    padding: "1em",
    borderRadius:"18px",
    boxShadow: "0px 2px 5px #ccc",
    boxSizing: "boder-box",
  },
  
  table2: {
    width: "95%",
    font: "normal normal normal 12px/14px Roboto",
    textAlign: "center",
    justifyContent: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },

  header: {
    background: "#FF7A42",
    color: "#fff",
  },
  textLinearP:{
    fontSize:"12px"
  }
}));
