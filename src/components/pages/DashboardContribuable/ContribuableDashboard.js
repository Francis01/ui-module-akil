import { useState } from "react";
import CircularBar from "../../layouts/progressBar/CircularBar";
import GridPaper from "../../layouts/grid/gridPaper";
import LineaProgressBar from "../../layouts/progressBar/LineaProgressBar";
import { Container, Grid, Paper } from "@material-ui/core";
import GridSimple from "../../layouts/grid/GridSimple";
import { UseGridSimpleStyle } from "../../layouts/grid/GridSimple.style";
import { UseStaticStyles } from "../Static.style";
import { UseCircularBarStyle } from "../../layouts/progressBar/CircularBar.style";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import ListeDesCGA from "../DashboardContribuable/ListeDesCGA";
import { UseListeDesCGAStyle } from "./ListeDesCGA.style";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";
import NavBar from "../../layouts/NavBar"
const ContribuableDashboard = (props) => {
  const [isValue, setIsValue] = useState(true);

  const data1 = [
    {
      header: [
        "IDA",
        "Numéro du contribuable",
        "Registre de commerce",
        "Raison sociale",
        "Mission",
        "Email",
        "Contacts",
      ],
      body: [
        "001",
        "1559082Z",
        "REG113569",
        "DiaudesCORP",
        "EXPERTISE",
        "info@diaudes@gmail.com",
        "+225 00 000 000",
      ],
    },
  ];
  const data2 = [
    {
      header: [
        "IDA",
        "Numéro du contribuable",
        "Registre de commerce",
        "Raison sociale",
        "Mission",
        "Email",
        "Contacts",
      ],
      body: [
        "002",
        "1559082A",
        "ROT113569",
        "AkilFrance",
        "Dynamique",
        "akil@diaudes@gmail.com",
        "+225 00 000 000",
      ],
    },
  ];

  return (
    <div >
      <NavBar/>
      
      <HeaderDegrade
        title="Tableau de bord / Contribuable"
        listeExpertComptable={
          <Link to="/liste-des-contibuables" style={{ textDecoration: "none" }}>
            <Boutton
              name="Liste des contribuables"
              style={UseStylesBoutton().orangeBtn}
            />
          </Link>
        }
        precedent={
          <Link to="/tableau-de-bord" style={{ textDecoration: "none" }}>
            <Boutton
              name="précédent"
              style={UseStylesBoutton().btnPriv}
              startIcon={<ArrowBackIcon />}
            />{" "}
          </Link>
        }
      />
      <Container style={{ marginTop: "2%" }}>
        <Grid container spacing={2}>
          <Grid item md={5}>
            <GridPaper
              headerTitle="Contribuable"
              headerTitlePrix="1200000"
              content1="Appartenant à des experts-comptables"
              content1Prix="32 0000"
              content2="Adhérents CGA"
              content2Prix="2000000"
              content3="Non affectés"
              content3Prix="68 0000"
              content1="Soumis à visa"
              content1Prix="47 000"
              style={UseStaticStyles()}
            />
          </Grid>

          <Grid md={7} spacing={2} container>
            <Grid md={4} item>
              <GridSimple
                title="Classification par régime RN"
                txt1="Contribuables"
                prix1="50000"
                txt2="Total produits"
                prix2="30000"
                txt3="État financier déposé"
                prix3="7000"
                style={UseGridSimpleStyle().borderGreen}
              />
            </Grid>

            <Grid md={4} item>
              <GridSimple
                title="Classification par régime RN"
                txt1="Contribuables"
                prix1="50000"
                txt2="Total produits"
                prix2="30000"
                txt3="État financier déposé"
                prix3="7000"
                style={UseGridSimpleStyle().borderBlue}
              />
            </Grid>

            <Grid md={4} item>
              <GridSimple
                title="Classification par régime RN"
                txt1="Contribuables"
                prix1="50000"
                txt2="Total produits"
                prix2="30000"
                txt3="État financier déposé"
                prix3="7000"
                style={UseGridSimpleStyle().borderOrange}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={4}>
            <Paper elevation={4} className={UseGridSimpleStyle().paper}>
              <Grid item container spacing={2}>
                <Grid item md={12} align="left">
                  Type de contrôle
                </Grid>
                <Grid item md={10} align="left">
                  Soumis à attestation
                </Grid>
                <Grid item md={2} align="right">
                  100
                </Grid>
                <Grid item md={12} align="right">
                  {" "}
                  <LineaProgressBar variant="determinate" value={50} />{" "}
                </Grid>
                <Grid item md={12}></Grid>

                <Grid item md={10} align="left">
                  Commissaire aux comptes
                </Grid>
                <Grid item md={2} align="right">
                  20
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={20} />{" "}
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid item md={4}>
            <Paper elevation={4} className={UseGridSimpleStyle().paper}>
              <CircularBar
                style={UseCircularBarStyle()}
                title="Valeur ajoutée par régime :"
                titlePrix="750 000 000 000 ( en FCFA)"
                value1={67}
                value2={46}
                value3={15}
                prix1="125 000 fcfa "
                prix2="75 000 fcfa "
                prix3="152 000 fcfa "
                legend1="Régime IS"
                legend2="Régime RSI"
                legend3="Régime RN"
              />
            </Paper>
          </Grid>

          <Grid item md={4}>
            <Paper elevation={4} className={UseGridSimpleStyle().paper}>
              <CircularBar
                style={UseCircularBarStyle()}
                title="Valeur ajoutée par régime :"
                titlePrix="750 000 000 000 ( en FCFA)"
                value1={67}
                value2={46}
                value3={15}
                prix1="125 000 fcfa "
                prix2="75 000 fcfa "
                prix3="152 000 fcfa "
                legend1="Régime IS"
                legend2="Régime RSI"
                legend3="Régime RN"
              />
            </Paper>
          </Grid>

          <Grid item md={5}>
            <Paper elevation={4} className={UseGridSimpleStyle().paper}>
              <Grid item container spacing={2}>
                <Grid item md={12} align="left">
                  Contribuable par référentiel comptable
                </Grid>
                <Grid item md={10} align="left">
                  Soumis à attestation
                </Grid>
                <Grid item md={2} align="right">
                  596
                </Grid>
                <Grid item md={12} align="right">
                  {" "}
                  <LineaProgressBar variant="determinate" value={85} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  CMA
                </Grid>
                <Grid item md={2} align="right">
                  495
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={20} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  BANQUAIRE
                </Grid>
                <Grid item md={2} align="right">
                  120
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={20} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  SGI
                </Grid>
                <Grid item md={2} align="right">
                  250
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={20} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  AUTRES
                </Grid>
                <Grid item md={2} align="right">
                  65
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar
                    /* style={{barColorPrimary: "green"}} */ variant="determinate"
                    value={20}
                  />{" "}
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid item md={7}>
            <Paper elevation={4} className={UseGridSimpleStyle().paper}>
              <Grid container>
                <Grid item md={6} align="left">
                  <Grid
                    item
                    md={12}
                    style={{
                      color: "#636363",
                      font: " normal normal medium 17px/20px Roboto",
                    }}
                  >
                    {" "}
                    Liste des CGA adhérents
                  </Grid>
                  <Grid
                    item
                    md={12}
                    style={{
                      color: "#CE332D",
                      font: "normal normal medium 14px/17px Roboto",
                    }}
                  >
                    Total{" "}
                    {isValue
                      ? "Adhérents" + ":" + data1.map((el) => el.body.length)
                      : "Non-Adhérents" +
                        ":" +
                        data2.map((el) => el.body.length)}
                  </Grid>
                </Grid>

                <Grid item md={6} align="right">
                  <Grid
                    item
                    container
                    style={{
                      border: "1px solid #FF7A42",
                      borderRadius: "10px",
                      width: "70%",
                      padding: "2%",
                    }}
                  >
                    <Grid align="left" item xs={6} md={6}>
                      <Boutton
                        name="Adhérents"
                        style={UseStylesBoutton().BtnContribuable}
                        click={() => {
                          setIsValue(true);
                        }}
                      />{" "}
                    </Grid>
                    <Grid align="right" item xs={6} md={6}>
                      <Boutton
                        name="Non-adhérents"
                        style={UseStylesBoutton().BtnContribuable}
                        click={() => {
                          setIsValue(false);
                        }}
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item md={12}>
                  {" "}
                  {!isValue ? (
                    <ListeDesCGA
                      style={UseListeDesCGAStyle().purpulHeader}
                      bodyData={UseListeDesCGAStyle().bodyData}
                      data={data1}
                    />
                  ) : (
                    <ListeDesCGA
                      bodyData={UseListeDesCGAStyle().bodyData}
                      style={UseListeDesCGAStyle().greenHeader}
                      data={data2}
                    />
                  )}
                </Grid>

                <Grid item md={12} align="center">
                  {" "}
                  <Boutton
                    name="Voir plus"
                    style={UseStylesBoutton().orangeBtn}
                  />{" "}
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default ContribuableDashboard;
