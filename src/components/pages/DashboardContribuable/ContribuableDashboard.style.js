import {makeStyles} from '@material-ui/core';

export const UseContribuableDashboardStyle =makeStyles((theme)=>({
    root:{
        flexGrow:1
    }
}))