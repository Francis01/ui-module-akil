import { Table } from "react-bootstrap";
const ListeDesCGA = ({ data,style,bodyData }) => {
  return (
    <div>
      <Table style={{fontSize:'12px'}}>
        <thead className={style}>
          <tr>
            <th>{data[0].header[0]}</th>
            <th>{data[0].header[1]}</th>
            <th>{data[0].header[2]}</th>
            <th>{data[0].header[3]}</th>
            <th>{data[0].header[4]}</th>
            <th>{data[0].header[5]}</th>
            <th>{data[0].header[6]}</th>
          </tr>
        </thead>
        <tbody className={bodyData}>
          <tr>
            <td>{data[0].body[0]}</td>
            <td>{data[0].body[1]}</td>
            <td>{data[0].body[2]}</td>
            <td>{data[0].body[3]}</td>
            <td>{data[0].body[4]}</td>
            <td>{data[0].body[5]}</td>
            <td>{data[0].body[6]}</td>
          </tr>
          <tr>
            <td>{data[0].body[0]}</td>
            <td>{data[0].body[1]}</td>
            <td>{data[0].body[2]}</td>
            <td>{data[0].body[3]}</td>
            <td>{data[0].body[4]}</td>
            <td>{data[0].body[5]}</td>
            <td>{data[0].body[6]}</td>
          </tr>
          <tr>
            <td>{data[0].body[0]}</td>
            <td>{data[0].body[1]}</td>
            <td>{data[0].body[2]}</td>
            <td>{data[0].body[3]}</td>
            <td>{data[0].body[4]}</td>
            <td>{data[0].body[5]}</td>
            <td>{data[0].body[6]}</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default ListeDesCGA;
