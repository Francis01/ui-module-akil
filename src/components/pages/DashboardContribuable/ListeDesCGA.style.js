import { makeStyles } from "@material-ui/core";
export const UseListeDesCGAStyle=makeStyles(()=>({
    greenHeader:{
        background:'#257163 0% 0% no-repeat padding-box',
        fontSize:'10px',
        color:'#fff',
        borderRadius:'18px',
        transition: 'all 2s ease-out',
    },
    purpulHeader:{
        background:'#3F4F84 0% 0% no-repeat padding-box',
        fontSize:'10px',
        color:'#fff',
        borderRadius:'18px',
        transition: 'all 2s ease-out',
    },
     
    bodyData:{
        color:'#757575',
        font:' normal normal medium 11px/13px Roboto',
        transition: 'all 2s ease-out',
    }
}))