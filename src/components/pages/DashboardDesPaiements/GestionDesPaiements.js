import Boutton from "../../layouts/boutton/Boutton";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import TableGestionDesPaiement from "./TableGestionDesPaiements";
import { Grid, Paper } from "@material-ui/core";
import { UseStaticStyles } from "../Static.style";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import NavBar from "../../layouts/NavBar";

const GestionDesPaiements = () => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / gestion des paiements"
        title2="Gestion des paiements"
        icons1={<LocalPrintshopIcon fontSize="large" />}
        icons2={<PictureAsPdfIcon fontSize="large" />}
      />

      <Grid container spacing={2} style={{width:"95%",marginLeft:"auto",marginRight:"auto",margin:"-2% 0px 1% 2%"}}>
        <Grid item md={4} xs={4}>
          <Link to="/Liste-des-offres-vendues" style={{textDecoration:"none",color:'inherit'}}>
             <Paper elevation={4} style={{borderRadius:"18px",padding:"2em"}} >
            <Grid item md={12} xs={12} style={{color:"#FF7A42",font: "normal normal medium 25px/30px Roboto",fontWeight:"bold"}}>
              Nombre d'offres vendues
            </Grid>
            <Grid item md={12} xs={12}>
              8000
            </Grid>
          </Paper>
          </Link>
         

        </Grid>

        <Grid item md={4} xs={4}>
          <Paper elevation={4} style={{borderRadius:"18px",padding:"2em"}}>
            <Grid item md={12} xs={12} style={{color:"#FF7A42",font: "normal normal medium 25px/30px Roboto",fontWeight:"bold"}}>
              Montant total des offres vendues (FCFA)
            </Grid>
            <Grid item md={12} xs={12}>
              80 000 000 000
            </Grid>
          </Paper>
        </Grid>

        <Grid item md={4} xs={4}>
          <Paper elevation={4} style={{borderRadius:"18px",padding:"2em"}}>
            <Grid item md={12} xs={12} style={{color:"#FF7A42",font: "normal normal medium 25px/30px Roboto",fontWeight:"bold"}}>
              Nombre d'experts-comptables abonnés
            </Grid>
            <Grid item md={12} xs={12}>
              200 000
            </Grid>
          </Paper>
        </Grid>
      </Grid>

      <Paper
        className={UseStaticStyles().paper}
        style={{
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <TableGestionDesPaiement />
      </Paper>
    </div>
  );
};
export default GestionDesPaiements;

