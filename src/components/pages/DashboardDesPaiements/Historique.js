import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import TableGestionDesPaiement from "./TableGestionDesPaiements";
import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Paper } from "@material-ui/core";
import { UseStaticStyles } from "../Static.style";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import MenuIconHistorique from "./MenuIconHistorique";
const Historique = () => {
  return (
    <div>
      <HeaderDegrade2
        title1="Tableau de bord / gestion des paiements"
        title2="Historique"
        icons1={<LocalPrintshopIcon />}
        icons2={<PictureAsPdfIcon />}
        boutton3={<Boutton name="Précédent" startIcon={<ArrowBackIcon/>} style={UseStylesBoutton().btnPriv}/>}
      />

      <Paper
        className={UseStaticStyles().paper}
        style={{
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "-2%",
        }}
      >
        <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Réf</th>
            <th>Nom</th>
            <th>Prénoms</th>
            <th>Date</th>
            <th>Mode de paiement</th>
            <th>Nombre d'unités</th>
            <th>Montant total</th>
            <th>offres</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="Validé" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <MenuIconHistorique  />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="Rejeté" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="En cours" style={UseStylesBoutton().orangeBtnTab} />
            </th>
            <th>
            </th>
          </tr>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="Rejeté" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
            </th>
          </tr>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="En cours" style={UseStylesBoutton().orangeBtnTab} />
            </th>
            <th>
            </th>
          </tr>
        </tbody>
      </Table>
      </Paper>
    </div>
  );
};
export default Historique;
