import React, { useState } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Grid, IconButton, ListItemIcon, Typography } from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import Groupe from "../../../assert/Groupe 12252.png";
import { Link } from "react-router-dom";
import DialogContent from "../../layouts/Dialogs/DialogContent";
import Popup from "../../layouts/Dialogs/simpleDialog";
import BlockIcon from '@material-ui/icons/Block';
import DoneIcon from '@material-ui/icons/Done';
const MenuIconHistorique = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [openPopup, setOpenPopup] = useState(false);

  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
       
            <Link
              to="#"
              style={{
                textDecoration: "none",
                textTransform: "none",
                color: "#757575",
              }}
            >
              <MenuItem onClick={handleClose}>
                <ListItemIcon>{<EditIcon />}</ListItemIcon>
                <Typography>Editer</Typography>
              </MenuItem>
            </Link>
          
     
            <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}>
              <DialogContent
                imageText="Validation paiement"
                textInterrogative="Valider"
                button1={
                  <Boutton
                    name="continuer"
                    style={UseStylesBoutton().comptableBtnOrange}
                    click={() => setOpenPopup(false)}
                  />
                }
                image={Groupe}
              />
            </Popup>
      </Menu>
    </div>
  );
};
export default MenuIconHistorique;
