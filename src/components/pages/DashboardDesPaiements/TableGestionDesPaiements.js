import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import MenuIconGestionDePaiement from "./MenuIconGestionDePaiement";
const TableGestionDesPaiement = () => {
  const [enCours,setEnCours]=useState(false)
  const [isOne,setIsOne]=useState(false)
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Réf</th>
            <th>Nom</th>
            <th>Prénoms</th>
            <th>Date</th>
            <th>Mode de paiement</th>
            <th>Nombre d'unités</th>
            <th>Montant total</th>
            <th>offres</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="Validé" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <MenuIconGestionDePaiement isOne={()=>setIsOne(true)} />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="Rejeté" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIconGestionDePaiement />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>Signature</th>
            <th>
              <Boutton name="En cours" style={UseStylesBoutton().orangeBtnTab} />
            </th>
            <th>
              <MenuIconGestionDePaiement enCours={()=>setEnCours(true)} />
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableGestionDesPaiement;
