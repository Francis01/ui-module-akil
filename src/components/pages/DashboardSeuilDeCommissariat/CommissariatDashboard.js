import { Grid, Paper } from "@material-ui/core";
import GridPaper from "../../layouts/grid/gridPaper";
import { UseStaticStyles } from "../Static.style";
import GridBorderR from "../../layouts/grid/gridWithBorderR/GridBorderR";
import CircularBar from "../../layouts/progressBar/CircularBar";
import { UseCircularBarStyle } from "../../layouts/progressBar/CircularBar.style";
import LineaProgressBar from "../../layouts/progressBar/LineaProgressBar";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import DynamicTable from "../../layouts/table/DynamicTable";
import VisibilityIcon from "@material-ui/icons/Visibility";
import TelegramIcon from "@material-ui/icons/Telegram";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import { Link } from "react-router-dom";
const CommissariatDashboard = () => {
  const tbData = [
    {
      headers: [
        "Forme juridique",
        "IDU",
        "Nuréro du contribuable",
        "Raison sociale",
        " Email",
        "Expert-comptable",
        "Action",
      ],
      bodyData: [
        "SARL",
        "25968954",
        "128556U",
        "Diaud Corp",
        "info@gmail.com",
        "Strataige SA",
      ],
    },
  ];
  const MuiIcons = [[VisibilityIcon, TelegramIcon]];
  const txt = ["Détails", "Relancer"];

  return (
    <div>
      <HeaderDegrade
        title="Tableau de bord/Seuil de commissariat"
        listeExpertComptable={
          <Link to="/listDesContibuables" style={{ textDecoration: "none" }}>
            <Boutton
              name="Liste des contribuables"
              style={UseStylesBoutton().orangeBtn2}
            />
          </Link>
        }
        precedent={
          <Boutton
            name="précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid
        container
        spacing={2}
        style={{ marginLeft: "auto", marginRight: "auto", marginTop: "2%" }}
      >
        <Grid container item md={6} spacing={2}>
          <Grid item md={12} xs={12}>
            <GridPaper
              headerTitle="Seuil de commissariat"
              headerTitlePrix="7000"
              content1="Nombre de contribuables qui doivent changer de régime de contrôle"
              content1Prix="7000"
              style={UseStaticStyles()}
            />
          </Grid>
          <Grid item md={12} xs={12}>
            <GridBorderR style={UseStaticStyles()} />
          </Grid>
        </Grid>

        <Grid item md={6} xs={12} container>
          <Paper className={UseStaticStyles().paper}>
            <Grid container>
              <Grid item md={8} align="left">
                <Grid item md={12}>
                  Liste des contribuables ayant dépassé le seuil
                </Grid>
                <Grid item md={12} style={{ color: "#CE332D" }}>
                  Total contribuables : 7000
                </Grid>
              </Grid>

              <Grid item md={4} align="right">
                <Boutton
                  name="Voir plus "
                  style={UseStylesBoutton().orangeBtn}
                />
              </Grid>
            </Grid>
            <Grid container item>
              <DynamicTable data={tbData} MuiIcons={MuiIcons} txt={txt} />
            </Grid>
          </Paper>
        </Grid>
      </Grid>

      <Grid container spacing={2} style={{ marginTop: "1%" }}>
        <Grid item md={6} xs={12}>
          <Paper className={UseStaticStyles().paper}>
            <CircularBar
              style={UseCircularBarStyle()}
              title="Nombre de contribuables ayant dépassé le seuil CAC (%)"
              value1={67}
              value2={46}
              value3={15}
              legend1="Régime IS"
              legend2="Régime RSI"
              legend3="Régime RN"
            />
          </Paper>
        </Grid>

        <Grid item md={6} xs={12}>
          <Paper className={UseStaticStyles().paper}>
            <Grid item container spacing={2}>
              <Grid item md={12} align="left">
                Nombre de contribuables ayant dépassé le seuil CAC par forme
                juridique
              </Grid>
              <Grid item md={4} container>
                <Grid item md={10} align="left">
                  SARL
                </Grid>
                <Grid item md={2} align="right">
                  100
                </Grid>
                <Grid item md={12} align="right">
                  <LineaProgressBar variant="determinate" value={50} />{" "}
                </Grid>
                <Grid item md={10} align="left">
                  SA
                </Grid>
                <Grid item md={2} align="right">
                  120
                </Grid>
                <Grid item md={12} align="right">
                  <LineaProgressBar variant="determinate" value={55} />{" "}
                </Grid>
                <Grid item md={10} align="left">
                  SAS
                </Grid>
                <Grid item md={2} align="right">
                  98
                </Grid>
                <Grid item md={12} align="right">
                  <LineaProgressBar variant="determinate" value={60} />{" "}
                </Grid>
              </Grid>

              <Grid item md={4} container>
                <Grid item md={10} align="left">
                  SNC
                </Grid>
                <Grid item md={2} align="right">
                  500
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={45} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  SCOOP
                </Grid>
                <Grid item md={2} align="right">
                  625
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={70} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  GIE
                </Grid>
                <Grid item md={2} align="right">
                  23
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={70} />{" "}
                </Grid>
              </Grid>

              <Grid item md={4} container>
                <Grid item md={10} align="left">
                  individuel
                </Grid>
                <Grid item md={2} align="right">
                  821
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={90} />{" "}
                </Grid>

                <Grid item md={10} align="left">
                  Autres
                </Grid>
                <Grid item md={2} align="right">
                  3
                </Grid>
                <Grid item md={12}>
                  {" "}
                  <LineaProgressBar variant="determinate" value={3} />{" "}
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default CommissariatDashboard;
