import {useStaylesDetailDeLexpertComptable } from '../pages/DetailDeLexpertComptable.style'
import {Avatar, Button, IconButton, Paper} from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PrintIcon from '@material-ui/icons/Print';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import assetou from '../../assert/assetou.png'
import { Col, Container, Row,Table } from 'react-bootstrap';
import NavBar from '../layouts/NavBar';
const DetailDeLexpertComptable=()=>{
    const classes=useStaylesDetailDeLexpertComptable()
    return(
        <div className={classes.root}>
            <NavBar/>
            <div className={classes.header}> 
                <div className={classes.headerTxt}>
                    <div className={classes.txt}>
                     Tableau de bord /liste des contribuables./Coulibaly Assetou
                     </div>
                </div>
                <div className={classes.headerBtn}>
                    <div className={classes.btnPreviews}>
                        <Button className={classes.btnPriv} startIcon={<ArrowBackIcon/>}>Précédent</Button>
                    </div>
                </div>
            </div>
            <Container>

            <div className={classes.content}>
            
                <Row className={classes.rowContainer}>
                <Col  xs={3}>
                        <div className={classes.profile}>
        
                        <p style={{font:'normal normal medium 28px/37px Roboto Slab',color:'#757575'}}>Detail de l’expert comptable</p>
                        <IconButton>
                            <Avatar src={assetou} className={classes.avater}/>
                        </IconButton>
                        <p style={{font:'normal normal bold 17px/20px Roboto',color:'#707070'}}> Coulibaly Assétou</p>
                        <p style={{font:'normal normal 300 17px/20px Roboto',color:'#707070',fontSize:'15px'}}>Contribuable à Akilcab</p>
                        <p style={{font:'normal normal bold 17px/20px Roboto',color:'#707070',fontWeight:'bold'}}>has@gmail.com</p>
                        <hr/>
                        <Button variant='contained' className={classes.btnModif}>Modifier ce contribuable</Button>
                        </div>

                    
                    
            </Col>
           

            <Col xs={9} > 
                    <Row >
                        <Col className={classes.sidebar}>
                            <p className={classes.liassesHeader}>Informations sur les liasses</p>
                            <Col>

                            <Row >
                            <Col xs={3} style={{borderRight:'0.20000000298023224px solid #7B7B7B'}}>
                            <p className={classes.row1Txt1}>En cours</p>
                            <p className={classes.row1Txt2}>120000</p>

                            </Col>
                            <Col xs={3} style={{borderRight:'0.20000000298023224px solid #7B7B7B'}}>
                            <p  className={classes.row1Txt1}>Transmits</p>
                            <p  className={classes.row1Txt2}>750000</p>
                            </Col>
                            <Col xs={3} style={{borderRight:'0.20000000298023224px solid #7B7B7B'}}>
                            <p  className={classes.row1Txt1}>Rejetés</p>
                            <p  className={classes.row1Txt2}>598000</p>
                            </Col>

                            <Col xs={3} >
                            <p  className={classes.row1Txt1}>Validés</p>
                            <p  className={classes.row1Txt2}>5012</p>
                            </Col>

                            </Row>
                            
                            </Col>


                        </Col>
                        
                    </Row>


                    <Row className={classes.row2}  >
                        <Col xs={3} >
                            <div className={classes.card} >
                            <p className={classes.div1}>Informations sur les dossiers</p>
                            <p className={classes.div2}>Nombres de dossiers</p>
                            <p className={classes.div3}>598000</p>
                            </div>
                        </Col>
                        <Col xs={3} >
                            <div className={classes.card}>
                            <p className={classes.div1}>Informations sur les attestations</p>
                            <p className={classes.div2}>Attestation édité</p>
                            <p className={classes.div3}>2500000</p>
                            </div>
                        </Col>
                        <Col xs={3} >
                            <div className={classes.card}>
                            <p className={classes.div1}>Informations sur les honoraires déclarés</p>
                            <p className={classes.div2}>Honoraires déclarés</p>
                            <p className={classes.div3}>12000000</p>
                            </div>
                        </Col>
                        <Col xs={3} >
                            <div className={classes.card}>
                            <p className={classes.div1}>Informations chiffres d'affaire</p>
                            <p className={classes.div2A} >Chiffres d'affaires déclarés</p>
                            <p className={classes.div3}>12000000</p>
                            </div>
                        </Col>
                    </Row>
                    <Row className={classes.row3}>
                    <Col className={classes.textab}>
                    <div>Liste des dossiers</div>
                    <div >
                        <IconButton><PrintIcon style={{color:'#fff'}}/></IconButton>
                        <IconButton><PictureAsPdfIcon style={{color:'#fff'}}/></IconButton>
                    </div>
                    
                    </Col>
                    <Table striped bordered hover className={classes.table}>
                    <thead className={classes.theader}>
                        <tr>
                        <th>IDU</th>
                        <th>Numéro compte contribuable</th>
                        <th>Registre de commerce</th>
                        <th>Raison sociale</th>
                        <th>Contact</th>
                        <th>Type de mission</th>
                        </tr>
                    </thead>
                    <tbody className={classes.tbody}>
                        <tr>
                        <td>022</td>
                        <td>1111111B</td>
                        <td>225 01458672</td>
                        <td>Gniré Aïssata SARL</td>
                        <td>02898696</td>
                        <td>
                            <Button variant='contained' className={classes.statut}>COMMISSARIAT</Button>
                        </td>
                        </tr>
                        <tr>
                        <td>022</td>
                        <td>1111111B</td>
                        <td>225 01458672</td>
                        <td>Gniré Aïssata SARL</td>
                        <td>02898696</td>
                        <td>
                        <Button variant='contained' className={classes.statut}>COMMISSARIAT</Button>
                        </td>
                        </tr>
                       
                    </tbody>
                    </Table>

                    </Row>
                        


            </Col>

               
            </Row>
            
            
                
            </div>
            </Container>

            
        </div>
    )
}
export default DetailDeLexpertComptable;