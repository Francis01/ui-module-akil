import { makeStyles } from "@material-ui/core";

export const useStaylesDetailDeLexpertComptable=makeStyles((theme)=>({
    root:{
        flexFlow:1,
        height:'100vh',
        display:'flex',
        flexDirection:'column',
        textAlign:'center',
        /* 
        alignItems:'center',
        justifyContent:'center' */
    },
    header:{
        background:'linear-gradient(30deg, #267466, #267466 70%,#378D7D 15px)',
        minHeight:'10%',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        width:'100%'
        
    },
    txt:{
        font:' normal normal medium 17px/20px Roboto',
        color:'#fff',
        fontSize:'10px'
    },
    row1Txt1:{
        fontWeight:'bold',
        font:' normal normal medium 17px/20px Roboto'
    },
    row1Txt2:{
        fontWeight:'small',
        color: '#686868',
        font:'normal normal medium 18px/22px Roboto'
    },
    btnPriv:{
        borderRadius:'10px 10px 10px 10px',
        color:'#fff',
        border:'1px solid white',
        textTransform:'capitalize',
    },
    content:{
        /* display:'flex',
        flexDirection:'row', */
        marginTop:theme.spacing(1),
        justifyContent:'space-between',
        
    },
    profile:{
        width: 'auto',
        height: '80vh',
        background:'#F9F9F9 0% 0% no-repeat padding-box',
        borderRadius:'20px',
        border:'0.5px solid #C7C7C7',
        

    },
    avater:{
        width: '159px',
        height: '159px',
    },
    btnModif:{
        background:'#FF7A42 0% 0% no-repeat padding-box',
        textTransform:'capitalize',
        borderRadius:'30px',
        color:'#fff',
        " &:hover":{
            background:'#FF7A42 0% 0% no-repeat padding-box'
        }
    },
    liassesHeader:{
        font:'normal normal medium 17px/20px Roboto',
        borderRadius:'18px ',
        boxShadow:'0px 6px 22px #0000000D',
        background:'#FFFFFF 0% 0% no-repeat padding-box',
        color:'#FF7A42'
    },
    sidebar:{
        background:'#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow:'0px 6px 22px #0000000D',
        border:' 0.20000000298023224px solid #C7C7C7',
        borderRadius:'18px'
    },
    card:{
        boxShadow:'0px 6px 22px #0000000D',
        border:' 0.20000000298023224px solid #C7C7C7',
        borderRadius:'18px',
        marginTop:'auto',
        marginBottom:'auto',
        height:'120px'
    },
    row2:{
        marginTop:theme.spacing(2)
    },
    row3:{
        marginTop:theme.spacing(1)
    },
    div1:{
        font:' normal normal medium 17px/20px Roboto',
        color: '#FF7A42',
        fontSize:'10px'
    },
    div2:{
        font: 'normal normal medium 22px/27px Roboto',
        color: '#484848',
        opacity: '1',
        fontWeight:'bold',
        fontSize:'15px'
    },
    div2A:{
        font: 'normal normal medium 22px/27px Roboto',
        color: '#484848',
        opacity: '1',
        fontWeight:'bold',
        fontSize:'13px'
    },
    div3:{
        font:' normal normal medium 40px/48px Roboto',
        color: '#686868',
        fontSize:'30px',
        fontWeight:'bold',
    },
    table:{
        boxShadow: '0px 6px 22px #2C282812',
        borderRadius:'10px 10px 0px 0px',
        opacity: 1,
        font:' normal normal bold 17px/20px Roboto',
        color:' #757575',
        letterSpacing: '0px',
        
    },
    theader:{
        font:'normal normal medium 14px/19px Roboto Slab',
        fontSize:'10px'
    },
    tbody:{
        fontSize:'10px'
    },
    statut:{
        background: '#2DA3CE 0% 0% no-repeat padding-box',
        width: '100px',
        height: '20px',
        borderRadius:'30px',
        color:'#fff',
        fontSize:'10px',
        '&:hover':{
            background: '#2DA3CE 0% 0% no-repeat padding-box',
        }
    },
    textab:{
        background: '#378D7D 0% 0% no-repeat padding-box',
        border: '1px solid #C7C7C7',
        borderRadius:' 10px 10px 0px 0px',
        color:'#fff',
        justifyContent:'space-between',
        display:'flex'

    }

}))