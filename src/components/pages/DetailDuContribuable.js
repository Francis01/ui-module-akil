import React, { useState } from "react";
import {
  Grid,
  Button,
  InputBase,
  IconButton,
  FormControl,
  Typography,
  InputAdornment,
  Paper,
} from "@material-ui/core";
import {
  UseStyleDetailDuContribuable,
  StyledTableCell,
  StyledTableRow,
} from "./DetailDuContribuable.style";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import strataige from "../../assert/Groupe 11580.png";
import TelegramIcon from "@material-ui/icons/Telegram";
import HeaderDegrade2 from "../layouts/headers/HeaderDegrade2";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";
import { UseStaticStyles } from "./Static.style";
import NavBar from "../layouts/NavBar";

function DetailDuContribuable() {
  const classes = UseStyleDetailDuContribuable();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <NavBar/>
      
      <HeaderDegrade2
        title1="Tableau de bord / liste des contribuables / Coulibaly Assetou"
        title2="Détail du contribuable"
        icons1={<PrintIcon fontSize="large" />}
        icons2={<PictureAsPdfIcon fontSize="large"/>}
        boutton3={
          <Boutton
            name="précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid container spacing={0} className={classes.root}>
        <Grid item md={4} xs={4} className={classes.firstGrid}>
          <Grid md={12} xs={12} item className={classes.div}>
            <Paper className={classes.paper}>
              <Grid container item md={12} xs={12}>
                <Grid item md={6} xs={6}>
                  
                  <strong className={classes.strong}>Forme juridique</strong>
                </Grid>
                <Grid item md={6} xs={6} className={classes.span}>
                  S.A.R.L
                </Grid>
                <Grid item md={6} xs={6}>
                 <strong className={classes.strong}> IDU</strong>
                </Grid>
                <Grid item md={6} xs={6} className={classes.span}>
                  02228965
                </Grid>
                <Grid item md={6} xs={6}>
                <strong className={classes.strong}>  NCC</strong>
                </Grid>
                <Grid item md={6} xs={6} className={classes.span}>
                  NCC
                </Grid>
                <Grid item md={6} xs={6}>
                   <strong className={classes.strong}>Raison sociale</strong>
                </Grid>
                <Grid item md={6} xs={6} className={classes.span}>
                  Strataige S.A
                </Grid>
                <Grid item md={6} xs={6}>
                   <strong className={classes.strong}>Expert-comptable</strong>
                </Grid>
                <Grid md={6} xs={6} className={classes.span}>
                  Diaudes Corp
                </Grid>
                <Grid item md={6} xs={6}>
                   <strong className={classes.strong}>Statut</strong>
                </Grid>
                <Grid item md={6} xs={6}>
                  <Boutton name="Activé" style={UseStylesBoutton().greenBtn} />
                </Grid>
                <Grid item md={6} xs={6}>
                 <strong className={classes.strong}>  CGA</strong>
                </Grid>
                <Grid item md={6} xs={6}>
                  <Boutton
                    name="Adhérent"
                    style={UseStylesBoutton().greenBtn}
                  />
                </Grid>
                <Grid item md={12} xs={12} align="center" style={{marginTop:"5%"}}>
                  <Boutton
                    name="Modifier ce contribuable"
                    style={UseStylesBoutton().orangeBtn2}
                  />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>

        <Grid item md={8} xs={8} container className={classes.secondGrid}>
          <Grid item md={6} xs={6} className={classes.div2} align="center">
            <Grid md={12} xs={12} item align="center" className={classes.title}>
              Informations financières
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total bilan :</strong> <span className={classes.span}>600000000000245</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total chiffre d'affaire :</strong> <span className={classes.span}>123500000000</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total produit :</strong> <span className={classes.span}>235550000</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Résultat NET :</strong> <span className={classes.span}>2055515221</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total éffectif :</strong> <span className={classes.span}>5555551114414411</span>
            </Grid>
          </Grid>
          <Grid item md={6} xs={6} className={classes.div2} align="center" style={{borderLeft:"1px solid #ccc"}}>
            <Grid md={12} xs={12} item align="center" className={classes.title}>
              Informations financières
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total bilan :</strong> <span className={classes.span}>600000000000245</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total chiffre d'affaire :</strong> <span className={classes.span}>123500000000</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total produit :</strong> <span className={classes.span}>235550000</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Résultat NET :</strong> <span className={classes.span}>2055515221</span>
              <hr className={classes.hr} />
            </Grid>
            <Grid item md={12} xs={12}>
              <strong className={classes.strong}>Total éffectif :</strong> <span className={classes.span}>5555551114414411</span>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default DetailDuContribuable;
