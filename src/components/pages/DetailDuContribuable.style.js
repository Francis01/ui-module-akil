import { makeStyles, withStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

export const UseStyleDetailDuContribuable = makeStyles((theme) => ({
  
  title:{
    color:"#FF7A42",
    font: 'normal normal medium 75px/71px Roboto Slab',
    marginBottom:"2em",
    fontWeight:"bold"
  },
  div:{
    flexGrow:1,
    height:"auto",
    marginTop:'2em',
    font: 'normal normal medium 75px/71px Roboto Slab'
    
    ,/* 
    display:"flex",
    justifyItems:"center",
    alignItems:"center",
    flexDirection:"column" */
    
  },
  div2:{
    height:"auto",
    marginTop:'2em',
    display:"flex",
    flexDirection:"column",
    justifyContent:"space-between",
    height:"250px",
    font: 'normal normal medium 75px/71px Roboto Slab'

    
    /* 
    display:"flex",
    justifyItems:"center",
    alignItems:"center",
    flexDirection:"column" */
    
  },
  root:{
    width:"98%",
    marginLeft:"auto",
    marginRight:"auto",
    font: 'normal normal medium 75px/71px Roboto Slab'
    
  },
  paper:{
    boxShadow:"0 2px 5px #ccc",
    borderRadius:"5px",
    boxSizing:"border-box",
    padding:"2em",
    margin:"16px",
    height:"400px",
    display:"flex",
    flexDirection:"column",
    background:"#fff",
  },
  firstGrid:{
   /*  position:"absolute",
    top:"-5%" */
    marginTop:"-5%",
    
  },
  secondGrid:{
    /* position:"relative" */
    marginTop:"1em"
    
 
    
  },
  hr:{
                  borderTop: "1px solid #ccc",
                  color: "#ccc",
                  overflow: "visible",
                  textAlign: "right",
                  width:"65%",
                  boxShadow:"0 2px 5px #ccc",
                  boxSizing:"border-box",
  },
  strong:{
    font:" normal normal medium 18px/22px Roboto",
    color:" #404040",
    fontSize:"14px"
  },
  span:{
    color: "#686868",
    font: "normal normal medium 18px/22px Roboto",
    fontSize:"12px"

  }
}));
