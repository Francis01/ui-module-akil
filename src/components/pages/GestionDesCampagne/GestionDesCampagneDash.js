import { Paper } from "@material-ui/core";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import { UseStaticStyles } from "../Static.style";
import { Link } from "react-router-dom";
import GestionDesCampagneTable from "./GestionDesCampagneTable/GestionDesCampagneTable";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import NavBar from "../../layouts/NavBar";
const GestionDesCampagneDash = () => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / Gestion des campagnes"
        title2="Gestion des campagnes"
        boutton3={
          <Link
            to="#"
            style={{ textDecoration: "none", textTransform: "none" }}
          >
            <Boutton
              name="Ajouter une campagne"
              style={UseStylesBoutton().orangeBtn2}
            />
          </Link>
        }
        icons1={<LocalPrintshopIcon fontSize="large" />}
        icons2={<PictureAsPdfIcon fontSize="large"/>}
      />

      <Paper
        className={UseStaticStyles().paper}
        style={{
          marginTop: "-2%",
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <GestionDesCampagneTable />
      </Paper>
    </div>
  );
};

export default GestionDesCampagneDash;
