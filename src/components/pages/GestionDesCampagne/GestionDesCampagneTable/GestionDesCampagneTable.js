import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import { UseStaticStyles } from "../../Static.style";
import MenuIconCampagne, {
  MenuIconOuverte,
  MenuIconProroger,
  MenuIconFermer,
} from "./MenuIconCampagne";
const GestionDesCampagneTable = () => {
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Date de debut</th>
            <th>Date de fin</th>
            <th>Regime</th>
            <th>Année fiscale</th>
            <th>Système</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody >
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>02/02/2020</th>
            <th>09/02/2020</th>
            <th>Réel normal</th>
            <th>2020</th>
            <th>OHADA</th>
            <th>
              <Boutton style={UseStylesBoutton().greenBtn} name="ouverte" />
            </th>
            <th>
              <MenuIconOuverte />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>02/02/2020</th>
            <th>09/02/2020</th>
            <th>Réel normal</th>
            <th>2020</th>
            <th>OHADA</th>
            <th>
              <Boutton style={UseStylesBoutton().greenBtn} name="ouverte" />
            </th>
            <th>
              <MenuIconOuverte />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>02/02/2020</th>
            <th>09/02/2020</th>
            <th>Réel normal</th>
            <th>2020</th>
            <th>OHADA</th>
            <th>
              <Boutton style={UseStylesBoutton().redBtn} name="Fermer" />
            </th>
            <th>
              <MenuIconFermer />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>02/02/2020</th>
            <th>09/02/2020</th>
            <th>Réel normal</th>
            <th>2020</th>
            <th>OHADA</th>
            <th>
              <Boutton style={UseStylesBoutton().orangeBtnTab} name="Proroger" />
            </th>
            <th>
              <MenuIconProroger />
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default GestionDesCampagneTable;
