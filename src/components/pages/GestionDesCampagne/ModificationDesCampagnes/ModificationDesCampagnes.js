import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import HeaderDegrade from "../../../layouts/headers/headerDegrade";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Grid, InputLabel } from "@material-ui/core";
import TextFields from "../../../layouts/input/TextField";
import { UseStylesTextField } from "../../../layouts/input/TextField.style";
import Groupe from "../../../../assert/Groupe 11517.png";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import NavBar from "../../../layouts/NavBar";
const ModificationDesCampagnes = () => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade
        title="Tableau de bord / Gestion des campagnes"
        precedent={
          <Boutton
            name="Précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid
        spacing={2}
        container
        style={{ width: "95%", marginLeft: "auto", marginRight: "auto" }}
      >
        <Grid
          item
          md={12}
          align="left"
          style={{ marginTop: "2%", color: "#757575", fontSize: "25px" }}
        >
          Modifier une campagne
        </Grid>

        <Grid item md={8} container spacing={2}>
          <Grid item md={6}>
            <InputLabel htmlFor="my-input">Régime fiscale</InputLabel>
            <TextFields
              id="my-input"
              variant="outlined"
              style={UseStylesTextField().marginModif}
              type="text"
              endIcon={<ExpandMoreIcon/>}
            />
            
          </Grid>

          <Grid item md={6}>
          <InputLabel htmlFor="Année Fiscale">Année fiscale</InputLabel>
            <TextFields
              type="date"
              variant="outlined"
              style={UseStylesTextField().marginModif}
              
            />
          </Grid>

          <Grid item md={6}>
          <InputLabel htmlFor="my-input">Date de début</InputLabel>
            <TextFields
              type="date"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
          <InputLabel htmlFor="my-input">Date de fin</InputLabel>
            <TextFields
              type="date"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>

          {/*prorogation */}

          <Grid item md={6}>
          <InputLabel htmlFor="my-input">Date de début</InputLabel>
            <TextFields
              type="date"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
          <InputLabel htmlFor="my-input">Date de fin</InputLabel>
            <TextFields
              type="date"
              variant="outlined"
              
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={12} container>
            <Grid item md={6}>
              <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn2} />
            </Grid>
            <Grid item md={6} align="center">
              <Boutton
                name="Enregistré"
                style={UseStylesBoutton().orangeBtn2}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={4}>
          <img src={Groupe} width="400" height="400" />
        </Grid>
      </Grid>
    </div>
  );
};

export default ModificationDesCampagnes;
