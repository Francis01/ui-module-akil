import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Grid, InputLabel } from "@material-ui/core";
import Groupe from "../../../assert/Groupe 11517.png";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import TextFields from "../../layouts/input/TextField";
import { UseStylesTextField } from "../../layouts/input/TextField.style";
const AjouterUneDirection = () => {
  return (
    <div>
      <HeaderDegrade
        title="Tableau de bord / Gestion des directions"
        precedent={
          <Boutton
            name="Précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid
        spacing={2}
        container
        style={{ width: "95%", marginLeft: "auto", marginRight: "auto" }}
      >
        <Grid
          item
          md={12}
          align="left"
          style={{ marginTop: "2%", color: "#757575", fontSize: "20px",fontWeight:"bold" }}
        >
          Ajouter une direction
        </Grid>

        <Grid item md={8} container spacing={2}>
          <Grid item md={6}>
            <InputLabel htmlFor="Libellé">Libellé</InputLabel>
            <TextFields
              id="Libellé"
              variant="outlined"
              placeholder="Libellé"
              style={UseStylesTextField().marginModif}
              type="text"
            />
          </Grid>

          <Grid item md={6}>
            <InputLabel htmlFor="Nom du Directeur Régionnale">
              Nom du Directeur Régionnale
            </InputLabel>
            <TextFields
              placeholder="Nom du Directeur Régionnale"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>

          <Grid item md={6}>
            <InputLabel htmlFor="my-input">
              prenom du Directeur Régionnale
            </InputLabel>
            <TextFields
              placeholder="prenom du Directeur Régionnale"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
            <InputLabel htmlFor="my-input">Email</InputLabel>
            <TextFields
              placeholder="Email"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>

          {/*prorogation */}

          <Grid item md={6}>
            <InputLabel htmlFor="my-input">Contact</InputLabel>
            <TextFields
              placeholder="contact"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
            <InputLabel htmlFor="my-input">Ville</InputLabel>
            <TextFields
              placeholder="ville"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={12} container>
            <Grid item md={6}>
              <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn2} />
            </Grid>
            <Grid item md={6} align="center">
              <Boutton
                name="Enregistré"
                style={UseStylesBoutton().orangeBtn2}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={4}>
          <img src={Groupe} width="400" height="400" />
        </Grid>
      </Grid>
    </div>
  );
};

export default AjouterUneDirection;
