import React, { useState } from "react";
import {
  Checkbox,
  FormControl,
  IconButton,
  InputBase,
  Menu,
  MenuItem,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Grid,
  Paper,
  Button,
  InputAdornment,
} from "@material-ui/core";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import CreateSharpIcon from "@material-ui/icons/CreateSharp";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import DirectionTable from "./ListeDirectionTable/DirectionTable";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import { UseStaticStyles } from "../Static.style";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import NavBar from "../../layouts/NavBar";

function ListeDesDirections() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / Gestion des directions"
        title2="Liste des directions"
        boutton2={
          <Boutton
            name="Liste des importations"
            style={UseStylesBoutton().orangeBtn2}
          />
        }
        boutton3={
          <Link
            to="/AjouterUneDirection"
            style={{ textDecoration: "none", textTransform: "none" }}
          >
            <Boutton
              name="Ajouter une direction"
              style={UseStylesBoutton().orangeBtn2}
            />
          </Link>
        }
        icons1={<LocalPrintshopIcon fontSize="large" />}
        icons2={<PictureAsPdfIcon   fontSize="large"/>}
      />

      <Paper
        className={UseStaticStyles().paper}
        style={{
          marginTop: "-2%",
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <DirectionTable />
      </Paper>
    </div>
  );
}

export default ListeDesDirections;
