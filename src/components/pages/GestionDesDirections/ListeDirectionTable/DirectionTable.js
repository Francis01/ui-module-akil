import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import MenuIconDirection from "./MenuIconDirection";
const DirectionTable = () => {
  return (
    <div>
      <Table
        striped
        
      >
        <thead style={{
          fontSize: "12px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Identifiant</th>
            <th>Libellé</th>
            <th>Nom du directeur Régional</th>
            <th>Prénoms du directeur Régional</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>ville</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>A54484</th>
            <th>Dr Abidjan</th>
            <th>Sylla</th>
            <th>Ben</th>
            <th>where@gmail.com</th>
            <th>+225 02597525</th>
            <th>Abidjan</th>
            <th>
              <MenuIconDirection/>
            </th>
          </tr>

         
        </tbody>
      </Table>
    </div>
  );
};

export default DirectionTable;
