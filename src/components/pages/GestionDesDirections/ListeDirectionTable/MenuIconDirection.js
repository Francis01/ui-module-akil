import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { IconButton, ListItemIcon, Typography } from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import ClearIcon from '@material-ui/icons/Clear';

const MenuIconDirection = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link
          to="/ModifierUneDirection"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<EditIcon />}</ListItemIcon>
            <Typography>Modifier</Typography>
          </MenuItem>
        </Link>
        <Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<ClearIcon />}</ListItemIcon>
            <Typography>Supprimer</Typography>
          </MenuItem>
        </Link>

       
      </Menu>
    </div>
  );
};

export default MenuIconDirection;
