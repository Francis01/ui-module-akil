import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Grid, InputLabel } from "@material-ui/core";
import Groupe from "../../../assert/Groupe 11517.png";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import TextFields from "../../layouts/input/TextField";
import { UseStylesTextField } from "../../layouts/input/TextField.style";
import NavBar from "../../layouts/NavBar";
const ModifierUneDirection = () => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade
        title="Tableau de bord / Gestion des directions"
        precedent={
          <Boutton
            name="Précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid
        spacing={2}
        container
        style={{ width: "95%", marginLeft: "auto", marginRight: "auto" }}
      >
        <Grid
          item
          md={12}
          align="left"
          style={{
            marginTop: "2%",
            color: "#757575",
            fontSize: "20px",
            fontWeight: "bold",
          }}
        >
          Modifier une direction
        </Grid>

        <Grid item md={8} container spacing={2}>
          <Grid item md={6}>
            <TextFields
              label="Libellé"
              id="Libellé"
              variant="outlined"
              placeholder="Libellé"
              style={UseStylesTextField().marginModif}
              type="text"
            />
          </Grid>

          <Grid item md={6}>
            <TextFields
              label="Nom du Directeur Régional"
              placeholder="Nom du Directeur Régional"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>

          <Grid item md={6}>
            
            <TextFields
              label="prénom(s) du Directeur Régiona"
              placeholder="prénom(s) du Directeur Régional"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
            <TextFields
              label="Email"
              placeholder="Email"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>

          {/*prorogation */}

          <Grid item md={6}>
            <TextFields
              label="contact"
              placeholder="contact"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={6}>
            <TextFields
              label="ville"
              placeholder="ville"
              type="text"
              variant="outlined"
              style={UseStylesTextField().marginModif}
            />
          </Grid>
          <Grid item md={12} container>
            <Grid item md={6}>
              <Boutton name="Annulé" style={UseStylesBoutton().whiteBtn2} />
            </Grid>
            <Grid item md={6} align="center">
              <Boutton
                name="Enregistré"
                style={UseStylesBoutton().orangeBtn2}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={4}>
          <img src={Groupe} width="400" height="400" />
        </Grid>
      </Grid>
    </div>
  );
};

export default ModifierUneDirection;
