import MenuIconDesExpertsComptablesAbonne from "./MenuIconDesExpertsComptablesAbonne";
import {Table} from "react-bootstrap"
import { Checkbox } from "@material-ui/core";
import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
const TableTisteDesExpertsComptablesAbonne = () => {
  return (
    <div>
      <Table
        striped
      
      >
        <thead style={{ justifyContent:"right",fontWeight:"bold",fontSize: "10px", color: "#757575",background:"#fff",}}>
          <tr style={{justifyContent:'right',alignContent:"right",justifyItems:'right',}}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Réf</th>
            <th>Nom</th>
            <th>Prénoms</th>
            <th>Date</th>
            <th>Mode de paiement</th>
            <th>Nombre d'unité</th>
            <th>Montant total</th>
            <th>Dossier restant</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody  style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
          background:"#fff",
          alignContent:"right  ", justifyContent:"right"
        }} >
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>200</th>
            <th><Boutton name="Validé" style={UseStylesBoutton().greenBtn}/></th>
            <th>
              <MenuIconDesExpertsComptablesAbonne />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>200</th>
            <th><Boutton name="Rejeté" style={UseStylesBoutton().grayBtn}/></th>
            <th>
              <MenuIconDesExpertsComptablesAbonne />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>200</th>
            <th><Boutton name="Validé" style={UseStylesBoutton().greenBtn}/></th>
            <th>
              <MenuIconDesExpertsComptablesAbonne />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>120052S</th>
            <th>Coulibaly</th>
            <th>Assetou</th>
            <th>02/02/2020</th>
            <th>Mobile money</th>
            <th>3</th>
            <th>3000</th>
            <th>200</th>
            <th><Boutton name="Expiré" style={UseStylesBoutton().redBtn}/></th>
            <th>
              <MenuIconDesExpertsComptablesAbonne />
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableTisteDesExpertsComptablesAbonne;
