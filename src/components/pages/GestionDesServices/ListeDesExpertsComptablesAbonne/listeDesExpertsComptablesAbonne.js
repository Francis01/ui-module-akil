import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import HeaderDegrade2 from "../../../layouts/headers/HeaderDegrade2";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import TableTisteDesExpertsComptablesAbonne from "./TableListeDesExpertsComptablesAbonne";
import { Grid, IconButton, Paper } from "@material-ui/core";
import Write from "../../../../assert/write.png";
import ListeDesOffresItems from "../ListeDesOffresItems";
import MenuIconListeDesServices from "../MenuIconListeDesServices";
import ClearIcon from "@material-ui/icons/Clear";
import NavBar from "../../../layouts/NavBar";
const ListeDesExpertsComptablesAbonne = (props) => {
  return (
    <Grid>
      <NavBar/>
      <Grid container item>
        <Grid item md={9} xs={9}>
          <Grid item md={12} xs={12}>
            {" "}
            <HeaderDegrade2
              title2="Liste des experts-comptables abonnés"
              icons3={
                <Link
                  to="#"
                  style={{ textDecoration: "none", textTransform: "none" }}
                >
                  <Boutton
                    name="Précédent"
                    style={UseStylesBoutton().btnPriv}
                    startIcon={<ArrowBackIcon />}
                  />
                </Link>
              }
            />
          </Grid>

          <Grid
            alignItems="left"
            item
            md={12}
            xs={12}
            style={{ width: "95%", marginLeft: "auto", overflow: "none",marginTop:"-4%" }}
          >
              <TableTisteDesExpertsComptablesAbonne />
            
          </Grid>
        </Grid>

        <Grid
          item
          md={3}
          xs={3}
          style={{
            width: "100%",
            height: "100vh",
            overflow: "none",
            boxShadow: "0 2px 5px 2px #ccc",
            boxSizing: "border-box",
            padding: "2em",
          }}
        >
          {" "}
          <ListeDesOffresItems
            logo={Write}
            prixUnitaire="100 frcs / unite"
            statutTexte="Actif"
            signature="Signature"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts-comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts-comptables désabonnés :"
            redNombre="20"
            offre='true'
          />
        </Grid>
      </Grid>
    </Grid>
  );
};
export default ListeDesExpertsComptablesAbonne;
