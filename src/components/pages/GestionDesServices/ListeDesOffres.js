import {useState} from 'react';
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import { UseStaticStyles } from "../Static.style";
import { Grid, Paper } from "@material-ui/core";
import WriteIco from "../../../assert/write.png";
import ListeDesOffresItems from "./ListeDesOffresItems";
import MenuIconListeDesServices from "./MenuIconListeDesServices";
import Changes from "../../../assert/change.png";
import NavBar from '../../layouts/NavBar';

const ListeDesOffres = () => {
  const [modifier,setModifier]=useState(false)
  const [supprimer,setSupprimer]=useState(false)
  return (
    <div style={{height:"100vh"}}>
      <NavBar/>
      <HeaderDegrade2
        title2="Liste des Services"
        icons3={
          <Link
            to="#"
            style={{ textDecoration: "none", textTransform: "none" }}
          >
            <Boutton
              name="Crée une offre"
              style={UseStylesBoutton().orangeBtn2}
            />
          </Link>
        }
      />

      <Grid item container spacing={2} style={{marginTop:"2%" ,width:"95%",marginLeft:"auto",marginRight:"auto"}}>
        <Grid item md={4}>
          <ListeDesOffresItems
            logo={WriteIco}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Signature"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts-comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts-comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>

        <Grid item md={4}>
          <ListeDesOffresItems
            logo={Changes}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Envoie des états financiers"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>
        <Grid item md={4}>
          <ListeDesOffresItems
            logo={Changes}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Envoie des états financiers"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>

        <Grid item md={4}>
          <ListeDesOffresItems
            logo={WriteIco}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Signature"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts-comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts-comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>
        <Grid item md={4}>
          <ListeDesOffresItems
            logo={Changes}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Envoie des états financiers"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>

        <Grid item md={4}>
          <ListeDesOffresItems
            logo={Changes}
            prixUnitaire="100 frcs / unité"
            statutTexte="Actif"
            signature="Envoie des états financiers"
            bodyTexte="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum"
            greenTexte="Nombre des experts comptables abonnés :"
            greenNombre="20"
            redTexte="Nombre des experts comptables désabonnés :"
            redNombre="20"
            iconButtons={<MenuIconListeDesServices />}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default ListeDesOffres;
