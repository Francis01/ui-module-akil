import {makeStyles} from "@material-ui/core"


export const  UseStylesListeDesOffres=makeStyles((theme)=>({
    prixUnitaire:{
        color:" #FF3A29",
        font: "normal normal medium 22px/27px Roboto",
        opacity: 1,
    },
    statutIcon:{
        background:" #38AA00 0% 0% no-repeat padding-box",
        width: "10px",
        height: "10px",
        borderRadius:"50%"

    },
    statutTexte:{
        font: "normal normal normal 16px/19px Roboto",
        color:" #757575",
        fontSize:"12px"
    },
    signature:{
        color: "#000000",
        font: "normal normal normal 25px/30px Roboto",
        
        

    },
    bodyTexte:{
        color:" #757575",
        font: "normal normal normal 14px/20px Roboto",
        
    },
    greenTetxte:{
        color: "#267466",
        font: "normal normal medium 16px/20px Roboto",
        textAlign:"left",
        fontSize:"14px"
    },
    greenNombre:{
        font: "normal normal medium 16px/20px Roboto",
        color: "#267466",
        opacity: 1
    },
    redTexte:{
        color: "#CE332D",
        font: "normal normal medium 16px/20px Roboto",
        textAlign:"left",
        fontSize:"14px"
    },
    redNombre:{
        color: "#CE332D",
        font: "normal normal medium 16px/20px Roboto",
        opacity: 1
    },
    detailOffre:{
        color:"#636363",
        font: "normal normal medium 16px/20px Roboto",
        fontSize:"20px",
        fontWeight:"bold",
        marginBottom:"2em",
        marginTop:"2em"

    }
}))