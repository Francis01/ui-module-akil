import {UseStylesListeDesOffres} from "./ListeDesOffres.style"
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import { UseStaticStyles } from "../Static.style";
import { Grid, Paper } from "@material-ui/core";
import WriteIco from "../../../assert/write.png"
const ListeDesOffresItems=(props)=>{
    const {logo,
        prixUnitaire,
        statutTexte,
        signature,
        bodyTexte,
        greenTexte,
        greenNombre,
        redTexte,
        iconButtons,
        redNombre}=props
    const classes=UseStylesListeDesOffres()
    return(
        <>
        {props.offre==='true'?(
        <Grid justify="center" alignItems="center" alignItems='center' >
        <Grid item container md={12} xs={12} alignContent="center" alignItems="center" style={{marginBottom:"2em"}}>
                <Grid item md={12} xs={12} className={classes.detailOffre}>Détails de l'offre</Grid>
                  <Grid item md={4} xs={4} align="left"><img src={logo} width="70"/>
                  </Grid>
                  <Grid md={4} xs={4} >
                  <Grid item md={6} xs={6} alignItems="left" align="left" className={classes.signature}><small>{signature}</small></Grid>
                  <Grid item md={6} xs={6} alignContent="center" alignItems="center">
                      <Grid container   alignContent="center" alignItems="center"><Grid item md={2} xs={2} align="rigth" className={classes.statutIcon}> </Grid> <Grid item md={10} xs={10} className={classes.statutTexte}>{statutTexte}</Grid></Grid>
                  </Grid>

                  </Grid>
                  
              </Grid>
              <Grid spacing={2} container>
                  <Grid item md={12} xs={12} className={classes.bodyTexte}>{bodyTexte}</Grid>
                  
                  <Grid item md={12} xs={12} container  alignContent="center" alignItems="center">
                      <Grid item md={10} xs={10} className={classes.greenTetxte}>{greenTexte}</Grid>
                      <Grid item md={2} xs={2} className={classes.greenNombre}>{greenNombre}</Grid>
                  </Grid>

                  <Grid item md={12} xs={12} container  alignContent="center" alignItems="center">
                  <Grid item md={10} xs={10} className={classes.redTexte}>{redTexte}</Grid>
                      <Grid item md={2} xs={2} className={classes.redNombre}>{redNombre}</Grid>
                  </Grid>
              </Grid>
        </Grid>):(
             <Paper className={UseStaticStyles().paper}>
             <Grid item container md={12} xs={12} alignContent="center" alignItems="center">
                 <Grid item md={4} xs={4} align="left"><img src={logo} width="50"/></Grid>
                 <Grid item md={4} xs={4} alignContent="center" alignItems="center">
                     <Grid item className={classes.prixUnitaire} align="left">{prixUnitaire}</Grid>
                     <Grid container   alignContent="center" alignItems="center"><Grid item md={1} xs={1} align="rigth" className={classes.statutIcon}> </Grid> <Grid item md={11} xs={11} className={classes.statutTexte}>{statutTexte}</Grid></Grid>
                 </Grid>
                 <Grid align="right" item md={4} xs={4} className={classes.menuIcon}>{iconButtons}</Grid>
             </Grid>
             <Grid spacing={2} container>
                 <Grid item md={12} xs={12} alignItems="left" align="left" className={classes.signature}><strong>{signature}</strong></Grid>
                 <Grid item md={12} xs={12} className={classes.bodyTexte}>{bodyTexte}</Grid>
                 
                 <Grid item md={6} xs={6} container  alignContent="center" alignItems="center">
                     <Grid item md={10} xs={10} className={classes.greenTetxte}>{greenTexte}</Grid>
                     <Grid item md={2} xs={2} className={classes.greenNombre}>{greenNombre}</Grid>
                 </Grid>

                 <Grid item md={6} xs={6} container  alignContent="center" alignItems="center">
                 <Grid item md={10} xs={10} className={classes.redTexte}>{redTexte}</Grid>
                     <Grid item md={2} xs={2} className={classes.redNombre}>{redNombre}</Grid>
                 </Grid>
             </Grid>
         </Paper>
        )}
       
          </>
    )
}
export default ListeDesOffresItems;