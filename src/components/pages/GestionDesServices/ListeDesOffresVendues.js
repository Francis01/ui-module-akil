import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import OffreTable from "./ListeOffreTable/OffreTable";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Grid } from "@material-ui/core";

const ListeDesOffresVendues = () => {
  return (
    <div>
      <HeaderDegrade
        title="Tableau de bord / Gestion des services"
        precedent={
          <Boutton
            name="Précédent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
      />
      <Grid
        container
        spacing={4}
        style={{
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
          fontSize: "25px",
        }}
      >
        <Grid
          item
          md={12}
          style={{
            fontSize: "25px",
            color: "#757575",
            font: " normal normal medium 14px/19px Roboto Slab",
          }}
        >
          Liste des offres vendues
        </Grid>
      </Grid>
      <OffreTable />
    </div>
  );
};
export default ListeDesOffresVendues;
