import React, { useState } from "react";
import {
  Checkbox,
  FormControl,
  IconButton,
  InputBase,
  Menu,
  MenuItem,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Grid,
  Paper,
  Button,
  InputAdornment,
} from "@material-ui/core";
import {
  useStyleListeDesServices,
  StyledTableCell,
  StyledTableRow,
} from "./ListeDesServices.style";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import CreateSharpIcon from "@material-ui/icons/CreateSharp";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import { UseStaticStyles } from "../Static.style";
import TableListeDesServices from "./TableListeDesServices";

function ListesDesServices() {
  const classes = useStyleListeDesServices();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.container}>
      <HeaderDegrade2
        title1="Tableau de bord / liste des services"
        title2="Liste des Service"
        boutton2={
          <Boutton
            name="Liste  des importations"
            style={UseStylesBoutton().orangeBtn2}
          />
        }
        boutton3={
          <Link
            to="#"
            style={{ textDecoration: "none", textTransform: "none" }}
          >
            <Boutton
              name="Ajouter un service"
              style={UseStylesBoutton().orangeBtn2}
            />
          </Link>
        }
        icons1={<LocalPrintshopIcon />}
        icons2={<PictureAsPdfIcon />}
      />

      <Paper
        className={UseStaticStyles().paper}
        style={{
          marginTop: "-2%",
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <TableListeDesServices />
      </Paper>
    </div>
  );
}

export default ListesDesServices;
