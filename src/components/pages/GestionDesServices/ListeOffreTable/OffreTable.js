import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Boutton from "../../../layouts/boutton/Boutton";
import InfoIcon from "@material-ui/icons/Info";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import Img1 from "../../../../assert/change.png";
import Img2 from "../../../../assert/write.png";
const OffreTable = () => {
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "15px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <thead>
          <tr>
            <th></th>
            <th>Libellé</th>
            <th>Date de création</th>
            <th>Nombre vendu</th>
            <th>Montant total</th>
            <th></th>
          </tr>
        </thead>
        <tbody style={{ overflow: "none" }}>
          <tr>
            <th>
              <img src={Img2} />
            </th>
            <th>Signature</th>
            <th>12/10/2019</th>
            <th>12</th>
            <th>200 000</th>
            <th>
              <Boutton
                name="Details"
                startIcon={<InfoIcon />}
                style={UseStylesBoutton().orangeBtn}
              />
            </th>
            <th></th>
          </tr>

          <tr>
            <th>
              <img src={Img1} />
            </th>
            <th>Envoie des état financiers</th>
            <th>12/10/2019</th>
            <th>24</th>
            <th>200 000</th>
            <th>
              <Boutton
                name="Details"
                startIcon={<InfoIcon />}
                style={UseStylesBoutton().orangeBtn}
              />
            </th>
            <th></th>
          </tr>

          <tr>
            <th>
              <img src={Img1} />
            </th>
            <th>Envoie des état financiers</th>
            <th>12/10/2019</th>
            <th>300</th>
            <th>200 000</th>
            <th>
              <Boutton
                name="Details"
                startIcon={<InfoIcon />}
                style={UseStylesBoutton().orangeBtn}
              />
            </th>
            <th></th>
          </tr>

          <tr>
            <th>
              <img src={Img2} />
            </th>
            <th>Signature</th>
            <th>12/10/2019</th>
            <th>7500</th>
            <th>200 000</th>
            <th>
              <Boutton
                name="Details"
                startIcon={<InfoIcon />}
                style={UseStylesBoutton().orangeBtn}
              />
            </th>
            <th></th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default OffreTable;
