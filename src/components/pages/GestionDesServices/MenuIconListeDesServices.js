import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Grid, IconButton, ListItemIcon, Typography } from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import Popup from "../../layouts/Dialogs/simpleDialog";
import Groupe from "../../../assert/Groupe 12252.png";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import DialogContent from "../../layouts/Dialogs/DialogContent";
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
const MenuIconListeDesServices = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [openPopup, setOpenPopup] = useState(false);

  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >

<Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<VisibilityIcon />}</ListItemIcon>
            <Typography>Détail</Typography>
          </MenuItem>
        </Link>
        
        <Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
          onClick={() => setOpenPopup(true)}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<DeleteIcon />}</ListItemIcon>
            <Typography>Supression</Typography>
          </MenuItem>
        </Link>

        <Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<EditIcon />}</ListItemIcon>
            <Typography>Modifier</Typography>
          </MenuItem>
        </Link>

        <Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
          onClick={() => setOpenPopup(true)}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<PowerSettingsNewIcon />}</ListItemIcon>
            <Typography>Désactiver</Typography>
          </MenuItem>
        </Link>

        <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}>
          <DialogContent
            imageText="Suppression"
            textInterrogative="Voulez-vous Supprimer?"
            button1={
              <Boutton
                name="continuer"
                style={UseStylesBoutton().orangeBtn2}
                click={() => setOpenPopup(false)}
              />
            }

            button2={
              <Boutton
                name="annuler"
                style={UseStylesBoutton().whiteBtn2}
                click={() => setOpenPopup(false)}
              />
            }
            image={Groupe}
          />
        </Popup>
      </Menu>
    </div>
  );
};

export default MenuIconListeDesServices;
