import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import SaveIcon from "@material-ui/icons/Save";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SmsIcon from "@material-ui/icons/Sms";
import { Link } from "react-router-dom";
import MenuIconListeDesServices from "./MenuIconListeDesServices";
const TableListeDesServices = () => {
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>code</th>
            <th>Nom du service</th>
            <th>Direction régionale</th>
            <th>Émail</th>
            <th>Télephone</th>
            <th>Ville</th>
            <th>Direction</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>001</th>
            <th>Service 0</th>
            <th>Direction 0</th>
            <th>assetou@gmail.com</th>
            <th>02340302</th>
            <th>Abidjan</th>
            <th>Dr Abidjan</th>
            <th>
              <MenuIconListeDesServices />
            </th>
          </tr>

        </tbody>
      </Table>
    </div>
  );
};

export default TableListeDesServices;
