import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Divider, Grid, IconButton, Paper } from "@material-ui/core";
import TextFields from "../../../layouts/input/TextField";
import HeaderDegrade from "../../../layouts/headers/headerDegrade";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PrintIcon from "@material-ui/icons/Print";
import { UseStylesTextField } from "../../../layouts/input/TextField.style";
import { UseStyleFacture } from "./Facture.style";
import { UseStaticStyles } from "../../Static.style";
import NavBar from "../../../layouts/NavBar";
import SelectInput from "../../../layouts/input/SelectInput"
import { Table } from "react-bootstrap";

const Facture = (props) => {
  const classes = UseStyleFacture();
  const SelectData=[
    {title:'mode de paiement'},
    {
      id:10,
      text:"Visa"
    },
    {
      id:20,
      text:"cheque"
    },
    {
      id:30,
      text:"espèce"
    },
    {
      id:40,
      text:"virement banquaire"
    },
    
  ]
  return (
    <Grid>
      <NavBar />
      <Grid container item>
        <Grid item md={8} xs={8}>
          <Grid item md={12} xs={12}>
            {""}
            <HeaderDegrade
              title="Tableau de bord / gestion des services"
              button3={
                <Boutton
                  name="Envoyer par mail"
                  startIcon={<MailOutlineIcon />}
                  style={UseStylesBoutton().orangeBtn}
                />
              }
              listeExpertComptable={
                <Boutton
                  startIcon={<PrintIcon />}
                  name="Imprimer la facture"
                  style={UseStylesBoutton().whiteBtn}
                />
              }
              precedent={
                <Boutton
                  name="précédent"
                  startIcon={<ArrowBackIcon />}
                  style={UseStylesBoutton().btnPriv}
                />
              }
            />
          </Grid>

          <Grid
            alignItems="left"
            item
            md={12}
            xs={12}
            style={{
              overflow: "none",
              background: " #F9F9F9 0% 0% no-repeat padding-box ",
              border: "0.5px solid #707070",
              padding: "1em 2em 1em 2em",
            }}
            container
          >
            <Grid item md={4} container >
              
              <Grid item md={6} className={classes.title}>
                Référence
              </Grid>
              <Grid item md={6} className={classes.content}>
                120052s
              </Grid>
              
              <Grid item md={6} className={classes.title}>
                Nom
              </Grid>
              <Grid item md={6} className={classes.content}>
                Coulibaly
              </Grid>
              <Grid item md={6} className={classes.title}>
                Prénoms
              </Grid>
              <Grid item md={6} className={classes.content}>
                Assetou
              </Grid>
            </Grid>
            
            <Grid item md={4} container className={classes.itemsContainer}>
              <Grid item md={6} className={classes.title}>
                offres souscrit
              </Grid>
              <Grid item md={6} className={classes.content}>
                Signature
              </Grid>
              <Grid item md={6} className={classes.title}>
                Montant offres (unité)
              </Grid>
              <Grid item md={6} className={classes.content}>
                1000
              </Grid>
              
              <Grid item md={6} className={classes.title}>
                Montant total
              </Grid>
              <Grid
                item
                md={6}
                style={{ fontWeight: "bold", fontSize: "13px" }}
              >
                3000
              </Grid>
            </Grid>


            <Grid item md={4} container className={classes.itemsContainer}>
              <Grid item md={12} className={classes.title}>
                Statut
              </Grid>
              <Grid item md={12}>
                <Boutton name="Validé" style={UseStylesBoutton().greenBtn} />
              </Grid>
            </Grid>
          </Grid>

          <Grid
            item
            md={12}
            xs={12}
            style={{
              width: "95%",
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: "4%",
            }}
          >
            <Grid item md={12} xs={12}>
              {" "}
              <label htmlFor="#date" className={classes.label}>Date de paiement</label>
              <TextFields
                variant="outlined"
                type="date"
                id="date"
                style={UseStylesTextField().formControl}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              {" "}
              <label htmlFor="#Montant"  className={classes.label}>Montant</label>
              <TextFields
                variant="outlined"
                type="text"
                placeholder="3000"
                id="Montant"
                style={UseStylesTextField().formControl}
              />
            </Grid>

            <Grid item md={12} xs={12}>
              {" "}{/* 
              <label htmlFor="#paiement" className={classes.label}>Mode de paiement</label> */}
              <SelectInput data={SelectData}/>
            </Grid>
          </Grid>
        </Grid>

        <Grid item md={4} xs={4} className={classes.siedBar}>
          <Grid
            container
            item
            md={12}
            xs={12}
            className={UseStaticStyles().paper}
          >
            <Grid item md={6} xs={6} className={classes.numFact}>
              <div>Facture N 0025</div>
              <div>OECCO</div>
            </Grid>
            <Grid item md={6} xs={6} className={classes.factureCoordonnée}>
              <Grid item md={12} xs={12}>
                Abidjan Ambassade de chine
              </Grid>
              <Grid item md={12} xs={12}>
                BP 0000 Abidjan01{" "}
              </Grid>
              <Grid item md={12} xs={12}>
                +225 00 000 000
              </Grid>
              <Grid item md={12} xs={12}>
                +225 00 000 000
              </Grid>
            </Grid>

            <Grid
              item
              md={6}
              xs={6}
              className={classes.factureCoordonnée}
              alignItems="center"
              justifyContent="center"
              alignContent="center"
            >
              <Grid item md={12} xs={12}>
                {" "}
                <strong>Client</strong>
              </Grid>
              <Grid item md={12} xs={12}>
                Diaudes Corp{" "}
              </Grid>
              <Grid item md={12} xs={12}>
                Canada Quebec BP 000 Quebec
              </Grid>
              <Grid item md={12} xs={12}>
                +00 00 000 000 / +00 00 000 000
              </Grid>
            </Grid>

            <Grid item md={6} xs={6} className={classes.factureCoordonnée}>
              <Grid item md={12}>
                <strong>Date:</strong>02/02/2020
              </Grid>
            </Grid>
            <Grid item md={12} xs={12}>
              <div className={classes.div}>
                <Table className={classes.table} aria-label="caption table">
                  <thead className={classes.theader} className={classes.theader} >
                             
                    <tr   >
                      <th className={classes.sharp}>#</th>
                      <th>QUANTITÉ</th>
                      <th>DÉSIGNATION</th>
                      <th>PRIX UNITAIRE</th>
                      <th>PRIX TOTAL</th>
                    </tr>
                  </thead>
                  <tbody className={classes.tbody}>
                    <tr>
                      <th className={classes.sharp}>
                        #
                      </th>
                      <th>01</th>
                      <th>Lorem</th>
                      <th>300.000</th>
                      <th>900.000</th>
                    </tr>
                    <tr>
                      <th className={classes.sharp}>
                        #
                      </th>
                      <th>02</th>
                      <th>Lorem</th>
                      <th>300.000</th>
                      <th>900.000</th>
                    </tr>
                    <tr>
                      <th>
                        
                      </th>
                      <th></th>
                      <th></th>
                      <th>Remise</th>
                      <th>0%</th>
                    </tr>
                    <tr >
                      <th>
                        
                      </th>
                      <th></th>
                      <th></th>
                      <th style={{background:"#378D7D 0% 0% no-repeat padding-box",color:"#fff", height:"30px"}}>taxe</th>
                      <th style={{background:"#378D7D 0% 0% no-repeat padding-box",color:"#fff", height:"30px"}}>0%</th>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};
export default Facture;
