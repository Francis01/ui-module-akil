import {makeStyles} from "@material-ui/core";
import Image from "../../../../assert/siedBar.png"
export const UseStyleFacture =makeStyles((theme)=>({

content:{
    fontWeight: "bold",
     fontSize: "13px",
     font: "normal normal normal 18px/22px Roboto"
},

title:{
    fontSize: "12px",
    color: "#707070",
    font: "normal normal normal 18px/22px Roboto",
},

siedBar:{
    width: "100%",
    height: "100%",
    overflow: "none",
    boxSizing: "border-box",
    padding: "2em",
    backgroundImage: `url(${Image})`,
    backgroundSize:"cover"
},

factureCoordonnée:{
    fontSize:"8px",
    color:"#707070"
},

numFact:{
    fontSize:"12px",
    color:"#378D7D",
    font: "normal normal medium 14px/19px Roboto Slab"
},
table:{
   width: "100%",
    fontSize:"10px",
    height:"200px",
    margin:"2em 0px 0 0"
   
},
theader:{
    fontSize:"8px",
    color:"#5D5C5C",
    background:"#F4F4F4 0% 0% no-repeat padding-box",
},
div:{
    height:"400px",
    padding:'0.5em'
},
tbody:{
    fontSize:"8px",
    color:"#5D5C5C",
},
sharp:{
    
color:"#EE7139"
},
itemsContainer:{
    borderLeft:"1px solid gray",
    paddingLeft:"2em"

},
label:{
    fontSize:"12px",
    marginLeft:"2%"
}
}))