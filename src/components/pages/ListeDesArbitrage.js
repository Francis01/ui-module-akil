import React, { useState } from "react";
import * as Mui from "@material-ui/core";
import {
  useStyleListesDesArbitrage,
  StyledTableCell,
  StyledTableRow,
} from "./ListeDesArbitrage.style";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import CreateSharpIcon from "@material-ui/icons/CreateSharp";

function ListeDesArbitrage() {
  const classes = useStyleListesDesArbitrage();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.container}>
      <div className={classes.submenu}>
        <Mui.Grid container className={classes.btnMenu}>
          <Mui.Grid item xs={6}>
            <Mui.Grid item xs={12} style={{ fontSize: "x-small" }}>
              Tableau de bord / Gestion des arbitrages
            </Mui.Grid>
          </Mui.Grid>
        </Mui.Grid>
        <Mui.Grid container className={classes.menu2}>
          <Mui.Grid item xs={6} container>
            <Mui.Grid item xs={6}>
              <Mui.Typography variant="h6">
                Gestion des arbitrages
              </Mui.Typography>
            </Mui.Grid>

            <Mui.Grid item xs={6}>
              <Mui.FormControl className={classes.formInp} variant="outlined">
                <Mui.InputBase
                  type="text"
                  fullWidth="true"
                  placeholder="Filtre par"
                  className={classes.inp}
                  endAdornment={
                    <Mui.InputAdornment position="end">
                      <Mui.IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                      >
                        <small
                          style={{
                            color: "#fff",
                            fontSize: "small",
                            fontWeight: "bold",
                          }}
                        >
                          En cours
                        </small>
                        <KeyboardArrowDownIcon style={{ color: "#fff" }} />
                      </Mui.IconButton>
                    </Mui.InputAdornment>
                  }
                  labelWidth={20}
                />
              </Mui.FormControl>
            </Mui.Grid>
          </Mui.Grid>

          <Mui.Grid item xs={6} container>
            <Mui.Grid item xs={8}>
              <Mui.InputBase
                fullWidth="true"
                style={{
                  color: "#fff",
                  borderBottom: "1px solid white",
                  fontSize: "15px",
                }}
                placeholder="Faite vos recherche ici"
              />
            </Mui.Grid>
            <Mui.Grid item xs={4} className={classes.icons}>
              <Mui.IconButton>
                <PrintIcon className={classes.icons} fontSize="large" />{" "}
              </Mui.IconButton>
              <Mui.IconButton>
                <PictureAsPdfIcon className={classes.icons} fontSize="large" />
              </Mui.IconButton>
            </Mui.Grid>
          </Mui.Grid>
        </Mui.Grid>
      </div>
      <div className={classes.tab}>
        <Mui.Paper
          style={{
            height: "100vh",
            width: "90%",
            align: "center",
            margin: "auto",
            marginTop: "-5%",
          }}
        >
          <Mui.TableContainer component={Mui.Paper}>
            <Mui.Table className={classes.table} aria-label="customized table">
              <Mui.TableHead>
                <Mui.TableRow>
                  <StyledTableCell>
                    <Mui.Checkbox />
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    Forme juridique{" "}
                  </StyledTableCell>
                  <StyledTableCell align="left">IDU</StyledTableCell>
                  <StyledTableCell align="left">
                    Compte contribuable
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    Registre de commerce
                  </StyledTableCell>
                  <StyledTableCell align="left">Raison social</StyledTableCell>
                  <StyledTableCell align="left">Mission</StyledTableCell>
                  <StyledTableCell align="left">
                    Date de conflit
                  </StyledTableCell>
                  <StyledTableCell align="center">Statut</StyledTableCell>
                  <StyledTableCell align="left">Actions</StyledTableCell>
                </Mui.TableRow>
              </Mui.TableHead>
              <Mui.TableBody>
                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    <Mui.Checkbox />
                  </StyledTableCell>

                  <StyledTableCell align="left">SARL</StyledTableCell>
                  <StyledTableCell align="left">5555555555A</StyledTableCell>
                  <StyledTableCell align="left">AAAA33333223</StyledTableCell>
                  <StyledTableCell align="left">REG113569</StyledTableCell>
                  <StyledTableCell align="left">DiaudesCORP</StyledTableCell>
                  <StyledTableCell align="left">EXPERTISE</StyledTableCell>
                  <StyledTableCell align="left">20/02/2020</StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.Button
                      size="small"
                      variant="contained"
                      className={classes.tabBtn1}
                    >
                      En arbitrage
                    </Mui.Button>
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.IconButton onClick={handleClick}>
                      <MoreHorizIcon className={classes.iconColor} />
                      <Mui.Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                      >
                        <Mui.MenuItem onClick={handleClose}>
                          <Mui.IconButton>
                            <VisibilityOutlinedIcon />
                          </Mui.IconButton>
                          <Mui.Typography>Régler le conflit</Mui.Typography>
                        </Mui.MenuItem>
                      </Mui.Menu>
                    </Mui.IconButton>
                  </StyledTableCell>
                </StyledTableRow>

                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    <Mui.Checkbox />
                  </StyledTableCell>

                  <StyledTableCell align="left">SARL</StyledTableCell>
                  <StyledTableCell align="left">5555555555A</StyledTableCell>
                  <StyledTableCell align="left">AAAA33333223</StyledTableCell>
                  <StyledTableCell align="left">REG113569</StyledTableCell>
                  <StyledTableCell align="left">DiaudesCORP</StyledTableCell>
                  <StyledTableCell align="left">EXPERTISE</StyledTableCell>
                  <StyledTableCell align="left">20/02/2020</StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.Button
                      size="small"
                      variant="contained"
                      className={classes.tabBtn2}
                    >
                      En conflit
                    </Mui.Button>
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.IconButton onClick={handleClick}>
                      <MoreHorizIcon className={classes.iconColor} />
                      <Mui.Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                      >
                        <Mui.MenuItem onClick={handleClose}>
                          <Mui.IconButton>
                            <VisibilityOutlinedIcon />
                          </Mui.IconButton>
                          <Mui.Typography>Régler le conflit</Mui.Typography>
                        </Mui.MenuItem>
                      </Mui.Menu>
                    </Mui.IconButton>
                  </StyledTableCell>
                </StyledTableRow>

                <StyledTableRow>
                  <StyledTableCell component="th" scope="row">
                    <Mui.Checkbox />
                  </StyledTableCell>

                  <StyledTableCell align="left">SARL</StyledTableCell>
                  <StyledTableCell align="left">5555555555A</StyledTableCell>
                  <StyledTableCell align="left">AAAA33333223</StyledTableCell>
                  <StyledTableCell align="left">REG113569</StyledTableCell>
                  <StyledTableCell align="left">DiaudesCORP</StyledTableCell>
                  <StyledTableCell align="left">EXPERTISE</StyledTableCell>
                  <StyledTableCell align="left">20/02/2020</StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.Button
                      size="small"
                      variant="contained"
                      className={classes.tabBtn3}
                    >
                      Conflit résolu
                    </Mui.Button>
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    <Mui.IconButton onClick={handleClick}>
                      <MoreHorizIcon className={classes.iconColor} />
                      <Mui.Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                      >
                        <Mui.MenuItem onClick={handleClose}>
                          <Mui.IconButton>
                            <VisibilityOutlinedIcon />
                          </Mui.IconButton>
                          <Mui.Typography>Régler le conflit</Mui.Typography>
                        </Mui.MenuItem>
                      </Mui.Menu>
                    </Mui.IconButton>
                  </StyledTableCell>
                </StyledTableRow>
              </Mui.TableBody>
            </Mui.Table>
          </Mui.TableContainer>
        </Mui.Paper>
      </div>
    </div>
  );
}

export default ListeDesArbitrage;

/* 
    const coolor=['#267466','#2F8B7B','#FFFFFF','#F8F8F8',
    '#2C282812','#0000001A','#757575','#2DCE98','#139DC6',
    '#E87615','#E81515','#FF7A42','#00000029','#2F776A',
    '#C7C7C7','#DFDFDF','#0000000A','#4A4A4A','#FF8A48'
    '#0000002B','#00000017','#FFFBF9','#707070','#00000057'
    '#636363','#00000012','#EFEFEF',
  
  ]
*/
