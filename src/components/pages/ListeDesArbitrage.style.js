import { makeStyles,withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export const useStyleListesDesArbitrage= makeStyles((theme)=>({
    container:{
        flexGrow:1,
        height:'100vh',
    },
    submenu:{
        backgroundColor:"#267466 ",
        color:'#fff',
        height:'40vh',
        width:'100%'
    },
 
   btnMenu:{
        position:'absolute',
        top:'5%',
        left:'5%',
        width:'90%'
    }, 
    menu2:{
      position:'absolute',
        top:'15%',
        left:'5%',
        width:'90%'
    },
    formInp:{
        width:'80%',
        height:'20%',
        


    },
    inp:{
        borderRadius:'18px !important',
        color:"#fff",
        borderColor:"#fff !important",
        border:'1px solid white',
        height:'40px',
        padding:theme.spacing(2)

    },
    table: {
        minWidth: 'auto',
      },
      tabBtn:{
        borderRadius:'18px',
        backgroundColor:'green',
        color:'#fff',
        height:'150%',
        width:'100%',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small'
      },

      tabBtn1:{
        borderRadius:'18px',
        backgroundColor:'#986948',
        color:'#fff',
        width: '138px',
        height:'40px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#986948'
        }
      },
      icons:{
          color:'#fff',
          height:'150%'
      },

      tabBtn2:{
        borderRadius:'18px',
        backgroundColor:'#CE2D2D',
        color:'#fff',
        height:'40px',
        width: '138px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#CE2D2D'
        }
      },

      tabBtn3:{
        borderRadius:'18px',
        backgroundColor:'#108D23',
        color:'#fff',
        height:'40px',
        width: '138px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#108D23'
        }
      },
      iconColor:{
       color:'#2F8B7B'
      },
  

 

}))


export const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.common.gray,
      fontSize: 10,
    },
    body: {
      fontSize: 10,
    },
  }))(TableCell);
  
export const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  