import React, { useState } from "react";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SaveIcon from "@material-ui/icons/Save";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ChatBubbleOutlineIcon from "@material-ui/icons/ChatBubbleOutline";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import Boutton from "../../layouts/boutton/Boutton";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import {Link} from "react-router-dom"

import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import { Paper } from "@material-ui/core";
import TableDesAttribution from "./TableDesAttribtion";
import { useStyleListeDesAttributions } from "./ListeDesAttribution.style";

function ListeDesAttributions() {
  const classes = useStyleListeDesAttributions()

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.container}>
      <HeaderDegrade2
        title1="Tableau de bord / liste des attributions"
        title2="Liste des attributions"
     
        boutton2={
          <Boutton
            name="Attribution automatique"
            style={UseStylesBoutton().whiteBtn2}
          />
        }
        boutton3={
          <Link to="/listDesContibuables" style={{textDecoration:"none",textTransform:"none"}}>
            <Boutton
            name="Liste des contribuables"
            style={UseStylesBoutton().orangeBtn2}
          />
          </Link>
          
        }
        icons1={<LocalPrintshopIcon />}
        icons2={<PictureAsPdfIcon />}
      />
      <div className={classes.tab}>
        <Paper
          style={{
            height: "100vh",
            width: "90%",
            align: "center",
            margin: "auto",
            marginTop: "-2%",
          }}
        >
          <TableDesAttribution/>
        </Paper>
      </div>
    </div>
  );
}

export default ListeDesAttributions;
