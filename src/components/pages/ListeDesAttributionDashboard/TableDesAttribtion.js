import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { UseStyleTableImportations } from "../dashboardListeDesImportations/TableImportation.style";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import { useStyleListeDesAttributions } from "./ListeDesAttribution.style";
import MenuIconAttribution from "./MenuIconAttribution";
const TableDesAttribution = () => {
  const classes = UseStyleTableImportations();
  return (
    <div>
      <Table striped className={classes.body}>
        <thead>
          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Forme juridique</th>
            <th>IDU</th>
            <th>Numéro du contribuable</th>
            <th>Registre de commerce</th>
            <th>Raison sociale</th>
            <th>Mission</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>Statut</th>
            <th>CGA</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SARL</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Affecté" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <IconButton style={{ outline: "none" }}>
                <MenuIconAttribution />
              </IconButton>
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SA</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Affecté" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Non-adhérent" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <IconButton style={{ outline: "none" }}>
                <MenuIconAttribution />
              </IconButton>
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SA</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Affecté" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <IconButton style={{ outline: "none" }}>
                <MenuIconAttribution />
              </IconButton>
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SA</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Affecté" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <IconButton style={{ outline: "none" }}>
                <MenuIconAttribution />
              </IconButton>
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SA</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Affecté" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <IconButton style={{ outline: "none" }}>
                <MenuIconAttribution />
              </IconButton>
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableDesAttribution;
