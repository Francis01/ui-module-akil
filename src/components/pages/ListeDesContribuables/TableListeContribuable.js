import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import SaveIcon from "@material-ui/icons/Save";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SmsIcon from "@material-ui/icons/Sms";
import { Link } from "react-router-dom";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import MenuIconContrib from "./MeniIconContrib";
const TableListeContribuable = () => {
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Forme juridique</th>
            <th>IDU</th>
            <th>Numéro du contribuable</th>
            <th>Registre de commerce</th>
            <th>Raison sociale</th>
            <th>Mission</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>Statut</th>
            <th>CGA</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SARL</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Activité" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <MenuIconContrib />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SA</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton name="Sommeil" style={UseStylesBoutton().bleuBtn} />
            </th>
            <th>
              <Boutton name="Non-adhérent" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIconContrib />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SAR</th>
            <th>111222558996S</th>
            <th>1559082Z</th>
            <th>REG113569</th>
            <th>DiaudesCORP</th>
            <th>Expertise</th>
            <th>infos@diaudes.com</th>
            <th>+225 00 000 000</th>
            <th>
              <Boutton
                name="Liquidé"
                style={UseStylesBoutton().orangeBtnTab}
              />
            </th>
            <th>
              <Boutton name="Adhérent" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
              <MenuIconContrib />
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableListeContribuable;
