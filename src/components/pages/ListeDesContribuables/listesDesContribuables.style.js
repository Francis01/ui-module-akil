import { makeStyles,withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export const useStyleListesDesContribuables= makeStyles((theme)=>({
    container:{
        flexGrow:1,
        height:'100vh',
        font:'normal normal normal 16px/19px Roboto'
    },
    submenu:{
        backgroundColor:"#267466 ",
        color:'#fff',
        height:'40vh',
        width:'100%',
        marginTop:theme.spacing(0),
        position:'relative',
        font:'normal normal normal 16px/19px Roboto'
       


    },
    btnContainer:{
    },
    btn1:{
        textTransform:'capitalize',
        marginLeft:'4%',
        borderRadius:'18px',
        color:'#FF7A42',
        background:'#FFFFFF',
        "&:hover":{
          background:'#FFFFFF',
        }
        
    },

    btn2:{
        textTransform:'capitalize',
        marginLeft:'4%',
        borderRadius:'18px',
        backgroundColor:'#FF7A42',
        color:'#FFFFFF',
        '&:hover':{
          backgroundColor:'#FF7A42'
        }
    },
    btn3:{
        textTransform:'capitalize',
        borderRadius:'18px',
        backgroundColor:'#FF7A42',
        marginLeft:'4%',
        color:'#FFFFFF',
        '&:hover':{
          backgroundColor:'#FF7A42'
        }

    },
   btnMenu:{
        position:'absolute',
        top:'5%',
        left:'5%',
        width:'90%',
        font:'normal normal normal 16px/19px Roboto'
        
    }, 
    menu2:{
      position:'absolute',
        top:'50%',
        left:'5%',
        width:'90%',
        font:'normal normal normal 16px/19px Roboto'

    },
    tab:{
      height:"50vh",
      width:"90%",
      align:"center",
      margin: "auto",
      zIndex:1,
      position:'absolute',
      top: '250px',
      left: '5%',
      right:'5%'
      
    },

    formInp:{
        width:'80%',
        height:'20%',
        


    },
    inp:{
        borderRadius:'18px !important',
        color:"#fff",
        borderColor:"#fff !important"
    },
    inp:{
      borderRadius:'18px !important',
      color:"#fff",
      borderColor:"#fff !important",
      border:'1px solid white',
      height:'40px',
      padding:theme.spacing(2)

  },
    form:{
        width:'100% !important'
    },
    table: {
        minWidth: '1000px',
      },
      tabBtn:{
        borderRadius:'18px',
        backgroundColor:'#2DCE98',
        color:'#fff',
        height:'150%',
        width:'100%',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        font:" normal normal bold 13px/15px Roboto",
        fontSize:'10px',
        '&:hover':{
          backgroundColor:'#2DCE98'
        }
      },

      tabBtn1:{
        borderRadius:'18px',
        backgroundColor:'#FF7A42',
        color:'#fff',
        height:'150%',
        width:'100%',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        font:" normal normal bold 13px/15px Roboto",
        fontSize:'10px',
        '&:hover':{
          backgroundColor:'#FF7A42'
        }
      },
      icons:{
        color:'#fff',
        height:'150%'
    },
  

 

}))


export const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.common.gray,
      fontSize: 10,
      font:'normal normal normal 16px/19px Roboto'
    },
    body: {
      fontSize: 10,
      font:'normal normal normal 16px/19px Roboto'
      
    },
  }))(TableCell);
  
export const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
        
      },
    },
  }))(TableRow);
  