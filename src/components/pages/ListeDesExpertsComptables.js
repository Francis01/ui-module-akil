import React from "react";
import HeaderDegrade2 from "../layouts/headers/HeaderDegrade2";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import ExpertComptableTab from "../layouts/headers/tabs/ExpertComptableTab";
import Tableau from "../layouts/table/Table";
import Tableau2 from "../layouts/table/Table2";
import Tableau3 from "../layouts/table/Table3";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";
import NavBar from "../layouts/NavBar";

const ListeDesExpertsComptables = () => {
  const theaders = [
    "IDU",
    "Numéro contribuable",
    "Noms",
    "prénoms",
    "Email",
    "Contacts",
    "Statut",
    "Actions",
  ];
  const tbodys = [
    "25968954",
    "1559082Z",
    "Konan",
    "manuella",
    "infos@diaudes.com",
    "+225 00 000 000",
  ];

  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / Liste des experts-comptables"
        title2="Liste des experts-comptables"
        icons1={<PrintIcon fontSize="large"/>}
        icons2={<PictureAsPdfIcon fontSize="large"/>}
        boutton2={
          <Boutton
            name="Ajouter un expert-comptable"
            style={UseStylesBoutton().comptableBtnOrange}
          />
        }
        boutton3={
          <Boutton
            name="Liste des importations"
            style={UseStylesBoutton().comptableBtnOrange}
          />
        }
      />
      <ExpertComptableTab
        titre1="Personnes physique"
        titre2="Société expert-comptable"
        titre3="CGA"
        table={<Tableau dataHeader={theaders} dataBody={tbodys} />}
        table2={<Tableau2 />}
        table3={<Tableau3 />}
      />
    </div>
  );
};
export default ListeDesExpertsComptables;
