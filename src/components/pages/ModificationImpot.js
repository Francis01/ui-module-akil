import React,{useState} from 'react';
import * as Mui from '@material-ui/core'
import {useStyleModificationImpot,StyledTableCell,StyledTableRow} from './ModificationImpot.style';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import PrintIcon from '@material-ui/icons/Print';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import CreateSharpIcon from '@material-ui/icons/CreateSharp';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SaveIcon from '@material-ui/icons/Save';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
function ModificationImpot() {
    const classes=useStyleModificationImpot()
 
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
 
    const handleClose = () => {
        setAnchorEl(null);
    };

 
 
    return(
        <div className={classes.container}>
            <div className={classes.submenu}>
                    <Mui.Grid container className={classes.btnMenu}>
                        <Mui.Grid item xs={6}>
                            <Mui.Grid item xs={12} style={{fontSize:'x-small'}}>Tableau de bord / Liste des importations</Mui.Grid>
                        </Mui.Grid>

                        <Mui.Grid item xs={6} className={classes.btnContainer}>
                            
                                 <Mui.Button startIcon={<ArrowBackIcon/> } size='small' className={classes.btn1} >Précédent</Mui.Button>
                            
                           
                                 <Mui.Button variant='contained' size='small' className={classes.btn2} >Annuler l'import</Mui.Button>
                            
                                 <Mui.Button variant='contained' size='small'className={classes.btn3}>Valider l'import</Mui.Button>
                            
                            
                        </Mui.Grid>
                        
                    </Mui.Grid>
                    <Mui.Grid container className={classes.menu2}>
                    <Mui.Grid item xs={6} container>
                        <Mui.Grid item xs={6}>
                            
                            <Mui.Typography variant='h6'>Modification des importations</Mui.Typography>
                            </Mui.Grid>

                        <Mui.Grid item xs={6} >
                            <Mui.FormControl className={classes.formInp} variant="outlined">
                                
                                <Mui.InputBase
                                type='text'
                                fullWidth='true'
                                placeholder='Filtre par'
                                className={classes.inp}
                                endAdornment={
                                    <Mui.InputAdornment position="end">
                                        <Mui.IconButton
                                        aria-label="toggle password visibility"
                                        edge="end"
                                        >
                                            <small style={{color:'#fff',fontSize:'small',fontWeight:'bold'}}>En cours</small>
                                        <KeyboardArrowDownIcon style={{color:'#fff'}}/>
                                        </Mui.IconButton>
                                    </Mui.InputAdornment>
                                    }
                                    labelWidth={20}
                                />
                        </Mui.FormControl>

                        </Mui.Grid>
                    </Mui.Grid>

                    <Mui.Grid item xs={6} container >
                        <Mui.Grid item xs={8}>
                        <Mui.InputBase fullWidth='true' style={{color:'#fff',borderBottom:'1px solid white',fontSize:'15px'}} placeholder="Faite vos recherche ici" />

                        </Mui.Grid>
                        <Mui.Grid item xs={4} className={classes.icons}>
                            <Mui.IconButton  ><PrintIcon className={classes.icons} fontSize='large'/> </Mui.IconButton>
                            <Mui.IconButton  ><PictureAsPdfIcon className={classes.icons} fontSize='large'/></Mui.IconButton>
                        </Mui.Grid>
                                    
                    </Mui.Grid>

                        </Mui.Grid>
            </div>
            <div className={classes.tab}>
                <Mui.Paper style={{height:"100vh",width:"100%",align:"center",    margin: "auto",marginTop:'-5%'}}>

                <Mui.TableContainer component={Mui.Paper}>
                  

      <Mui.Table className={classes.table} aria-label="customized table">
        
        <Mui.TableHead>
        
          <Mui.TableRow>

            
            <StyledTableCell align="left">Raison sociale</StyledTableCell>
            <StyledTableCell align="left">Nrc</StyledTableCell>
            <StyledTableCell align="left">Ncc</StyledTableCell>
            <StyledTableCell align="left">Régime</StyledTableCell>
            <StyledTableCell align="left">Service</StyledTableCell>
            <StyledTableCell align="left">Matricule</StyledTableCell>
            <StyledTableCell align="left">Téléphone</StyledTableCell>
            <StyledTableCell align="center">Mission</StyledTableCell>
            <StyledTableCell align="left">Email</StyledTableCell>
            <StyledTableCell align="left">Statut</StyledTableCell>
            <StyledTableCell align="left">Actions</StyledTableCell>
            <StyledTableCell align="left">Commentaire</StyledTableCell>
          </Mui.TableRow>
        </Mui.TableHead>
        <Mui.TableBody>
            <StyledTableRow>
              <StyledTableCell align="left">
                <Mui.Button endIcon="|" className={classes.btn11}>AS</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon="|" className={classes.btn12}>1234567A</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon="|" className={classes.btn13}>CI-ABJ-2020-A-2020</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon={<ExpandMoreIcon/>} className={classes.btn14}>Réel normal</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon="|" className={classes.btn15}>Yopougon</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon="|" className={classes.btn16}>200000</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon="|" className={classes.btn17}>45671345</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button endIcon={<ExpandMoreIcon/>}  className={classes.btn18}>Expertise</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button  className={classes.btn19}>return@gmail.com</Mui.Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button className={classes.btn20}>Erronée</Mui.Button>
              </StyledTableCell>

              <StyledTableCell align="left">
                <Mui.IconButton  >
                <SaveIcon/> <HighlightOffIcon/>
                </Mui.IconButton>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Mui.Button  className={classes.btn22}>
              <Mui.IconButton >
                <ChatBubbleOutlineIcon/>
              </Mui.IconButton>
              </Mui.Button>
              </StyledTableCell>     
              


            </StyledTableRow>

    
        </Mui.TableBody>
      </Mui.Table>
    </Mui.TableContainer>

                </Mui.Paper>
                                    
           
            </div>

        </div>
    )
    
}

export default ModificationImpot;




/* 
    const coolor=['#267466','#2F8B7B','#FFFFFF','#F8F8F8',
    '#2C282812','#0000001A','#757575','#2DCE98','#139DC6',
    '#E87615','#E81515','#FF7A42','#00000029','#2F776A',
    '#C7C7C7','#DFDFDF','#0000000A','#4A4A4A','#FF8A48'
    '#0000002B','#00000017','#FFFBF9','#707070','#00000057'
    '#636363','#00000012','#EFEFEF',
  
  ]
*/