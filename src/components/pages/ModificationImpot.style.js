import { makeStyles,withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

export const useStyleModificationImpot= makeStyles((theme)=>({
    container:{
        flexGrow:1,
        height:'100vh',
    },
    submenu:{
        backgroundColor:"#267466 ",
        color:'#fff',
        height:'40vh',
        width:'100%'
    },
 
   btnMenu:{
        position:'absolute',
        top:'5%',
        left:'5%',
        width:'90%'
    }, 
    menu2:{
      position:'absolute',
        top:'15%',
        left:'5%',
        width:'90%'
    },
    formInp:{
        width:'80%',
        height:'20%',
    },
    
    inp:{
        borderRadius:'18px !important',
        color:"#fff",
        borderColor:"#fff !important",
        border:'1px solid white',
        height:'40px',
        padding:theme.spacing(2)

    },
    table: {
        width: 'auto',
      },
      tabBtn:{
        borderRadius:'18px',
        backgroundColor:'green',
        color:'#fff',
        height:'150%',
        width:'100%',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small'
      },

      tabBtn1:{
        borderRadius:'18px',
        backgroundColor:'#986948',
        color:'#fff',
        width: '138px',
        height:'40px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#986948'
        }
      },
      icons:{
          color:'#fff',
          height:'150%'
      },

      tabBtn2:{
        borderRadius:'18px',
        backgroundColor:'#CE2D2D',
        color:'#fff',
        height:'40px',
        width: '138px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#CE2D2D'
        }
      },

      tabBtn3:{
        borderRadius:'18px',
        backgroundColor:'#108D23',
        color:'#fff',
        height:'40px',
        width: '138px',
        textAlign: 'center',
        textTransform:"capitalize",
        fontSize:'x-small',
        '&:hover':{
          backgroundColor:'#108D23'
        }
      },
      iconColor:{
       color:'#2F8B7B'
      },
      btn1:{
        textTransform:'capitalize',
        marginLeft:'5%',
        borderRadius:'18px',
        color:'#fff',
        border:'1px solid #fff',
        width:'150px',
        height:'50px'
        
    },

    btn2:{
        textTransform:'capitalize',
        marginLeft:'5%',
        borderRadius:'18px',
        backgroundColor:'#FF7A42',
        color:'#FFFFFF',
        width:'180px',
        height:'50px',
        '&:hover':{
          backgroundColor:'#FF7A42'
        }
    },
    btn3:{
        textTransform:'capitalize',
        borderRadius:'18px',
        backgroundColor:'#FF7A42',
        marginLeft:'5%',
        color:'#FFFFFF',
        width:'180px',
        height:'50px',
        '&:hover':{
          backgroundColor:'#FF7A42'
        }

    },

    btn11:{
      textTransform:'capitalize',
      borderRadius:' 21px',
      marginLeft:'5%',
      color:'#FFFFFF',
      width:'94px',
      height:'41px',
      border:'1px solid #707070',
      color:'#757575',
      font:'normal normal normal 16px/19px Roboto',
      fontSize:'10px'

  },
  btn12:{
    textTransform:'capitalize',
    borderRadius:' 21px',
    marginLeft:'5%',
    color:'#FFFFFF',
    width:'94px',
    height:'41px',
    border:'1px solid #707070',
    color:'#757575',
    font:'normal normal normal 16px/19px Roboto',
    fontSize:'10px'

},
btn13:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
btn14:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
btn15:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},

btn16:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
btn17:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
    
btn18:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
  
btn19:{
  textTransform:'capitalize',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#FFFFFF',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},
btn20:{
  textTransform:'capitalize',
  backgroundColor:'#CE332D',
  borderRadius:' 21px',
  marginLeft:'5%',
  color:'#fff',
  width:'94px',
  height:'41px',
  border:'1px solid #707070',
  color:'#757575',
  font:'normal normal normal 16px/19px Roboto',
  fontSize:'10px'

},

}))


export const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.common.gray,
      fontSize: 10,
    },
    body: {
      fontSize: 5,
    },
  }))(TableCell);
  
export const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  