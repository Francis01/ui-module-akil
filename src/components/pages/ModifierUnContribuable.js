import {
  Grid,
  Typography,
  InputBase,
  Button,
  OutlinedInput,
  InputAdornment,
} from "@material-ui/core";
import { useStyleModifierUnContribuable } from "./ModifierUnContribuable.style";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Link } from "react-router-dom";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";
const ModifierUnContribuable = () => {
  const classes = useStyleModifierUnContribuable();
  return (
    <div className={classes.container}>
      <Grid container className={classes.menu}>
        <Grid item xs={6} className={classes.submenu}>
          <small>
            Tableau de bord / Liste des contribuables / Coulibaly Assetou
          </small>
        </Grid>
        <Grid item xs={6} className={classes.btncontainer}>
          <Link
            to="/ListeDesAttributions"
            style={{ textTransform: "none", textDecoration: "none" ,outline:"none"}}
          >
            <Button startIcon={<ArrowBackIcon />} className={classes.btn}>
              {" "}
              Précédent
            </Button>
          </Link>
        </Grid>
      </Grid>
      <Typography
        component="small"
        variant="h6"
        style={{
          marginLeft: "12%",
          color: "#757575",
          font: "28px/37px Roboto Slab",
          marginTop: "1%",
          marginLeft: "-45%",
        }}
      >
        Modifier un contribuable
      </Typography>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div className={classes.form}>
          <Grid container spacing={0}>
            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  NCC
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="CI-ABJ-2020-A-2020"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Régime fiscal
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="RSI"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Email
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="cont@gmail.com"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>
          </Grid>

          {/* block 2 */}

          <Grid container spacing={0}>
            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  RCCM
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="1234567A"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Direction régionale
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Selectionner"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Statut
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Activité"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>
          </Grid>

          {/* block3 */}

          <Grid container spacing={0}>
            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  IDU
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="1234567A3222V"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Services des impôts
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Selectionner"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Adhérent CGA
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Oui"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>
          </Grid>

          {/* block4 */}

          <Grid container spacing={0}>
            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Raison sociale
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Strataige SA"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Contact mobile du dirigeant
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value=""
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Type de mission
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="Expertise"
                  labelWidth={0}
                />
              </FormControl>
            </Grid>

            {/* block5 */}

            <Grid item xs={4}>
              <FormControl
                className={clsx(classes.margin, classes.textField)}
                variant="outlined"
              >
                <FormHelperText id="outlined-weight-helper-text">
                  Forme juridique
                </FormHelperText>
                <OutlinedInput
                  id="outlined-adornment-weight"
                  aria-describedby="outlined-weight-helper-text"
                  value="SARL"
                  labelWidth={0}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <ExpandMoreIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>
            </Grid>
          </Grid>
        </div>
      </div>

      <Grid container style={{ marginTop: "2%" }}>
        <Grid item xs={6}>
          <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn} />
        </Grid>
        <Grid item xs={6}>
          <Boutton name="Suivant" style={UseStylesBoutton().orangeBtn} />
        </Grid>
      </Grid>
    </div>
  );
};

export default ModifierUnContribuable;
