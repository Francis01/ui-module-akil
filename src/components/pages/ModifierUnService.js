import {
  Grid,
  Typography,
  InputBase,
  Button,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Avatar,
  Badge,
} from "@material-ui/core";
import { useStyleModifierUnService } from "./ModifierUnService.style";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import assetou from "../../assert/assetou.png";

const ModifierUnService = () => {
  const classes = useStyleModifierUnService();
  return (
    <div className={classes.container}>
      <Grid container className={classes.menu}>
        <Grid item xs={6} className={classes.submenu}>
          <small>Tableau de bord / Gestion des services</small>
        </Grid>
        <Grid item xs={6} className={classes.btncontainer}>
          <Button startIcon={<ArrowBackIcon />} className={classes.btn}>
            {" "}
            Précédent
          </Button>
        </Grid>
      </Grid>
      <Typography
        component="small"
        variant="h6"
        style={{
          marginRight: "63%",
          color: "#757575",
          font: "28px/37px Roboto Slab",
          marginTop: "1%",
        }}
      >
        Modifier un service
      </Typography>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div className={classes.form}>
          {/* nouvelle implementation */}

          <Grid container spacing={8} className={classes.formContent}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Libellé
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Libellé"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      District
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="District"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Contact 1
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Contact 1"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Direction
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Direction"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Email
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Email"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Contact 02
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Contact 02"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Ville
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Ville"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={4}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Quartier
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Quartier"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid container style={{ marginTop: "2%" }}>
            <Grid item xs={4}>
              <Button className={classes.btn1}>Annuler</Button>
            </Grid>
            <Grid item xs={4}>
              <Button className={classes.btn2}>Suivant</Button>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
};

export default ModifierUnService;
