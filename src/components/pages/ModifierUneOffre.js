import {
  Grid,
  Typography,
  InputBase,
  Button,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Avatar,
  Badge,
} from "@material-ui/core";
import { useStyleModifierUneOffre } from "./ModifierUneOffre.style";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import clsx from "clsx";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const ModifierUneOffre = () => {
  const classes = useStyleModifierUneOffre();
  return (
    <div className={classes.container}>
      <Grid container className={classes.menu}>
        <Grid item xs={6} className={classes.submenu}>
          Tableau de bord / Gestion des services
        </Grid>
        <Grid item xs={6} className={classes.btncontainer}>
          <Button startIcon={<ArrowBackIcon />} className={classes.btn}>
            {" "}
            Précédent
          </Button>
        </Grid>
      </Grid>
      <Typography
        component="small"
        variant="h6"
        style={{
          marginRight: "68%",
          color: "#757575",
          font: "28px/37px Roboto Slab",
          marginTop: "1%",
        }}
      >
        Modifier une offre
      </Typography>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div className={classes.form}>
          {/* nouvelle implementation */}
          <Grid container spacing={8} className={classes.formContent}>
            <Grid
              item
              xs={4}
              style={{ display: "flex", flexDirection: "column" }}
            >
              <Grid spacing={2}>
                <Grid item xs={6}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Libellé
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Libellé"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>

                <Grid item xs={6}>
                  <FormControl
                    className={clsx(classes.margin, classes.textField)}
                    variant="outlined"
                  >
                    <FormHelperText id="outlined-weight-helper-text">
                      Prix (unité)
                    </FormHelperText>
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      aria-describedby="outlined-weight-helper-text"
                      placeholder="Prix (unité)"
                      labelWidth={0}
                    />
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={8}>
              <Grid item xs={12}>
                <FormControl
                  className={clsx(classes.margin, classes.textField2)}
                  variant="outlined"
                >
                  <FormHelperText id="outlined-weight-helper-text">
                    Description
                  </FormHelperText>
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    aria-describedby="outlined-weight-helper-text"
                    placeholder="Description"
                    labelWidth={0}
                    rows={15}
                    multiline={true}
                  />
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ marginTop: "2%" }}>
            <Grid item xs={4}>
              <Button className={classes.btn1}>Annuler</Button>
            </Grid>
            <Grid item xs={4}>
              <Button className={classes.btn2}>Suivant</Button>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
};

export default ModifierUneOffre;
