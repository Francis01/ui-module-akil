
import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container'
import {useStylesMotif} from './Motif.style'
import groupe11389 from '../../assert/Groupe 11389.png'
import { InputBase, TextareaAutosize, TextField } from '@material-ui/core';









function Motif() {
    const classes = useStylesMotif();
  
    return (
      <Container component="main" maxWidth="md" >
        <CssBaseline />
        <div className={classes.paper}>
          
          <Grid container alignContent='center' alignItems='center' justify='center'>
          <Grid item xs={12} className={classes.space1}>
              <Grid container alignItems='center' alignContent='center' justify='center' >
                  <Grid item xs={4} alignItems='center' className={classes.text} >Motifs</Grid>
                  <Grid item xs={8} style={{maxWidth:'29%'}} container>
                      <img src={groupe11389} className={classes.img} />
                  </Grid>
              </Grid>
          </Grid>
          </Grid>
          
          <Grid item xs={12} style={{display:'flex',flexDirection:'column'}}>
             
             <small className={classes.small}>Commentaires</small> 
              
          <InputBase
          id="outlined-multiline-static"
          label="Commentaire"
          multiline
          rows={8}
          variant="outlined"
          className={classes.inp}
        />
          </Grid>

          <Grid item xs={12} style={{display:'flex',flexDirection:'column'}}>
             
            <div className={classes.rapport}>
                <span className={classes.textRapport}>
                Charger le rapport d'arbitrage
                </span>
           
            </div>
              
        
          </Grid>

          <form className={classes.form} noValidate>
            
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.btn1}
            >
              Valider
            </Button>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.btn2}
            >
              Annuler
            </Button>
          </form>
        </div>
      </Container>
    );
  }

  export default  Motif;