
import { makeStyles } from '@material-ui/core/styles';

export const useStylesMotif = makeStyles((theme) => ({
    paper: {/* 
      marginTop: theme.spacing(8), */
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      width:' 882px',
      height: '810px',
      borderRadius:'15px',
      border:'1px solid white',
      boxShadow:'0px 10px 23px #00000029'
    },
  
    form: {
       // Fix IE 11 issue.
      display: 'flex',
      flexDirection: 'row',
    },
    btn1: {
      margin: theme.spacing(3, 0, 2),
      backgroundColor:'#FF7A42',
      color:'#fff',
      width: '274px',
        height: '70px',
      borderRadius:'35px',
      textTransform:'capitalize',
      marginLeft:'20%',
      "&:hover":{
          backgroundColor:'#FF7A42'
      }
    },
    btn2: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor:'#fff',
        color:'#FF7A42',
        width: '274px',
        height: '70px',
        borderRadius:'35px',
        border:'1px solid #FF7A42',
        textTransform:'capitalize',
        alignItems:'center',
        marginLeft:'20%',
        
        "&:hover":{
            backgroundColor:'#fff',

        }
      },
    space1:{
        flexGrow:1,
        marginTop:theme.spacing(8),
        maxWidth: '826px',
        height: '123px',
        border:'0.5px solid #FF7A42',
        borderRadius:'11px',
        justifyContent:'center',
        alignItems:'center'
       
    },
    img:{/* 
        position:'absolute', */
        /* top:"20%", */
        height:'120px'
    },
    text:{
        maxWidth:'29%',
        /* marginTop:'50px', */
        fontSize:'25px',
        fontFamily:'roboto',
        color:'#757575'
    },
    inp:{ 
        width:'760px',
        border: '0.5px solid #707070',
        borderRadius:'11px',
        marginTop:theme.spacing(2)

    },
    small:{
        marginTop:theme.spacing(4)
    },
    rapport:{
        width:'760px',
        height:'103px',
        border: '1px dashed #2A2A2A',
        borderRadius:'11px',
        marginTop:theme.spacing(8),
        alignItems:'center',
        textAlign:'center',
        justifyContent:'center',
    
    },
    textRapport:{
        color:'#757575',
        font:'normal normal medium 25px/30px Roboto',
        height:'103px',
        width: '760px'
    }
    
    
  }));
  

  