import { Grid, Typography, Button, Avatar, Badge } from "@material-ui/core";
import { useStyleProfile } from "./Profile.style";
import assetou from "../../assert/assetou.png";
import HeaderDegrade from "../layouts/headers/headerDegrade";
import Precedent from "../layouts/boutton/Precedent";
import TextFields from "../layouts/input/TextField";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";
import { UseStylesTextField } from "../layouts/input/TextField.style";

const Profile = () => {
  const classes = useStyleProfile();
  return (
    <div className={classes.container}>
      <HeaderDegrade
        title="Tableau de bord / liste des contribuables. / Coulibaly Assetou"
        style={{overflow:"hidden"}}
        precedent={
          <Precedent
            startIcon={<ArrowBackIcon />}
            title="Précédent"
            style={UseStylesBoutton().btnPriv}
          />
        }
      />
      <Grid align="left"
        style={{
          color: "#757575",
          font: "28px/37px Roboto Slab",
          width:"95%",marginLeft:'auto',
          marginTop:"2%",
          marginBottom:"2%",
          
        }}
      >
        Profil
      </Grid>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          height: "80%",
        }}
      >
        <div className={classes.form}>
          <Grid container spacing={8} className={classes.formContent}>
            <Grid item xs={4} className={classes.profileContainer}>
              <Grid item xs={6}>
                <Avatar className={classes.avatar} src={assetou} />
              </Grid>
              <Grid item xs={6} className={classes.textAvatar}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et
              </Grid>
            </Grid>

            <Grid item xs={8}>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <TextFields
                    placeholder="Entrer votre numéro matricule"
                    label="Numéro matricule *"
                    variant="outlined"
                    style={UseStylesTextField().profile}
                  />
                </Grid>

                <Grid item xs={6}>
                  <TextFields
                    placeholder="Entrer votre nom "
                    label="Nom *"
                    variant="outlined"
                    style={UseStylesTextField().profile}
                  />
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <TextFields
                    placeholder="Entrer votre prénom(s)"
                    label="Prénom(s) *"
                    variant="outlined"
                    style={UseStylesTextField().profile}
                  />
                </Grid>

                <Grid item xs={6}>
                  <TextFields
                    placeholder="Entrer votre contact"
                    label=" Contact 02 *"
                    variant="outlined"
                    style={UseStylesTextField().profile}
                  />
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <TextFields
                    placeholder="Entrer votre email"
                    label=" Email *"
                    variant="outlined"
                    style={UseStylesTextField().profile}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </div>

      <Grid container className={classes.ul} alignItems="center" alignContent="center">
        <Grid item xs={6} alignItems="left">
          <Boutton name="Enregistrer" style={UseStylesBoutton().orangeBtn2} />
        </Grid>
        <Grid item xs={6} alignItems="left">
          <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn2} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Profile;
