import { makeStyles, withStyles } from "@material-ui/core/styles";

export const useStyleProfile = makeStyles((theme) => ({
  container: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    height: "100vh",
    width: "100vw",
    
  },
  menu: {
    width: "1920",
    height: "80px",
    backgroundColor: "#267466",
    position: "relative",
  },
  submenu: {
    fontSize: "18px",
    display: "flex",
    flexDirection: "column",
    color: "#fff",
    fontFamily: "roboto",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  btn: {
    border: "1px solid white",
    borderRadius: "24px",
    textTransform: "capitalize",
    color: "white",
    display: " flex",
    flexDirection: "row-reverse",
    alignItems: "center",
    justifyContent: "center",
    width: "150px",
    position: "absolut",
    right: "-90px",
  },
  btncontainer: {
    alignItems: "center",
    justifyContent: "center",
    display: " flex",
    flexDirection: "row-reverse",
  },
  form: {
    border: "0.5px solid #707070",
    borderRadius: "20px",
    height: "auto",
    width:'90%',
    color: "#f9f9f9",
    backgroundColor: "#2C282812",
    padding:"2em"
    /* 
    position: "absolute",
    right: "10%",
    left: "10%",
    top: "20%", */
  },
  formContent: {
    marginTop: "2%",
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: "35ch",
    borderRadius: "11px",
    backgroundColor: "#fcfcfc",
  },
  btn1: {
    border: "1px solid #FF7A42",
    borderRadius: "35px",
    opacity: "1px",
    color: "#FF7A42",
    textTransform: "capitalize",
    width: "200px",
    height: " 50px",/* 
    marginLeft: "4%", */
  },
  btn2: {
    backgroundColor: "#FF7A42",
    border: "1px solid #fff",
    borderRadius: "35px",
    opacity: "1px",
    color: "#fff",
    textTransform: "capitalize",
    width: "200px",
    height: "50px",/* 
    marginRight: "4%", */

    "&:hover": {
      backgroundColor: "#FF7A42",
    },
  },
  /* nouvelle implementation */
  profile: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "red",
  },
  profileContainer: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    align: "center",
    flexDirection: "column",
    
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    width: "159px",
    height: "159px",
    border: "1px solid #707070",
  },
  textAvatar: {
    color: "#757575",
    font: "normal normal normal 16px/19px Roboto",
    textAlign: "center",
    marginTop: theme.spacing(4),
  },
  ul:{
    
    marginTop:'5%',



  }
}));
