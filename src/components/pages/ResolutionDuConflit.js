import React, { useState } from "react";
import * as Mui from "@material-ui/core";
import {
  useStyleResolutionDuConflit,
  StyledTableCell,
  StyledTableRow,
} from "./ResolutionDuConflit.style";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import PrintIcon from "@material-ui/icons/Print";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import strataige from "../../assert/Groupe 11580.png";
import TelegramIcon from "@material-ui/icons/Telegram";

function ResolutionDuConflit() {
  const classes = useStyleResolutionDuConflit();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.container}>
      <div className={classes.submenu}>
        <Mui.Grid container className={classes.btnMenu}>
          <Mui.Grid item xs={12} container>
            <Mui.Grid item xs={6} style={{ fontSize: "x-small" }}>
              Tableau de bord / liste des contribuables. / Coulibaly Assetou
            </Mui.Grid>
            <Mui.Grid item xs={6} align="right">
              <Mui.Button
                size="large"
                startIcon={<ArrowBackIcon />}
                className={classes.previewBtn}
              >
                Précedent
              </Mui.Button>
            </Mui.Grid>
          </Mui.Grid>
        </Mui.Grid>

        <Mui.Grid container className={classes.menu2}>
          <Mui.Grid item xs={6} container>
            <Mui.Grid item xs={6}>
              <Mui.Typography variant="h6">
                Résolution de conflits
              </Mui.Typography>
            </Mui.Grid>

            <Mui.Grid item xs={6}>
              <Mui.FormControl className={classes.formInp} variant="outlined">
                <Mui.InputBase
                  type="text"
                  fullWidth="true"
                  placeholder="Filtre par"
                  className={classes.inp}
                  endAdornment={
                    <Mui.InputAdornment position="end">
                      <Mui.IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                      >
                        <small
                          style={{
                            color: "#fff",
                            fontSize: "small",
                            fontWeight: "bold",
                          }}
                        >
                          En cours
                        </small>
                        <KeyboardArrowDownIcon style={{ color: "#fff" }} />
                      </Mui.IconButton>
                    </Mui.InputAdornment>
                  }
                  labelWidth={20}
                />
              </Mui.FormControl>
            </Mui.Grid>
          </Mui.Grid>

          <Mui.Grid item xs={6} container>
            <Mui.Grid item xs={8}>
              <Mui.InputBase
                fullWidth="true"
                style={{
                  color: "#fff",
                  borderBottom: "1px solid white",
                  fontSize: "15px",
                }}
                placeholder="Faite vos recherche ici"
              />
            </Mui.Grid>
            <Mui.Grid item xs={4} className={classes.icons}>
              <Mui.IconButton>
                <PrintIcon className={classes.icons} fontSize="large" />{" "}
              </Mui.IconButton>
              <Mui.IconButton>
                <PictureAsPdfIcon className={classes.icons} fontSize="large" />
              </Mui.IconButton>
            </Mui.Grid>
          </Mui.Grid>
        </Mui.Grid>
      </div>

      <Mui.Grid className={classes.container2} container>
        <Mui.Grid item xs={12} style={{ display: "flex" }}>
          <Mui.Container component="main" className={classes.strataige}>
            <Mui.CssBaseline />
            <div className={classes.paper}>
              <Mui.Typography component="h1" variant="h5">
                Strataige Expert S.A
              </Mui.Typography>
              <Mui.Avatar className={classes.avatar} src={strataige} />

              <form className={classes.form} noValidate>
                <Mui.Grid container justify="center">
                  <Mui.Grid item xs={6} className={classes.contentLeft}>
                    Registre de commerce
                  </Mui.Grid>

                  <Mui.Grid item xs={6} className={classes.contentRight}>
                    CI-ABJ-2020-A-2020
                  </Mui.Grid>
                </Mui.Grid>

                <Mui.Grid container spacing={4}>
                  <Mui.Grid item xs={6} className={classes.contentLeft}>
                    Compte contribuable
                  </Mui.Grid>
                  <Mui.Grid item xs={6} className={classes.contentRight}>
                    1111111B
                  </Mui.Grid>
                </Mui.Grid>

                <Mui.Grid container spacing={4}>
                  <Mui.Grid item xs={6} className={classes.contentLeft}>
                    Date de conflit
                  </Mui.Grid>
                  <Mui.Grid item xs={6} className={classes.contentRight}>
                    20/12/2019
                  </Mui.Grid>
                </Mui.Grid>

                <Mui.Grid container spacing={4}>
                  <Mui.Grid item xs={6} className={classes.contentLeft}>
                    Mission
                  </Mui.Grid>
                  <Mui.Grid item xs={6} className={classes.contentRight}>
                    Non renseigner
                  </Mui.Grid>
                </Mui.Grid>
              </form>
            </div>
          </Mui.Container>

          <Mui.Container component="main">
            <Mui.TableContainer
              component={Mui.Paper}
              className={classes.tableContainer}
            >
              <Mui.Table
                className={classes.table}
                aria-label="customized table"
              >
                <Mui.TableHead>
                  <Mui.TableRow>
                    <StyledTableCell>IDU</StyledTableCell>
                    <StyledTableCell align="right">CCU</StyledTableCell>
                    <StyledTableCell align="right">RCCU</StyledTableCell>
                    <StyledTableCell align="right"> Mission</StyledTableCell>
                    <StyledTableCell align="right"> Confrères</StyledTableCell>
                    <StyledTableCell align="right">
                      {" "}
                      Date conflit
                    </StyledTableCell>
                    <StyledTableCell align="center"> Action</StyledTableCell>
                    <StyledTableCell align="center"> </StyledTableCell>
                  </Mui.TableRow>
                </Mui.TableHead>
                <Mui.TableBody>
                  <StyledTableRow>
                    <StyledTableCell component="th" scope="row">
                      5555555555A
                    </StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                    <StyledTableCell align="right">
                      Commissariat
                    </StyledTableCell>
                    <StyledTableCell align="right">Strataige</StyledTableCell>
                    <StyledTableCell align="right">20/12/2019</StyledTableCell>
                    <StyledTableCell align="center">
                      <Mui.Button
                        variant="contained"
                        size="medium"
                        startIcon={<TelegramIcon />}
                        className={classes.tabBtn1}
                      >
                        Transférer
                      </Mui.Button>
                    </StyledTableCell>

                    <StyledTableCell align="center">
                      <Mui.Button
                        variant="contained"
                        size="medium"
                        className={classes.tabBtn2}
                      >
                        Voir la lettre de mission
                      </Mui.Button>
                    </StyledTableCell>
                  </StyledTableRow>

                  <StyledTableRow>
                    <StyledTableCell component="th" scope="row">
                      5555555555A
                    </StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                    <StyledTableCell align="right"></StyledTableCell>
                    <StyledTableCell align="right">
                      Commissariat
                    </StyledTableCell>
                    <StyledTableCell align="right">Sejen SA</StyledTableCell>
                    <StyledTableCell align="right">20/12/2019</StyledTableCell>
                    <StyledTableCell align="center">
                      <Mui.Button
                        variant="contained"
                        size="medium"
                        startIcon={<TelegramIcon />}
                        className={classes.tabBtn1}
                      >
                        Transférer
                      </Mui.Button>
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <Mui.Button
                        variant="contained"
                        size="medium"
                        className={classes.tabBtn2}
                      >
                        Voir la lettre de mission
                      </Mui.Button>
                    </StyledTableCell>
                  </StyledTableRow>
                </Mui.TableBody>
              </Mui.Table>
            </Mui.TableContainer>
          </Mui.Container>
        </Mui.Grid>
      </Mui.Grid>
    </div>
  );
}

export default ResolutionDuConflit;

