import { makeStyles, withStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

export const useStyleResolutionDuConflit = makeStyles((theme) => ({
  container: {
    flexGrow: 1,
    height: "100vh",
  },
  submenu: {
    backgroundColor: "#267466 ",
    color: "#fff",
    height: "30vh",
    width: "100%",
  },

  btnMenu: {
    position: "absolute",
    top: "5%",
    left: "5%",
    width: "90%",
  },
  menu2: {
    position: "absolute",
    top: "15%",
    left: "5%",
    width: "90%",
  },
  formInp: {
    width: "80%",
    height: "20%",
  },
  inp: {
    borderRadius: "18px !important",
    color: "#fff",
    borderColor: "#fff !important",
    border: "1px solid white",
    height: "40px",
    padding: theme.spacing(2),
  },
  previewBtn: {
    border: "1px solid white",
    borderRadius: "18px",
    textTransform: "capitalize",
    color: "white",
    width: "175px",
    position: "absolute",
    right: "5%",
  },

  tabBtn: {
    borderRadius: "18px",
    backgroundColor: "green",
    color: "#fff",
    height: "150%",
    width: "100%",
    textAlign: "center",
    textTransform: "capitalize",
    fontSize: "x-small",
  },

  tabBtn1: {
    borderRadius: "18px",
    backgroundColor: "#256F9D",
    color: "#fff",
    width: "138px",
    height: "40px",
    textAlign: "center",
    textTransform: "capitalize",
    fontSize: "x-small",
    "&:hover": {
      backgroundColor: "#256F9D",
    },
  },
  icons: {
    color: "#fff",
    height: "150%",
  },

  tabBtn2: {
    borderRadius: "18px",
    backgroundColor: "#FF7A42",
    color: "#fff",
    height: "40px",
    width: "174px",
    textAlign: "center",
    textTransform: "capitalize",
    fontSize: "x-small",
    "&:hover": {
      backgroundColor: "#FF7A42",
    },
  },

  tabBtn3: {
    borderRadius: "18px",
    backgroundColor: "#108D23",
    color: "#fff",
    height: "40px",
    width: "138px",
    textAlign: "center",
    textTransform: "capitalize",
    fontSize: "x-small",
    "&:hover": {
      backgroundColor: "#108D23",
    },
  },
  iconColor: {
    color: "#2F8B7B",
  },
  strataigeForm: {
    flexFlow: 1,
    padding: theme.spacing(4),
    /*  backgroundColor:theme.palette.common.black, */
    height: "100%",
    borderRadius: "20px",
    textAlign: "center",
  },

  container2: {
    height: "70vh",
  },
  strataigeForm: {
    maxWidth: "40wh",
    backgroundColor: "#f9f9f9",
    height: "65vh",
    borderRadius: "18px",
    border: "1px solid #707070",
  },
  paper: {
    marginTop: theme.spacing(6.5),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#F9F9F9",
    height: "100%",
    borderTopLeftRadius: "20px",
    borderBottomLeftRadius: "20px",
    width: "100%",
    marginLeft: "50px",
    border: "1px solid #707070",
    backgroundColot:'gray',
    padding:"2em"
  },
  strataige: {
    maxWidth: "510px",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
    width: "164px",
    height: "164px",
    border: "1px solid #707070",
  },
  form: {
    height: "60px",
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(4),
  },

  formContainer: {
   /*  marginLeft: "25%", */
  },
  contentLeft: {
    color: "#707070",
    font: "roboto",
    fontWeight: "bold",
  },
  contentLeft: {
    color: "#707070;",
    fontSize: "17px",
    letterSpacing: "0px",
    font: "roboto",
  },
  table: {
    width: "100%",
  },
  tableContainer: {
    height: "70vh",
    backgroundColor: "#F9F9F9",
    border: "1px solid #707070",
    borderBottomRightRadius: "20px",
    borderTopLeftRadius: "20px",
    borderTopRightRadius: "20px",
    marginTop: "-10px",
    width: "100%",
  },
}));

export const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.gray,
    fontSize: 10,
  },
  body: {
    fontSize: 10,
  },
}))(TableCell);

export const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
