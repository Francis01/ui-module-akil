import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import imgWork from "../../assert/Groupe 11295.png";
import { UseStaticStyles } from "./Static.style";
import GridPaper from "../layouts/grid/gridPaper";
import { UseGridSimpleStyle } from "../layouts/grid/GridSimple.style";
import { Link } from "react-router-dom";
function Static() {
  const classes = UseStaticStyles();

  return (
    <div className={classes.root}>
      <div className={classes.containers}>
        <Grid container spacing={2} className={classes.gridContainer}>
          <Grid item md={4} xs={12}>
            <Grid container className={classes.paper}>
              <Grid item md={7} xs={6}>
                <Grid spacing={2} style={{ width: "95%" }}>
                  <Grid item md={12} style={{ fontSize: "50px" }}>
                    Hello!
                  </Grid>
                  <Grid
                    item
                    md={12}
                    style={{ fontSize: "30px", fontWeight: "bold" }}
                  >
                    Guei dessekane
                  </Grid>
                  <Grid item md={12} style={{ fontSize: "15px" }}>
                    Bienvenue dans votre espace, nous vous souhaitons une
                    excellente journée
                  </Grid>
                </Grid>
              </Grid>
              <Grid item md={5} xs={4}>
                <img src={imgWork} style={{ width: "150px" }} />
              </Grid>
            </Grid>
          </Grid>

          <Grid container item md={8} xs={12}>
            <Grid container spacing={2} className={classes.paper}>
              <Grid
                md={12}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  boxShadow: "0px 5px 3px #0000000D",
                  width: "100%",
                }}
              >
                <Grid md={12} align="left">
                  <strong>Liasses</strong>
                </Grid>
                <Grid md={12} align="right" style={{ color: "#FF7A42" }}>
                  Total liasses : 250000000
                </Grid>
              </Grid>

              <Grid container md={12}>
                <Grid item md={3} xs={3} className={classes.borderR}>
                  <div
                    style={{
                      height: "100px",
                      marginTop: "10%",
                      marginBottom: "10%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                      fontSize: "15px",
                    }}
                  >
                    <Grid item md={12}>
                      <strong>En attente</strong>
                    </Grid>
                    <Grid item md={12}>
                      2500000
                    </Grid>
                  </div>
                </Grid>
                <Grid item md={3} xs={3} className={classes.borderR}>
                  <div
                    style={{
                      height: "100px",
                      marginTop: "10%",
                      marginBottom: "10%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                      fontSize: "15px",
                    }}
                  >
                    <Grid item md={12}>
                      <strong>Transmise(s) hors délais</strong>
                    </Grid>
                    <Grid item md={12}>
                      2500000
                    </Grid>
                  </div>
                </Grid>
                <Grid item md={3} xs={3} className={classes.borderR}>
                  <div
                    style={{
                      height: "100px",
                      marginTop: "10%",
                      marginBottom: "10%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                      fontSize: "15px",
                    }}
                  >
                    <Grid item md={12}>
                      <strong>Rejetée(s)</strong>
                    </Grid>
                    <Grid item md={12}>
                      2500000
                    </Grid>
                  </div>
                </Grid>
                <Grid item md={3} xs={3} className={classes.borderN}>
                  <div
                    style={{
                      height: "100px",
                      marginTop: "10%",
                      marginBottom: "10%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-between",
                      fontSize: "15px",
                    }}
                  >
                    <Grid item md={12}>
                      <strong>Transmise(s)</strong>
                    </Grid>
                    <Grid item md={12}>
                      2500000
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item md={4}>
            <Link
              to="/tableau-de-bord-contribuable"
              style={{ textDecoration: "none" }}
            >
              <GridPaper
                style={UseStaticStyles()}
                headerTitle="Contribuables"
                headerTitlePrix="1200000"
                content1="Appartenant à des experts-comptables"
                content1Prix="32 000"
                content2="Adhérents CGA"
                content2Prix="20 0000"
                content3="Non affectés"
                content3Prix="68 0000"
                content4="Soumis à visa"
                content4Prix="47 0000"
              />
            </Link>
          </Grid>
          <Grid item md={4}>
            <Link
              to="/tableau-de-bord-commissariat"
              style={{ textDecoration: "none" }}
            >
              <GridPaper
                style={UseStaticStyles()}
                headerTitle="Seuil de commissariat"
                headerTitlePrix="7000"
                content1="Nombre de contribuables qui doivent changer de régime de contrôle"
                content1Prix="6 000"
              />
            </Link>
          </Grid>

          <Grid item md={4}>
            <Link to="/chiffre-daffaire" style={{ textDecoration: "none" }}>
              <GridPaper
                style={UseStaticStyles()}
                headerTitle="Chiffre d'affaire de la profession"
                headerTitlePrix="100 000 000 000 (En fCFA)"
                content1="Nombre de contribuables qui doivent changer de régime de contrôle"
                content1Prix="32 000"
              />
            </Link>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
export default Static;
