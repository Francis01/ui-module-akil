import { makeStyles } from "@material-ui/core";
export const UseStaticStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "15px",
    height: "100%",
    color: "#757575",
  },

  containers: {
    width: "95%",
    marginRight: "auto",
    marginLeft: "auto",
  },
  paper: {
    flexGrow: 1,
    borderRadius: "18px",
    backgroundColor: "#fff",
    minHeight: "251px",
    alignItems: "center",
    textAlign: "center",
    padding: "1em",
    boxShadow: "0px 2px 5px #ccc",
    boxSizing:"boder-box",
    overflow:"none",
    
  },
  paper2: {
    flexGrow: 1,
    borderRadius: "18px",
    backgroundColor: "#fff",
    minHeight: "251px" ,
    boxShadow: "0px 2px 5px #ccc",
    boxSizing:"boder-box",
 

  },
  paperTab: {
    flexGrow: 1,
    borderRadius: "18px",
    backgroundColor: "#fff",
    alignItems: "center",
    textAlign: "center",
    padding: "1em",
    boxShadow: "0px 0px 5px 0px gray",
  },

  gridContainer: {
    alignItems: "center",
    width: "100%",
    /* backgroundColor:"red", */
    height: "auto",
    color: "#757575",
  },

  divChild: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    /*  backgroundColor:"gray" */
  },
  borderR: {
    minHeight: "150px",
    width: "10%" /* 
        borderRight: '0px solid #7B7B7B', */,
    opacity: "1",
    boxShadow: "1px 0px #7B7B7B",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    justifySelf: "center",
  },

  borderN: {
    minHeight: "150px",
    width: "10%",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    justifySelf: "center",
    marginTop: "auto",
    marginBottom: "auto",
  },
  headerGrid: {
    flexGrow: 1,
    background: "#FFF",
    borderRadius: "18px",
    minHeight: "100px",
    padding: "2em",
    boxShadow: "0px 0px 9px 0px #0000000D",
    boxShadow: "0px 0px 5px 0px gray",
  },

  headerGridOrange: {
    flexGrow: 1,
    background: "#FF7A42 0% 0% no-repeat padding-box",
    borderRadius: "18px",
    minHeight: "70px",
    padding: "1.5em",
    boxShadow: "0px 0px 5px 0px gray",
  },

  headerGridWhite: {
    flexGrow: 1,
    background: "#FFF0% 0% no-repeat padding-box",
    borderRadius: "18px",
    minHeight: "70px",
    padding: "1.5em",
    boxShadow: "0px 0px 1px 0px gray",
    color: "ihnerit",
  },

  titleHeaderOrange: {
    color: "#FF7A42",
    fontSize: "17px",
    fontWeight:"bold"
  },
  prixHeaderOrange: {
    fontSize: "10px",
  },
  bodyGrid: {
    flexGrow: 1,
    /* minHeight:'200px', */
    height: "auto",
    padding: "1.5em",
    fontSize: "13px",
  },
  prix: {
    color: "#267466",
  },
  titleHeader: {
    color: "#FF7A42",
  },
}));
