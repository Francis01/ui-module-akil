import {makeStyles} from "@material-ui/core";

export const UseStylesStatistiquesContribuables=makeStyles((theme)=>({

    legendText:{
        fontSize:"12px",
        color:"#636363",
        font: "normal normal medium 17px/20px Roboto"
    },
    legendTitle:{
        color:"#636363",
        font: "normal normal medium 17px/20px Roboto",
        fontWeight:"bold",
        fontSize:"12px"
    },
    
    legendIconBleu:{
        width:"17px",
        height:"17px",
        borderRadius:"50%",
        background:"#0C88BF"
    },
    legendIconOrange:{
        width:"17px",
        height:"17px",
        borderRadius:"50%",
        background:"#FF7A42"
    },
    legendIconGreen:{
        width:"17px",
        height:"17px",
        borderRadius:"50%",
        background:"#319441"
    },
    textContent:{
        fontSize:"12px",
        padding:"1em"
    },
    prix:{
        color:"#CE332D",
        font: "normal normal medium 14px/17px Roboto",

    },
    country:{
        color:"#FF7A42",
        font: "normal normal normal 14px/17px Roboto"
    }

}))