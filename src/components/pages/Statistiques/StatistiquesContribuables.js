import { Grid, Paper,Divider } from "@material-ui/core";
import { UseStaticStyles } from "../Static.style";
import React from "react";
import {
  DataChartPie,
  DataChartPieNombreDeContribuable,
  DataChartPieNombreDeContribuableParDirectionRegionale,
  DataChartPieNombreEtatFinancier,
  DataChartPieDirectionRegional,
} from "../../../@store/DataChart";
import { Pie } from "react-chartjs-2";
import { UseStylesStatistiquesContribuables } from "./StatiquesContribuables.style";
import NavBar from "../../layouts/NavBar";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import PaginationRounded from "../../layouts/pagination/Pagination"

const StatistiquesContribuables = () => {
  const classes = UseStylesStatistiquesContribuables();
  return (
    <div>
      <NavBar/>
      <HeaderDegrade title="Contribuable"/>
      <Grid container  style={{width:"95%",marginLeft:"auto",marginRight:"auto",marginTop:"2%"}}>
        <Grid item md={12} container spacing={2}>
          <Grid md={4} xs={4}>
            <Paper className={UseStaticStyles().paper}>
              <Pie
                data={DataChartPieNombreDeContribuable}
                options={
                  DataChartPieNombreDeContribuable.DataChartPieOptionNombreDeContribuable
                }
              />
            </Paper>
          </Grid>

          <Grid container item md={5} xs={4} justify="center" alignItems="center" alignContent='center'>
            <Paper className={UseStaticStyles().paper}>
              <Grid container item md={12} xs={12} justify="center" alignItems="center" alignContent='center'>
                <Grid md={4} xs={4}>
                  <Grid item md={12} className={classes.legendTitle}>
                    Régime IS
                  </Grid>
                  <Grid item md={12}>
                    <Pie
                      data={
                        DataChartPieNombreDeContribuableParDirectionRegionale
                      }
                      options={
                        DataChartPieNombreDeContribuableParDirectionRegionale.DataChartPieOptionNombreDeContribuableParDirectionRegionale
                      }
                    />
                  </Grid>

                  <Grid item md={12}>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                      justify="center" alignItems="center" alignContent='center'
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconBleu}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 1
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center" justify="center" alignItems="center" alignContent='center'
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconOrange}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 2
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconGreen}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 3
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid md={4} xs={4} justify="center" alignItems="center" alignContent='center'>
                  <Grid item md={12} justify="center" alignItems="center" alignContent='center' className={classes.legendTitle}>
                    Régime RIS
                  </Grid>
                  <Grid item md={12} justify="center" alignItems="center" alignContent='center'>
                    <Pie
                      data={
                        DataChartPieNombreDeContribuableParDirectionRegionale
                      }
                      options={
                        DataChartPieNombreDeContribuableParDirectionRegionale.DataChartPieOptionNombreDeContribuableParDirectionRegionale
                      }
                    />
                  </Grid>
                  <Grid item md={12}>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                      justify="center" alignItems="center" alignContent='center'
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconBleu}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 1
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconOrange}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 2
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconGreen}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 3
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

                <Grid md={4} xs={4} justify="center" alignItems="center" alignContent='center'>
                  <Grid item md={12} className={classes.legendTitle}>
                    Régime Réel
                  </Grid>
                  <Grid item md={12} justify="center" alignItems="center" alignContent='center'>
                    <Pie
                      data={
                        DataChartPieNombreDeContribuableParDirectionRegionale
                      }
                      options={
                        DataChartPieNombreDeContribuableParDirectionRegionale.DataChartPieOptionNombreDeContribuableParDirectionRegionale
                      }
                    />
                  </Grid>
                  <Grid item md={12}>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconBleu}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 1
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconOrange}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 2
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                      container
                      alignItem="center"
                      alignContent="center"
                    >
                      <Grid
                        item
                        md={1}
                        className={classes.legendIconGreen}
                      ></Grid>
                      <Grid item md={11} className={classes.legendText}>
                        Région 3
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid md={3} xs={3} justify="center" alignItems="center" alignContent='center'>
            <Paper className={UseStaticStyles().paper}>
                <Pie
                  data={DataChartPieNombreEtatFinancier}
                  options={
                    DataChartPieNombreEtatFinancier.DataChartPieOptionDataChartPieNombreEtatFinancier
                  }
                />
            </Paper>
          </Grid>

          <Grid item md={7} xs={7} container spacing={2}>
            <Grid item md={6} xs={6} justify="center" alignItems="center" alignContent='center'>
              <Paper className={UseStaticStyles().paper}>
                  <Pie
                    data={DataChartPieDirectionRegional}
                    options={
                      DataChartPieDirectionRegional.DataChartPieOptionDirectionRegional
                    }
                  />
              </Paper>
            </Grid>

            <Grid item md={6} xs={6} justify="center" alignItems="center" alignContent='center'>
              <Paper className={UseStaticStyles().paper}>
                  <Pie
                    data={DataChartPieDirectionRegional}
                    options={
                      DataChartPieDirectionRegional.DataChartPieOptionDirectionRegional
                    }
                  />
              </Paper>
            </Grid>
          </Grid>

          <Grid container item md={5} xs={5} justify="center" alignItems="center" alignContent='center'>
            <Grid container spacing={2} className={UseStaticStyles().paper}>
          <Grid md={12} xs={12} align="left" style={{font: "normal normal medium 17px/20px Roboto",color:" #636363"}}>Services des impôts</Grid>
          <Grid md={12} xs={12} align="left" style={{font:" normal normal medium 14px/17px Roboto",color: "#CE332D",fontSize:"12px"}}>Total services : 7000</Grid>
                <Grid item md={6} xs={6} justify="center" alignItems="center" alignContent='center' >
                  <Paper className={classes.textContent}>
                    <Grid item md={12} xs={12} style={{padding:"1em 0px 1em 0px"}}  align="left">
                      <strong>Abidjan Nord</strong>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'entreprises:</span><span className={classes.prix}>8000</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Repartition des entreprises:</span> <span className={classes.prix}>7 services</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span className={classes.country}>(Palmeraie,Yopougon,II plateau,Djibi...)</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'etats financiers attendu:</span><span className={classes.prix}>105220</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'etats financiers déposé:</span><span className={classes.prix}>105220</span>
                    </Grid>
                  </Paper>
                </Grid>

                <Grid item md={6} xs={6} justify="center" alignItems="center" alignContent='center'>
                  <Paper className={classes.textContent}>
                    <Grid item md={12} xs={12} style={{padding:"1em 0px 1em 0px"}} align="left">
                     <strong>Abidjan Nord</strong> 
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'entreprises:</span> <span className={classes.prix}>8000</span> 
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Repartition des entreprises</span> <span className={classes.prix}>7 services</span> 
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span className={classes.country}>(Palmeraie,Yopougon,II plateau,Djibi...)</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'etats financiers attendu:</span><span className={classes.prix}>105220</span>
                    </Grid>
                    <Divider/>
                    <Grid item md={12} xs={12}>
                      <span>Nombre d'etats financiers déposé:</span><span className={classes.prix}>105220</span>
                    </Grid>
                  </Paper>
                </Grid>
               <Grid item md={12} xs={12} alignItems="center" ><PaginationRounded/></Grid> 
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default StatistiquesContribuables;
