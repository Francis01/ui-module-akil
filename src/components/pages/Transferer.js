
import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container'
import {useStylesTransferer} from './Transferer.style'
import groupe11389 from '../../assert/Groupe 11389.png'









function Transferer() {
    const classes = useStylesTransferer();
  
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          
          <Grid container alignContent='center' alignItems='center' justify='center'>
          <Grid item xs={12} className={classes.space1}>
              <Grid container alignItems='center' alignContent='center' justify='center' >
                  <Grid item xs={4} alignItems='center' className={classes.text} >Transfert</Grid>
                  <Grid item xs={8} style={{maxWidth:'29%'}} container>
                      <img src={groupe11389} className={classes.img} />
                  </Grid>
              </Grid>
          </Grid>
          </Grid>
          
          <Typography component="h1" variant="h5" style={{font:'roboto',
        color:'#757575',}}>
          Voulez-vous <br/> Transférer ?
          </Typography>
          <form className={classes.form} noValidate>
            
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.btn1}
            >
              Continuer
            </Button>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.btn2}
            >
              Annuler
            </Button>
          </form>
        </div>
      </Container>
    );
  }

  export default  Transferer;