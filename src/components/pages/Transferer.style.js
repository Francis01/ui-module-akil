
import { makeStyles } from '@material-ui/core/styles';

export const useStylesTransferer = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      width: '446px',
      maxHeight: '663px',
      borderRadius:'15px',
      border:'1px solid white',
      boxShadow:'0px 10px 23px #00000029'
    },
  
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    btn1: {
      margin: theme.spacing(3, 0, 2),
      backgroundColor:'#FF7A42',
      color:'#fff',
      width:' 274px',
      height: '70px',
      borderRadius:'35px',
      textTransform:'capitalize',
      marginLeft:'20%',
      "&:hover":{
          backgroundColor:'#FF7A42'
      }
    },
    btn2: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor:'#fff',
        color:'#FF7A42',
        width:' 274px',
        height: '70px',
        borderRadius:'35px',
        border:'1px solid #FF7A42',
        textTransform:'capitalize',
        alignItems:'center',
        marginLeft:'20%',
        
        "&:hover":{
            backgroundColor:'#fff',

        }
      },
    space1:{
        flexGrow:1,
        marginTop:theme.spacing(8),
        maxWidth: '390px',
        height: '123px',
        border:'0.5px solid #FF7A42',
        borderRadius:'11px',
        justifyContent:'center',
        alignItems:'center'
       
    },
    img:{/* 
        position:'absolute', */
        /* top:"20%", */
        height:'120px'
    },
    text:{
        maxWidth:'29%',
        /* marginTop:'50px', */
        fontSize:'25px',
        fontFamily:'roboto',
        color:'#757575'
    }
  }));
  

  