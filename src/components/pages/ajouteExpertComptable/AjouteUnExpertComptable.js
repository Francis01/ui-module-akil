import { useStylesAjouteUnExpertComptable } from "./AjouteUnExpertComptable.style";
import { Avatar, Button, IconButton, Paper } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import Precedent from "../../layouts/boutton/Precedent";
import TextFields from "../../layouts/input/TextField";
import { UseStylesTextField } from "../../layouts/input/TextField.style";
import Boutton from "../../layouts/boutton/Boutton";
import { Col, Container, Row, Table } from "react-bootstrap";
import Bonome from "../../../assert/bonome.png";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";

const AjouteUnExpertComptable = () => {
  const classes = useStylesAjouteUnExpertComptable();
  return (
    <div className={classes.root}>
      <HeaderDegrade
        precedent={
          <Precedent
            startIcon={<ArrowBackIcon />}
            title="Précédent"
            style={UseStylesBoutton().btnPriv}
          />
        }
        title="Tableau de bord / Ajouter un expert comptable"
      />
      <div>
        <h1>Ajouter un expert comptable</h1>
      </div>
      <div>Type de personne</div>

      <div className={classes.radio}>
        <div>
          <input
            type="radio"
            id="contactChoice1"
            name="contact"
            value="email"
            checked
          />
          <label for="contactChoice1">Personne physique</label>
        </div>

        <div>
          <input
            type="radio"
            id="contactChoice1"
            name="contact"
            value="email"
            checked
          />
          <label for="contactChoice1">Société expert comptable</label>
        </div>

        <div>
          <input
            type="radio"
            id="contactChoice1"
            name="contact"
            value="email"
            checked
          />
          <label for="contactChoice1">CGA</label>
        </div>
      </div>
    </div>
  );
};
export default AjouteUnExpertComptable;
