import { makeStyles } from "@material-ui/core";

export const useStylesAjouteUnExpertComptable = makeStyles((theme) => ({
  root: {
    flexFlow: 1,
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    /* 
        alignItems:'center',
        justifyContent:'center' */
  },
  header: {
    background: "linear-gradient(30deg, #267466, #267466 70%,#378D7D 15px)",
    minHeight: "10%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  txt: {
    font: " normal normal medium 17px/20px Roboto",
    color: "#fff",
    fontSize: "10px",
  },
  row1Txt1: {
    fontWeight: "bold",
    font: " normal normal medium 17px/20px Roboto",
  },
  row1Txt2: {
    fontWeight: "small",
    color: "#686868",
    font: "normal normal medium 18px/22px Roboto",
  },
  btnPriv: {
    borderRadius: "10px 10px 10px 10px",
    color: "#fff",
    border: "1px solid white",
    textTransform: "capitalize",
  },
  content: {
    marginTop: theme.spacing(1),
    justifyContent: "space-between",
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  imgContainer: {
    justifyContent: "center",
    alignItems: "center",
    align: "center",
    marginTop: "auto",
    marginBottom: "auto",
  },
  radio: {
    display: "flex",
    flexDirection: "row",
    textAlign: "left",
    marginLeft: "5%",
    width: "30%",
    justifyContent: "space-between",
    font: "normal normal medium 15px/18px Roboto",
    color: " #707070",
  },
}));
