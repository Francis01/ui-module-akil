import { useStylesAjouteUnExpertComptable } from "./AjouteUnExpertComptable.style";
import { Avatar, Button, IconButton, Paper } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import Precedent from "../../layouts/boutton/Precedent";
import TextFields from "../../layouts/input/TextField";
import { UseStylesTextField } from "../../layouts/input/TextField.style";
import Boutton from "../../layouts/boutton/Boutton";
import { Col, Container, Row, Table } from "react-bootstrap";
import villa from "../../../assert/Groupe 11857.png";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";

const SocieteExpertComptable = () => {
  const classes = useStylesAjouteUnExpertComptable();
  return (
    <div className={classes.root}>
      <HeaderDegrade
        precedent={
          <Precedent
            startIcon={<ArrowBackIcon />}
            title="Précédent"
            style={UseStylesBoutton().btnPriv}
          />
        }
        title="Tableau de bord / Ajouter un expert comptable"
      />
      <div className={classes.content}>
        <Row>
          <Col xs={8}>
            <Row>
              <Col xs={6}>
                <TextFields
                  placeholder="Nom"
                  label="IDU*"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
              <Col xs={6}>
                <TextFields
                  placeholder="Saisir votre contact"
                  label="Email*"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <TextFields
                  placeholder="Email"
                  label="Numero de contrribuable*"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
              <Col xs={6}>
                <TextFields
                  placeholder="Email"
                  label="contact 01"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <TextFields
                  placeholder="Matricule"
                  label="Nom*"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
              <Col xs={6}>
                <TextFields
                  placeholder="Saisir votre contact"
                  label="Contact 02"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
            </Row>

            <Row>
              <Col xs={6}>
                <TextFields
                  placeholder="Prénom"
                  label="Prénom"
                  variant="outlined"
                  style={UseStylesTextField().margin}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn} />
              </Col>
              <Col xs={6}>
                <Boutton name="Suivant" style={UseStylesBoutton().orangeBtn} />
              </Col>
            </Row>
          </Col>

          <Col xs={4} className={classes.imgContainer}>
            <img src={villa} width="300px" />
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default SocieteExpertComptable;
