import {
  /* Button, */ withStyles,
  Grid,
  makeStyles,
  Paper,
  /* Table, TableBody, TableCell, TableHead */ /* TableRow */ Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { Line, Bar, Pie } from "react-chartjs-2";
/* import LinearProgress from '@material-ui/core/LinearProgress';
 */ import {
  DataChartBar,
  DataChartLine,
  DataChartPie,
  DataChartPie2,
} from "../../@store/DataChart";
import { useStylesChart } from "./Chart.style";
import { Table, Button } from "react-bootstrap";
import Boutton from "../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../layouts/boutton/Boutton.style";
import LineaProgressBar from "../layouts/progressBar/LineaProgressBar";
import { UseStaticStyles } from "./Static.style";
function Chart(props) {
  const classes = useStylesChart();
  const [assuranceVie, setAssuranceVie] = useState(100);
  const [assuranceNonVie, setAssuranceNonVie] = useState(0);
  const [systemeBancaire, setSystemeBancaire] = useState(20);
  const [ohada, setOhada] = useState(0);
  const [autre, setAutre] = useState(0);
  const [adherents, setAdherents] = useState(100);
  const [nonAdherents, setNonAdherents] = useState(20);

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item xs={12} md={8} sm={12}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <Paper className={UseStaticStyles().paper}>
                <Bar
                  data={DataChartBar}
                  options={{
                    title: {
                      display: true,
                      text: "Volume d'affaire au format SN 200 Contribuable",
                      fontSize: 15,
                    },
                    legend: {
                      display: false,
                    },
                    scales: {
                      yAxes: [
                        {
                          display: false,
                        },
                      ],
                    },
                  }}
                />
              </Paper>
            </Grid>

            <Grid item xs={12} md={6}>
              <Paper className={UseStaticStyles().paper}>
                <Bar
                  data={DataChartBar}
                  options={DataChartBar.DataChartBarOption}
                />
              </Paper>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <Paper className={UseStaticStyles().paper}>
                <Line
                  data={DataChartLine}
                  options={DataChartLine.DataChartLineOption}
                />
              </Paper>
            </Grid>

            <Grid item xs={12} md={6}>
              <Paper className={UseStaticStyles().paper}>
                <Pie
                  data={DataChartPie}
                  options={DataChartPie.DataChartPieOption}
                />
              </Paper>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={4}>
          <Paper className={classes.table}>
            <Typography
              align="left"
              style={{
                font: "normal normal bold 7px/9px Roboto",
                paddingBottom:"2em",
                color: "#636363",
                fontWeight:"bold",
                fontSize:"15px"
              }}
            >
              Campagne
            </Typography>
            <Table className={classes.table2}>
              <thead className={classes.header}>
                <tr>
                  <th>Régime</th>
                  <th>Début</th>
                  <th>Fin</th>
                  <th>Statut</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Impôt synthétique</td>
                  <td>13/05/2019</td>
                  <td>23/02/2020</td>
                  <td>
                    {/* <Boutton className={classes.btn2}>En cours</Boutton> */}
                    <Boutton
                      name="En cours"
                      style={UseStylesBoutton().greenDashBtn}
                    />
                  </td>
                </tr>
                <tr>
                  <td>Réel Normal d’Imposition</td>
                  <td>13/05/2019</td>
                  <td>23/02/2020</td>
                  <td>
                    <Boutton
                      name="En cours"
                      style={UseStylesBoutton().greenDashBtn}
                    />
                  </td>
                </tr>
                <tr>
                  <td>Réel Simplifié d’Imposition</td>
                  <td>13/05/2019</td>
                  <td>23/02/2020</td>
                  <td>
                    <Boutton
                      name="En cours"
                      style={UseStylesBoutton().greenDashBtn}
                    />
                  </td>
                </tr>
              </tbody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        {/* item1 */}
        <Grid item xs={12} md={4}>
          <Paper className={UseStaticStyles().paper}>
            <Pie
              data={DataChartPie2}
              options={DataChartPie2.DataChartPie2Option}
            />
          </Paper>
        </Grid>

        {/* item2 */}
        <Grid item xs={12} md={4} style={{ color: "#757575" }}>
          <Paper className={UseStaticStyles().paper}>
            <span
              style={{
                color: "#636363",
                fontWeight: "bold",
                marginLeft: "-45%",
                fontSize:"14px"
              }}
            >
              Volume d'affaire par système
            </span>
            <br />
            <br />
            <Grid spacing={2} container>
              <Grid item xs={6}>
                <Grid container style={{ color: "#757575" }}>
                  <Grid item align="left" md={10} xs={6} className={classes.textLinearP}>
                    Assurance vie
                  </Grid>
                  <Grid item align="right" md={2} xs={6} className={classes.textLinearP}>
                    {assuranceVie}
                  </Grid>
                </Grid>
                <LineaProgressBar variant="determinate" value={assuranceVie} />

                <br />

                <Grid container style={{ color: "#757575" }}>
                  <Grid item align="left" md={10} xs={6} className={classes.textLinearP}>
                    Système bancaire
                  </Grid>
                  <Grid item align="right" md={2} xs={6} className={classes.textLinearP}>
                    {systemeBancaire}
                  </Grid>
                </Grid>

                <LineaProgressBar
                  variant="determinate"
                  value={systemeBancaire}
                />
                <br />

                <Grid container style={{ color: "#757575" }}>
                  <Grid item align="left" md={10} xs={6} className={classes.textLinearP}>
                    Autres
                  </Grid>
                  <Grid item align="right" md={2} xs={6} className={classes.textLinearP}>
                    {autre}
                  </Grid>
                </Grid>

                <LineaProgressBar variant="determinate" value={autre} />
              </Grid>

              <Grid item xs={6} style={{ color: "#757575" }}>
                <Grid container>
                  <Grid item align="left" md={10} xs={6} className={classes.textLinearP}>
                    Assurance non vie
                  </Grid>
                  <Grid item align="right" md={2} xs={6} className={classes.textLinearP}>
                    {assuranceNonVie}
                  </Grid>
                </Grid>
                <LineaProgressBar
                  variant="determinate"
                  value={assuranceNonVie}
                />
                <br />

                <Grid container>
                  <Grid item align="left" md={10} xs={6} className={classes.textLinearP}>
                    OHADA
                  </Grid>{" "}
                  <Grid item align="right" md={2} xs={6} className={classes.textLinearP}>
                    0
                  </Grid>
                </Grid>

                <LineaProgressBar variant="determinate" value={ohada} />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        {/* item3 */}

        <Grid item xs={12} md={4}>
          <Paper className={UseStaticStyles().paper}>
            <span
              style={{
                color: "#636363",
                marginLeft: "-90%",
                fontWeight: "bold",
                fontSize:"14px"
              }}
            >
              CGA
            </span>
            <br />
            <br />
            <Grid spacing={1} container>
              <Grid item xs={12} style={{ color: "#636363" }}>
                <Grid container>
                  <br />
                  <Grid item xs={6} align="left" className={classes.textLinearP}>
                    Adhérents
                  </Grid>
                  <br />
                  <Grid item xs={6} align="right" className={classes.textLinearP}>
                    {adherents}
                  </Grid>
                </Grid>

                <LineaProgressBar variant="determinate" value={adherents} />

                <br />
                <Grid container>
                  <Grid item xs={6} align="left" className={classes.textLinearP}>
                    Non adhérents
                  </Grid>
                  <br />

                  <Grid item xs={6} align="right" className={classes.textLinearP}>
                    {nonAdherents}
                  </Grid>
                </Grid>
                <LineaProgressBar variant="determinate" value={nonAdherents} />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default Chart;
