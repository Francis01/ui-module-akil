import React from "react";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import ListeExpertComptable from "../../layouts/boutton/ListeExpertComptable";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import ChriffreDaffaireChart from "../../pages/chriffreDaffaire/ChriffreDaffaireChart";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";

const ChriffreDaffaire = () => {
  const title = "Tableau de bord / Chiffre d'affaire de la profession";
  return (
    <div>
      <HeaderDegrade
        title={title}
        listeExpertComptable={
          <Boutton
            name="Liste des experts-comptables"
            style={UseStylesBoutton().comptableBtnOrange}
          />
        }
        precedent={
          <Link
            to="/tableau-de-bord"
            style={{ textDecoration: "none", textTransform: "none" }}
          >
            <Boutton
              name="précédent"
              startIcon={<ArrowBackIcon />}
              style={UseStylesBoutton().btnPriv}
            />
          </Link>
        }
      />
      <ChriffreDaffaireChart />
    </div>
  );
};
export default ChriffreDaffaire;
