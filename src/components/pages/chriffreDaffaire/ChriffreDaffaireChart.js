import { useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import {
  DataChartBar,
  DataChartLine,
  DataChartPie,
  DataChartPie2,
  DataChartBarExpertComptable,
} from "../../../@store/DataChart";
import { Line, Bar, Pie } from "react-chartjs-2";
import { UseStylesChriffreDaffaireChart } from "./ChriffreDaffaireChart.style";
import CircularBar from "../../layouts/progressBar/CircularBar";
import LegendAndTitle from "../../layouts/progressBar/LegendAndTitle";
import Item1 from "./chartComponent/item1";
import { UseCircularBarStyle } from "../../layouts/progressBar/CircularBar.style";
import { UseGridSimpleStyle } from "../../layouts/grid/GridSimple.style";
import { UseLegendAndTitleStyle } from "../../layouts/progressBar/LegendAndTitle.style";
import NavBarChiffreDaffaire from "./Tableau_et_NavBar/NavBarChiffreDaffaire";
import TableauExpert from "./Tableau_et_NavBar/TableauExpert";
import TableauCga from "./Tableau_et_NavBar/TableauCga";
import TableauAutre from "./Tableau_et_NavBar/TableauAutre";
import ChiffreAffaireTab from "./Tableau_et_NavBar/chiffreAffaireTab";

const ChriffreDaffaireChart = (props) => {
  const classes = UseStylesChriffreDaffaireChart();

  const [chart, setChart] = useState(true);
  const [expert, setExpert] = useState(false);
  const [cga, setCga] = useState(false);
  const [autre, setAutre] = useState(false);

  const handleChart = () => {
    setChart(false);
  };
  const handleExpert = (e) => {
    setExpert(true);
    setChart(false);
  };
  const handleCga = (e) => {
    setCga(true);
    setChart(false);
  };
  const handleAutre = (e) => {
    setAutre(true);
    setChart(false);
  };

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <Grid container spacing={4} className={classes.gridContainer}>
          <Grid
            item
            xs={12}
            md={4}
            spacing={4}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Grid item xs={12}>
              <Item1 />
            </Grid>
            <Grid
              item
              xs={12}
              style={{
                justifyContent: "center",
                alignContent: "center",
                marginTop: "2%",
              }}
            >
              <Paper
                elevation={4}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  height: "100%",
                  borderRadius: "18px",
                  textAlign: "center",
                  alignItems: "center",
                }}
              >
                <Grid item xs={12}>
                  <CircularBar
                    title="Chiffre d'affaire de la profession entre 2017-2019 (%)"
                    value1={30}
                    value2={46}
                    value3={68}
                    legend1="2017"
                    legend2="2018"
                    legend3="2019"
                    style={UseCircularBarStyle()}
                  />
                </Grid>
              </Paper>
            </Grid>
          </Grid>

          <Grid item xs={12} md={8}>
            <Paper
              elevation={4}
              style={{
                borderRadius: "18px",
                height: "100%",
                width: "90%",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <LegendAndTitle
                title="Chiffre d'affaire de la profession en 2017-2019"
                legend1="2017"
                legend2="2018"
                legend3="2019"
                style={UseLegendAndTitleStyle()}
              />
              <Bar
                data={DataChartBarExpertComptable}
                options={
                  DataChartBarExpertComptable.DataChartBarExpertComptableOption
                }
              />
            </Paper>
          </Grid>
        </Grid>

        {/* */}

        <Grid container spacing={4}>
          <Grid item md={12} xs={12}>
            <Paper className={UseGridSimpleStyle().paper}>
              <Grid container spacing={2}>
                <Grid item md={6} align="left">
                  Répartition du CA
                </Grid>
                <Grid
                  item
                  md={6}
                  align="right"
                  justify="space-between"
                  container
                  style={{ background: "#FF7A421A" }}
                >
                  <Grid
                    item
                    md={4}
                    alignItems="center"
                    container
                    align="right"
                    onClick={handleExpert}
                  >
                    <div
                      className={UseLegendAndTitleStyle().BluelegendStyle}
                    ></div>
                    <div>Expert comptable (100 000)</div>
                  </Grid>

                  <Grid
                    item
                    md={4}
                    alignItems="center"
                    container
                    align="right"
                    onClick={handleCga}
                  >
                    <div
                      className={UseLegendAndTitleStyle().GreenlegendStyle}
                    ></div>
                    <div>CGA (125 000)</div>
                  </Grid>

                  <Grid
                    item
                    md={4}
                    alignItems="center"
                    container
                    align="right"
                    onClick={handleAutre}
                  >
                    <div
                      className={UseLegendAndTitleStyle().OrangelegendStyle}
                    ></div>
                    <div>Autres (110 000)</div>
                  </Grid>
                </Grid>
              </Grid>

              <div>
                {chart === true ? (
                  <Bar
                    data={DataChartBarExpertComptable}
                    options={
                      DataChartBarExpertComptable.DataChartBarExpertComptableOption
                    }
                  />
                ) : null}
                {chart === false ? (
                  <ChiffreAffaireTab
                    titre1="Expert-comptable (100 000)"
                    titre2="CGA (125 000)"
                    titre3="Autres (110 000)"
                    table={<TableauExpert />}
                    table2={<TableauCga />}
                    table3={<TableauAutre />}
                  />
                ) : null}
              </div>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default ChriffreDaffaireChart;
