import { makeStyles } from "@material-ui/core";

export const UseStylesChriffreDaffaireChart = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(4),
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  gridContainer: {
    textAlign: "center",
    display: "flex",
    justifyContent: "space-between",
  },
  gridHeader: {
    width: "100%",
    height: "20%",
    border: "0.5px solid gray",
    borderRadius: "15px",
    borderRight: "none",
    borderLeft: "none",
    background: "#FF7A42",
    color: "#fff",
  },
  gridContainer: {},
  grid: {
    border: "0.5px solid gray",
    borderRadius: "14px",
    minHeight: "150px",
  },
  div: {
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    height: "auto",
  },
  li: {
    width: "85%",
    fontSize: "15px",
  },

  ul: {
    display: "flex",
    flexDirection: "row",
    listStyle: "none",
    justifyContent: "space-between",
    marginLeft: "auto",
    marginRight: "auto",
  },
  paper2: {
    minHeight: "300px",
  },
  paper3: {
    marginTop: "5%",
    textAlign: "center",
    borderRadius: "18px",
    width: "auto",
  },
}));
