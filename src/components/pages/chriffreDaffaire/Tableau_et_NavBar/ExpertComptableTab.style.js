import {makeStyles} from '@material-ui/core'

export const UseStylesExpertComptableTab = makeStyles((theme)=>({
    root:{
        flexGrow:1,
        outline:'none'
       
    },
    container:{
        marginTop:theme.spacing(4),
        width:'90%',
        marginLeft:'auto',
        marginRight:'auto',
        color:'#757575',
        outline:'none'
    },
    indicator: {
        backgroundColor: '#FF7A42',
        border: '2px solid #FF7A42',
      },
      line:{outline:'none'}

}))