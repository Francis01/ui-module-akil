import { Grid } from "@material-ui/core";
import LegendAndTitle from "../../../layouts/progressBar/LegendAndTitle";
import { UseLegendAndTitleStyle } from "../../../layouts/progressBar/LegendAndTitle.style";

const NavBarChiffreDaffaire = (props) => {
  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={6} align="left">
          Répartition du CA
        </Grid>
        <Grid
          item
          md={6}
          align="right"
          justify="space-between"
          container
          style={{ background: "#FF7A421A" }}
        >
          <Grid item md={4} alignItems="center" container align="right">
            <div className={UseLegendAndTitleStyle().BluelegendStyle}></div>
            <div>Expert-comptable (100 000)</div>
          </Grid>

          <Grid item md={4} alignItems="center" container align="right">
            <div className={UseLegendAndTitleStyle().GreenlegendStyle}></div>
            <div>CGA (125 000)</div>
          </Grid>

          <Grid item md={4} alignItems="center" container align="right">
            <div className={UseLegendAndTitleStyle().OrangelegendStyle}></div>
            <div>Autres (110 000)</div>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default NavBarChiffreDaffaire;
