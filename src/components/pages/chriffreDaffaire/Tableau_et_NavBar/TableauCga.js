import { Table } from "react-bootstrap";
import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
const TableauCga = (props) => {
  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>IDU</th>
            <th>Matricule</th>
            <th>N.Agrement</th>
            <th>Raison sociale CGA</th>
            <th>Expert-comptable CGA</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>Statut</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>1559082Z Ab 08</td>
            <td>Akilcab</td>
            <td>Akilcab</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>1559082Z Ab 08</td>
            <td>Akilcab</td>
            <td>Akilcab</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>1559082Z Ab 08</td>
            <td>Akilcab</td>
            <td>Akilcab</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>1559082Z Ab 08</td>
            <td>Akilcab</td>
            <td>Akilcab</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>1559082Z Ab 08</td>
            <td>Akilcab</td>
            <td>Akilcab</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
        </tbody>
      </Table>
    </>
  );
};

export default TableauCga;
