import { Table } from "react-bootstrap";
import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
const TableauExpert = (props) => {
  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>IDU</th>
            <th>Numéro contribuable</th>
            <th>Noms</th>
            <th>prénoms</th>
            <th>Email</th>
            <th>Contacts</th>
            <th>Statut</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Konan</td>
            <td>manuella</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Konan</td>
            <td>manuella</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Konan</td>
            <td>manuella</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Konan</td>
            <td>manuella</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
          <tr>
            <td>25968954</td>
            <td>1559082Z</td>
            <td>Konan</td>
            <td>manuella</td>
            <td>info@gmail.com</td>
            <td>+225 00 000 000</td>
            <td>
              <Boutton name="Actif" style={UseStylesBoutton().greenBtn} />
            </td>
          </tr>
        </tbody>
      </Table>
    </>
  );
};

export default TableauExpert;
