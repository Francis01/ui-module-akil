import { Grid, Paper } from '@material-ui/core'
import {UseStylesItems1} from './item1.style'
const Items1 =() =>{
    const classes = UseStylesItems1()
    return(
        <div className={classes.root} >
            <Grid spacing={2}>
                <Paper className={classes.paper}>
                    <div className={classes.gridHeader}>
                        
                        <p>Chiffre d'affaire de la profession</p>
                        <p style={{fontSize:'10px'}}>100 000 000 000 (En fCFA)</p>
                    
                                        
                    </div>

                    <div className={classes.gridContainer}>
                            <Grid container className={classes.ul}>
                            <Grid item md={9} className={classes.li}> Nombre de contribuables de la profession comptable</Grid>
                            <Grid item md={3} style={{fontSize:'15px',color:'#267466'}} >32 000</Grid>
                        </Grid>
                    </div>
                </Paper>

            </Grid>

        </div>
    )
}
export default Items1;