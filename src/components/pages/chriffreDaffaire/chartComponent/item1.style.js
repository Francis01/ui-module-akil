import { makeStyles } from '@material-ui/core';


export const UseStylesItems1 = makeStyles((theme)=>({
    root:{
        flexGrow:1
    },
    paper: {
        width:'100%',
        textAlign: 'center',
        borderRadius: "15px",
        /* height: "170px" */
        minHeight: "170px",
        
    },
    gridHeader:{
        flexGrow:1,
        borderRadius:'15px',
        background:'#FF7A42',
        color:'#fff',
        padding:'15px 0',
        

    },
    li:{
        width:'85%',
        fontSize:'15px',
        
    },
    
    ul:{
        display:'flex',
        flexDirection:'row',
        listStyle:'none',
    },
    gridContainer:{
        textAlign:'center',
    },
}))