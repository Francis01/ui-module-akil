import React from "react";
import Static from "../pages/Static";
import Chart from "../pages/chart";
import NavBar from "../layouts/NavBar";
import { Grid, Typography } from "@material-ui/core";
function Dashboard() {
  return (
    <div>
      <NavBar />
        <Grid
          item
          xs={12}
          sm={12}
          alignItems="center"
          justify="center"
          style={{
            backgroundColor: "#267466",
            minHeight: "150px",
            color: "#fff",
          }}
        >
          <Typography
            align="left"
            style={{
              marginLeft: "65px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              variant="div"
              component="div"
              style={{ marginTop: "20px" }}
            >
              {" "}
              Tableau de bord
            </Typography>
          </Typography>
        </Grid>
      <div
        style={{ marginLeft: "auto", marginRight: "auto", marginTop: "-7%" }}
      >
        <Static />
        <Chart />
      </div>
    </div>
  );
}

export default Dashboard;
