import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Grid, Paper } from "@material-ui/core";
import { UseStaticStyles } from "../Static.style";
import TableImportation from './TableImportation'

const ListeDesImportationsDashboard = () => {
  return (
    <div>
      <HeaderDegrade2
        title1="Tableau de bord / liste des importations"
        title2="Détail de l'import"
        boutton1={
          <Boutton
            name="précedent"
            style={UseStylesBoutton().btnPriv}
            startIcon={<ArrowBackIcon />}
          />
        }
        boutton2={
          <Boutton
            name="Annuler l import"
            style={UseStylesBoutton().orangeBtn}
          />
        }
        boutton3={
          <Boutton
            name="Valider l'import"
            style={UseStylesBoutton().orangeBtn}
          />
        }
        icons1={<LocalPrintshopIcon />}
        icons2={<PictureAsPdfIcon />}
      />

      <Grid
        item
        contaner
        style={{
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "-2%",
        }}
      >
        <Paper className={UseStaticStyles().paper}>
          <TableImportation/>
        </Paper>
      </Grid>
    </div>
  );
};

export default ListeDesImportationsDashboard;
