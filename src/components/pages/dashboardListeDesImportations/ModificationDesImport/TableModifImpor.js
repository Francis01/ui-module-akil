import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import Boutton from "../../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../../layouts/boutton/Boutton.style";
import { UseStyleTableImportations } from "../TableImportation.style";
import SaveIcon from "@material-ui/icons/Save";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import Groupe from "../../../../assert/Groupe 12251.png";
import SmsIcon from "@material-ui/icons/Sms";
import { Link } from "react-router-dom";
const TableModifImpor = () => {
  const classes = UseStyleTableImportations();

  return (
    <div>
      <Table striped className={classes.body}>
        <thead>
          <tr className={classes.row}>
            <th>Raison sociale</th>
            <th>Nrc</th>
            <th>Ncc</th>
            <th>Régime</th>
            <th>Service</th>
            <th>Matricule</th>
            <th>Téléphone</th>
            <th>Mission</th>
            <th>Email</th>
            <th>Statut</th>
            <th>Actions</th>
            <th>Commentaire</th>
          </tr>
        </thead>
        <tbody style={{overflow:'scroll'}}>
          <tr className={classes.row}>
            <th>
              <Boutton name="AS |" style={UseStylesBoutton().BtnBorderGray} />
            </th>
            <th>
              <Boutton
                name="1234567A"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton
                name="CI-ABJ-2020-A-2020"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton
                name="Réel normal"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton name="200000" style={UseStylesBoutton().BtnBorderGray} />
            </th>
            <th>
              <Boutton
                name="45671345"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton
                name="Expertise"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton
                name="return@gmail.com"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton
                name="return@gmail.com"
                style={UseStylesBoutton().BtnBorderGray}
              />
            </th>
            <th>
              <Boutton name="Erronée" style={UseStylesBoutton().redBtn} />
            </th>
            <th
              style={{
                alignItems: "center",
                justifyContent: "center",
                textAlign: "center",
              }}
            >
              <Link
                to="/ListeDesImportationsDashboard"
                style={{
                  color: "inherit",
                  textDecoration: "none",
                  textTransform: "none",
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <SaveIcon /> <HighlightOffIcon />
                </div>
              </Link>
            </th>

            <th>
            
                <SmsIcon />
              
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableModifImpor;
