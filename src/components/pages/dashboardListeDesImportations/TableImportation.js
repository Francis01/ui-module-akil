import { Checkbox } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { UseStyleTableImportations } from "./TableImportation.style";
import { useState } from "react";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import MenuIco from "./MenuIco";
import MsIcon from "./msIcon";
const TableImportation = () => {
  const classes = UseStyleTableImportations();
  const [checked, setChecked] = useState(true);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  return (
    <div>
      <Table striped className={classes.body}>
        <thead>
          <tr className={classes.row}>
            <th>
              <Checkbox />
            </th>
            <th>IDU</th>
            <th>Nrc</th>
            <th>Ncc</th>
            <th>Matricule</th>
            <th>Téléphone</th>
            <th>Mission</th>
            <th>Email</th>
            <th>Raison sociale</th>
            <th>Regime</th>
            <th>Service</th>
            <th>Statut</th>
            <th>Actions</th>
            <th>Commentaire</th>
          </tr>
        </thead>
        <tbody>
          <tr className={classes.row}>
            <th>
              <Checkbox />
            </th>
            <th>123</th>
            <th>1234567A</th>
            <th>CI-ABJ-2020-A-2020</th>
            <th>202589</th>
            <th>45896523</th>
            <th>Expertise</th>
            <th>return@gmail.com</th>
            <th>Expertise</th>
            <th>Réel normal</th>
            <th>Yopougon</th>
            <th>
              <Boutton name="Erronée" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIco />
            </th>
            <th>
              <MsIcon />
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox />
            </th>
            <th>112</th>
            <th>1234567A</th>
            <th>CI-ABJ-2020-A-2020</th>
            <th>202589</th>
            <th>45896523</th>
            <th>Expertise</th>
            <th>return@gmail.com</th>
            <th>Expertise</th>
            <th>Réel normal</th>
            <th>Yopougon</th>
            <th>
              <Boutton name="Erronée" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIco />
            </th>
            <th>
              <MsIcon />
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox />
            </th>
            <th>113</th>
            <th>1234567A</th>
            <th>CI-ABJ-2020-A-2020</th>
            <th>202589</th>
            <th>45896523</th>
            <th>Expertise</th>
            <th>return@gmail.com</th>
            <th>Expertise</th>
            <th>Réel normal</th>
            <th>Yopougon</th>
            <th>
              <Boutton name="Erronée" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIco />
            </th>
            <th>
              <MsIcon />
            </th>
          </tr>

          <tr className={classes.row}>
            <th>
              <Checkbox />
            </th>
            <th>145</th>
            <th>1234567A</th>
            <th>CI-ABJ-2020-A-2020</th>
            <th>202589</th>
            <th>45896523</th>
            <th>Expertise</th>
            <th>return@gmail.com</th>
            <th>Expertise</th>
            <th>Réel normal</th>
            <th>Yopougon</th>
            <th>
              <Boutton name="Erronée" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIco />
            </th>
            <th>
              <MsIcon />
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableImportation;
