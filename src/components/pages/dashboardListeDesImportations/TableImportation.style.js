import {makeStyles} from "@material-ui/core";

export const UseStyleTableImportations=makeStyles((theme)=>({
    body:{
        color:"#757575",
        font: "normal normal medium 14px/19px Roboto Slab",
        fontSize:"10px"
    },
    row:{
        alignItems:'center',
        justifyContent:'center',
        textAlign:'center'
    }
}))