import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { IconButton, ListItemIcon, Typography } from "@material-ui/core";
import Groupe from "../../../assert/Groupe 12251.png";
const MsIcon = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <img src={Groupe} alt="icon btn" width="40" />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <div style={{ width: "180px", overflow: "scroll" }}>
          
              Lorem ipsum dolor sit amet,<br/> consetetur sadipscing elitr,<br/> sed diam
              nonumy eirmod tempor invidunt <br/>ut labore et dolore magna aliquyam
              erat,<br/> sed diam voluptua.<br/> At vero eos et accusam et<br/> justo duo
              dolores et ea rebum.<br/> Stet clita
            
          </div>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default MsIcon;
