import React from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import HeaderDegrade from "../../layouts/headers/headerDegrade";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import ExpertComptableTab from "../../layouts/headers/tabs/ExpertComptableTab";
import Identification from "./Indentification";
import Role from "./Role";
import PhotoProfile from "./PhotoProfile";
import { Grid } from "@material-ui/core";
const GestionDesAcces = (props) => {
  return (
    <div>
      <HeaderDegrade
        title="Tableau de bord / Gestion des accès"
        precedent={
          <Link
            to="/gestion-des-acces-table"
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <Boutton
              startIcon={<ArrowBackIcon />}
              name="Précédent"
              style={UseStylesBoutton().btnPriv}
            />
          </Link>
        }
      />
      <div
        style={{
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
          color: "#757575",
          fontSize: "15px",
          fontWeight: "bold",
          marginTop: "1%",
        }}
      >
        Ajouter un utilisateur
      </div>

      <ExpertComptableTab
        titre1="Identification"
        titre2="Rôle"
        titre3=" Photo de profile"
        table={<Identification />}
        table2={<Role />}
        table3={<PhotoProfile />}
      />
      <Grid
        container
        spacing={4}
        style={{
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
          color: "#757575",
          fontSize: "25px",
          fontWeight: "bold",
          marginTop: "auto",
        }}
      >
        <Grid item xs={6}>
          <Boutton name="Annuler" style={UseStylesBoutton().whiteBtn2} />
        </Grid>
        <Grid item xs={6}>
          <Boutton name="Suivant" style={UseStylesBoutton().orangeBtn2} />
        </Grid>
      </Grid>
    </div>
  );
};

export default GestionDesAcces;
