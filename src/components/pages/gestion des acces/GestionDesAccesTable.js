import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import PdfIcon from "../../../assert/Groupe 858.png";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import PrintIcon from "../../../assert/Groupe 864.png";
import TableauGestionAcces from "./table";
import { BrowserRouter, Route, Link } from "react-router-dom";
import NavBar from "../../layouts/NavBar";
const GestionDesAccesTable = (props) => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / Gestion des accès"
        title2="Gestion des accès"
        icons1={<img src={PrintIcon} />}
        icons2={<img src={PdfIcon} />}
        boutton3={
          <Link
            to="gestion-des-acces"
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <Boutton
              name="Ajouter un utilisateur"
              style={UseStylesBoutton().comptableBtnOrange}
            />
          </Link>
        }
      />
      <div
        style={{
          width: "90%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "-2%",
          background: "#fff",
        }}
      >
        <TableauGestionAcces />
      </div>
    </div>
  );
};
export default GestionDesAccesTable;
