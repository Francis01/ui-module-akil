import { Grid } from '@material-ui/core';
import React from 'react';
import Boutton from '../../layouts/boutton/Boutton';
import { UseStylesBoutton } from '../../layouts/boutton/Boutton.style';
import TextFields from '../../layouts/input/TextField';
import {UseStylesTextField} from '../../layouts/input/TextField.style'
const Identification =(props)=>{
    return(
        <div>
            <form className={UseStylesTextField().form}>
                
                <Grid container spacing={4}>
                    <Grid item xs={6}>
                    <TextFields 
                        placeholder='Nom' 
                        label='Nom *' 
                        variant='outlined'
                        style={UseStylesTextField().profile}
                        />
                    </Grid>
                    <Grid item xs={6}>
                    <TextFields 
                        placeholder='Prénoms' 
                        label='Pénoms *' 
                        variant='outlined'
                        style={UseStylesTextField().profile}
                        />
                    </Grid>

                    <Grid item xs={6}>
                    <TextFields 
                        placeholder='Email' 
                        label='Email *' 
                        variant='outlined'
                        style={UseStylesTextField().profile}
                        />
                    </Grid>

                    <Grid item xs={6}>
                    <TextFields 
                        placeholder='Saisir votre contact' 
                        label='Contact 02' 
                        variant='outlined'
                        style={UseStylesTextField().profile}
                        />
                    </Grid>

                    <Grid item xs={6}>
                    <TextFields 
                        placeholder='Saisir votre contact' 
                        label='Contact 01' 
                        variant='outlined'
                        style={UseStylesTextField().profile}
                        />
                    </Grid>
                </Grid>
                
            </form>

        </div>
    )
}

export default Identification;