import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Grid, IconButton, ListItemIcon, Typography } from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import BlockIcon from '@material-ui/icons/Block';
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";
import Popup from "../../layouts/Dialogs/simpleDialog";
import Groupe from "../../../assert/Groupe 12252.png";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import DialogContent from "../../layouts/Dialogs/DialogContent";


const MenuIconAccès = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [openPopup, setOpenPopup] = useState(false);

  return (
    <div>
      <IconButton
        style={{ outline: "none" }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        style={{ outline: "none" }}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link
          to="/profil"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<VisibilityIcon />}</ListItemIcon>
            <Typography>Voir</Typography>
          </MenuItem>
        </Link>
        <Link
          to="#"
          style={{
            textDecoration: "none",
            textTransform: "none",
            color: "#757575",
          }}
          onClick={() => setOpenPopup(true)}
        >
          <MenuItem onClick={handleClose}>
            <ListItemIcon>{<BlockIcon style={{color:"red"}} />}</ListItemIcon>
            <Typography>Désactiver</Typography>
          </MenuItem>
        </Link>

       
        <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}>
          <DialogContent
            imageText="Désactivation d'un utilisateur"
            textInterrogative="Voulez-vous désactiver un accès ?"
            button1={
              <Boutton
                name="continuer"
                style={UseStylesBoutton().comptableBtnOrange}
                click={() => setOpenPopup(false)}
              />
            }

            button2={
              <Boutton
                name="annuler"
                style={UseStylesBoutton().comptableBtnWhite}
                click={() => setOpenPopup(false)}
              />
            }
            image={Groupe}
          />
        </Popup>
      </Menu>
    </div>
  );
};

export default MenuIconAccès;
