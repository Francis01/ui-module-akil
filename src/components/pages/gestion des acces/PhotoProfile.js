import { Button } from "@material-ui/core";

const PhotoProfile = (props)=>{
    return(
        <div>

            <div style={{width:'90%',
        marginTop:'3%'}}>
            <input
                accept="image/*"
                style={{ display: 'none' }}
                id="raised-button-file"
                multiple
                type="file"
                />
            <label htmlFor="raised-button-file" style={{border:'1px solid #707070', 
                width:'50vw',
                height:'20vw',
                borderRadius:'18px', 
                background:'#FCFCFC',
                border:' 0.5px solid #707070',
                justifyContent:'center',
                alignItems:'center',
                textAlign:'center'

            }}>
            <Button variant="raised" component="span" 
                style={{justifyContent:'center',
                alignItems:'center',
                textAlign:'center',
                marginTop:'15%',
                marginBottom:'10%',
                fontWeight:'bold',
                color:'#A8A8A8',
                fontSize:'18px'
                }}>
                Charger l'image ici
            </Button>


            </label> 
            </div>
            

        </div>
    )
}

export default PhotoProfile;