import { Grid } from "@material-ui/core"
import { UseStylesTextField } from "../../layouts/input/TextField.style";
import SelectInput from '../../layouts/input/SelectInput'

const Role=(props)=>{
    const SelectData=[
    
        {
          id:10,
          text:"Membre du conseil"
        },
        {
          id:20,
          text:"Administration DGI"
        },
        {
          id:30,
          text:"Administration Expert comptable"
        },
        {
          id:40,
          text:"Administration comtribuable"
        },
        {
            id:50,
            text:"Trésorier"
          },
          {
            id:60,
            text:"Assistant"
          },
        
      ]
    return(
        <div>
            <div className={UseStylesTextField().form}>
                <Grid container spacing={4}>
                <Grid item xs={12}>
                    <div style={{width:'60%',color:'#818181',fontSize:'10px'}}>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore 
                    et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <SelectInput data={SelectData}/>
                </Grid>
            </Grid>
            </div>
            

        </div>
    )
}
export default Role;