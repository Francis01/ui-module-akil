import {makeStyles} from '@material-ui/core';

export const UseStylesTable=makeStyles((theme)=>({
    theader:{
        color:'#757575',
        fontSize:'15px',
        fontStyle:'Roboto Slab',
        textAlign:'center',
        fontSize:'10px',
    },
    tbody:{
        color:'#757575',
        fontSize:'15px',
        fontStyle:'Roboto Slab',
        textAlign:'center',
        fontSize:'12px',
        alignItems:'center',
        justifyContent:'center'
    },
}))