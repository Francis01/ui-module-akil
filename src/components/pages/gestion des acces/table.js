import React from "react";
import { Table } from "react-bootstrap";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import { UseStylesTable } from "./Table.style";
import Assetou from "../../../assert/assetou.png";
import MenuIconAccès from "./MeniIconAccès";
const TableauGestionAcces = (props) => {
  const classes = UseStylesTable();

  return (
    <div>
      <Table striped bordered>
        <thead className={classes.theader}>
          <tr>
            <th>{<input type="checkbox" />} </th>
            <th></th>
            <th>Nom</th>
            <th>Prénoms</th>
            <th>Email</th>
            <th>Contact</th>
            <th>Rôle</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody className={classes.tbody}>
          <tr>
            <td>{<input type="checkbox" />} </td>
            <td>{<img src={Assetou} width="40" height="40" />}</td>
            <td>Coulibaly</td>
            <td>Assetou</td>
            <td>CASSETOU@MAIL.com</td>
            <td>+225 00 000 000</td>
            <td>QA</td>
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().greenBtn} name="Actif" />{" "}
            </td>
            <td>
             <MenuIconAccès/>
            </td>
          </tr>
          <tr>
            <td>{<input type="checkbox" />} </td>
            <td>{<img src={Assetou} width="40" height="40" />}</td>
            <td>Coulibaly</td>
            <td>Assetou</td>
            <td>CASSETOU@MAIL.com</td>
            <td>+225 00 000 000</td>
            <td>QA</td>
            <td>
              {" "}
              <Boutton style={UseStylesBoutton().redBtn} name="Bloqué" />{" "}
            </td>
            <td>
            <MenuIconAccès/>
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};
export default TableauGestionAcces;
