import Boutton from "../../layouts/boutton/Boutton";
import HeaderDegrade2 from "../../layouts/headers/HeaderDegrade2";
import { Link } from "react-router-dom";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import { Grid, Paper } from "@material-ui/core";
import { UseStaticStyles } from "../Static.style";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import TableGestionDesArbitrages from "./TableGestionDesArbitrages";
import NavBar from "../../layouts/NavBar";

const GestionDesArbitrages = () => {
  return (
    <div>
      <NavBar/>
      <HeaderDegrade2
        title1="Tableau de bord / gestion des arbitrages"
        title2="Gestion des arbitrages"
        icons1={<LocalPrintshopIcon fontSize="large" />}
        icons2={<PictureAsPdfIcon fontSize="large" />}
      />

     
      <Paper
        className={UseStaticStyles().paper}
        style={{
          width: "95%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop:"-2%"
        }}
      >
        <TableGestionDesArbitrages />
      </Paper>
    </div>
  );
};
export default GestionDesArbitrages;

