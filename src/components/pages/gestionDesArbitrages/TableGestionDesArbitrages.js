import { Checkbox, IconButton } from "@material-ui/core";
import { Table } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";
import MenuIconGestionDePaiement from "./MenuIconGestionDesArbitrages";
const TableGestionDesArbitrages = () => {
  const [enCours,setEnCours]=useState(false)
  const [isOne,setIsOne]=useState(false)
  return (
    <div>
      <Table
        striped
        style={{
          fontSize: "10px",
          color: "#757575",
          font: " normal normal medium 14px/19px Roboto Slab",
        }}
      >
        <thead>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>Forme juridique</th>
            <th>IDU</th>
            <th>Compte contribuable</th>
            <th>Régistre de commerce</th>
            <th>Raison social</th>
            <th>Mission</th>
            <th>Date du conflit</th>
            <th>Statut</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody style={{ overflow: "scroll" }}>
          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SARL</th>
            <th>55555555555555A</th>
            <th>AAAAAA33332222</th>
            <th>REG123456789</th>
            <th>DiaudesCORP</th>
            <th>EXPERTISE</th>
            <th>20/02/2020</th>
            <th>
              <Boutton name="En arbitrage" style={UseStylesBoutton().marronBtn} />
            </th>
            <th>
              <MenuIconGestionDePaiement/>
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SARL</th>
            <th>55555555555555A</th>
            <th>AAAAAA33332222</th>
            <th>REG123456789</th>
            <th>DiaudesCORP</th>
            <th>EXPERTISE</th>
            <th>20/02/2020</th>
            <th>
              <Boutton name="En conflit" style={UseStylesBoutton().redBtn} />
            </th>
            <th>
              <MenuIconGestionDePaiement />
            </th>
          </tr>

          <tr>
            <th>
              <Checkbox color="primary" />
            </th>
            <th>SARL</th>
            <th>55555555555555A</th>
            <th>AAAAAA33332222</th>
            <th>REG123456789</th>
            <th>DiaudesCORP</th>
            <th>EXPERTISE</th>
            <th>20/02/2020</th>
            <th>
              <Boutton name="Conflit résolu" style={UseStylesBoutton().greenBtn} />
            </th>
            <th>
            </th>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default TableGestionDesArbitrages;
