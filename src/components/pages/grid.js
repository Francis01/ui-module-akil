import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Img from '../../assert/download'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { AppBar, Avatar, Badge, IconButton,  Toolbar, Typography } from '@material-ui/core';
import { NotificationsNone } from '@material-ui/icons';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        

    },
    indicator: {
        top: "0px",
        backgroundColor: "#FF7A42",
        maxWidth:"50px",
        marginLeft:theme.spacing(7)
    },
    title: {
        flexGrow:1,
        padding:theme.spacing(0),
        textTransform:"inherit",
        color:"black"
        /* 
        minWidth:"133px" */
    },
    childTypo: {
        flexGrow: 1,

        display: "flex",
        margin: 'auto',
        position: 'relative',
        alignItems: "center",
        justifyContent: "center",
        marginLeft: theme.spacing(-20),
        [theme.breakpoints.down('sm')]: {
            flexGrow: 1,
            marginLeft: theme.spacing(-50),
        }
    },
    childTypo1: {
        flexGrow: 1,

        display: "flex",
        margin: 'auto',
        position: 'relative',
        alignItems: "center",
        justifyContent: "center",
        marginLeft: theme.spacing(-20),
        [theme.breakpoints.down('sm')]: {
            flexGrow: 1,
            marginLeft: theme.spacing(-50),
        }
    },
    deroulant: {
        borderRadius: "15px",
        borderColor: "#ff3d00",
        backgroundColor: "white",
        width: '50%',
        fontSize:"10px",
        

    },
}));


function Grids() {
    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    const handleChangeTabs = (event, value) => {
        console.log(event);
        setValue(value);
    };

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" color="primary">
                        
                            <Tabs onChange={handleChangeTabs}
                                value={value}
                                classes={{
                                    indicator: classes.indicator
                                }}
                                centered
                                scrollButtons="desktop"

                            >
                                <Tab  label="Accueil" />
                                <Tab className={classes.title} label="Contribuable" />
                                <Tab className={classes.title} label={
                                    <>
                                    <IconButton aria-label="more"
                                    aria-controls="long-menu"
                                    aria-haspopup="true">
                                        <MoreVertIcon  onClick={handleClick} />
                                    </IconButton >
                                    <Menu
                                    id="simple-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={Boolean(anchorEl)}
                                    onClose={handleClose}
                                  >
                                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                                    <MenuItem onClick={handleClose}>My account</MenuItem>
                                    <MenuItem onClick={handleClose}>Logout</MenuItem>
                                  </Menu>
                                  </>
                                    /* onClick={handleClick} */
                                } />


                                
                                <Tab className={classes.title} label="Experts comptables" />
                                <Tab className={classes.title} label="Acces" />
                                <Tab className={classes.title} label="Paiement" />
                                <Tab className={classes.title} label="Offres" />
                                <Tab className={classes.title} label="Acces" />

                                <Tab className={classes.title} label={
                                    <select size="0" className={classes.deroulant} >
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                </select>
                                } />
                                <Tab className={classes.title} label={
                                    <IconButton aria-label="show 4 new notifications" color="inherit" className={classes.title} >
                                <Badge badgeContent={4} color="secondary">
                                    <NotificationsNone />
                                </Badge>

                            </IconButton>
                                }/>
                        
                            

                        <Tab className={classes.title} label={
                                    <Avatar alt="image" src={Img} />
                        
                                }/>

                        <Tab className={classes.title}  label={
                                   `Guei Dessekane problem solver`
                                }/> 
                            
                            <Tab className={classes.title} label={
                                    <IconButton aria-label="show 4 new notifications" color="inherit" className={classes.title} >
                                    <PowerSettingsNewIcon style={{ color: "#ff3d00" }} />
                            </IconButton>
                                }/>
                      
                            
                        
                        </Tabs>
            </AppBar>
        </div>
    )
}

export default Grids;



