import React from "react";
import NavBar from "./navBar";
import SeConnecter from "../../layouts/boutton/SeConnecter";
import FirstBlock from "./firstBlock";
import SideBar from "./sideBar";
import Footer from "./footer";
import Section from "./section";
import Logo from "../../../assert/NoPath - Copie (3).png"
import Boutton from "../../layouts/boutton/Boutton";
import { UseStylesBoutton } from "../../layouts/boutton/Boutton.style";

const LandingPage = () => {
  return (
    <div>
      <NavBar
        logo={Logo}
        Accueil="Accueil"
        Innovations="Innovation"
        Nous_contacter="Nous contacter"
        bntSeConnecter={<Boutton name='Se Connecter' style={UseStylesBoutton().SeConnecter} />}
      />
      <FirstBlock />
      <SideBar />
      <Section />
      <Footer />
    </div>
  );
};
export default LandingPage;
