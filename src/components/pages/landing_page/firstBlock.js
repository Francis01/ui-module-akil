import React from "react";
import image from "../../../assert/Groupe de masques 6.png";
import { UseStylesFirstBlock } from "./firstBlock.style";

const FirstBlock = () => {
  const classes = UseStylesFirstBlock();
  return (
    <div className={classes.root} >
      <div className={classes.blockText}>
        <div className={classes.content}>
          <p className={classes.p1}>Ensemble construisons l'innovation</p>
          <p className={classes.p2}>
            au cœur du métier de
            <span style={{ fontWeight: "bold" }}> l'expert-comptable</span>
          </p>
        </div>
      </div>
      <div className={classes.blockimage}>
        <div>
          <img src={image} alt="image" className={classes.imgs} />
        </div>
      </div>
    </div>
  );
};

export default FirstBlock;
