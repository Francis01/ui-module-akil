import { makeStyles } from "@material-ui/core/styles";
import { Translate } from "@material-ui/icons";

export const UseStylesFirstBlock = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "row",
    height: "70vh",
    width: "100%",
    
  },
  blockText: {
    background: "#2F8B7B 0% 0% no-repeat padding-box",
    height: "auto",
    width: "50%",
    display: "flex",
    flexDirection: "row",
    textAlign: "center",
    justifyContent: "left",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    justifyItems: "center",
    font:" normal normal medium 75px/71px Roboto Slab",
},

  p1: {
    color: " #FFFFFF",
    font: "normal normal medium 75px/71px Roboto Slab",
     fontSize: "40px",
      width: "auto",
    fontWeight: "bold",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    alignContent:'center'
  },

  p2: {
    color: " #FFFFFF",
    font: " normal normal 300 34px/45px Roboto Slab",
    width: "auto",
    fontSize:"20px",
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems:"center",
    alignContent:'center'
  },

  blockimage: {
    width: "50%",
    display: "flex",
    flexDirection: "row",
  },
  imgs: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },

  div: {
    color: " #FFFFFF",
    font: "normal normal medium 75px/71px Roboto Slab",
    fontSize: "15px",
  },
}));
