import {UseStylesFooter} from './footer.style'
import facebook from '../../../assert/facebook.png';
import instagram from '../../../assert/instagram.png';
import tweter from '../../../assert/tweter.png';
import Oecci from "../../../assert/NoPath - Copie (3).png"
import { Grid } from '@material-ui/core';
const Footer = ()=>{
    const classes=UseStylesFooter();
    return(
        <div className={classes.root}>

            <div className={classes.footer1}>
                <div className={classes.container}>
                <div>
                
                
                    <ul className={classes.disp1}>
                        <li className={classes.title}>
                            <img src={Oecci} width="70"/>
                        </li>
                        <li className={classes.contente}>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr
                        </li>
                        <li className={classes.contente}>
                        sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                        </li>
                        <li className={classes.contente}>
                        aliquyam erat, sed diam voluptua. At vero eos
                        </li>

                    </ul>
                    </div>
                    <div>

                
<ul className={classes.disp1}>
    <li className={classes.title}>
        A propos de nous
    </li>
    <li className={classes.contente}>
    A propos de nous 
    </li>
    <li className={classes.contente}>
    Nos services
    </li>
    <li className={classes.contente}>
    Nos formations
    </li>

</ul>
</div>
                    <div>
                        <ul className={classes.disp1}>
                            <li className={classes.title}>Informations légales</li>
                            <li className={classes.contente}>Conditions générales d'utilisation </li>
                            <li className={classes.contente}>Notes privées</li>
                        </ul>
                    </div>
                    <div>
                        <ul className={classes.disp3}>
                            <li className={classes.title}>Nos reseaux</li>
                            
                              
                              <ul className={classes.icons}>
                                    <li className={classes.iconsCard}>
                                     <div className={classes.div}>
                                         <img src={tweter} height='25' />
                                     </div>
                                     </li>
                                    <li className={classes.iconsCard}>
                                    <div className={classes.div}>
                                    <img src={tweter} height='25' />
                                    </div>
                                    </li>
                                    <li className={classes.iconsCard}>
                                    <div className={classes.div}>
                                    <img src={tweter} height='25' />
                                    </div>
                                    </li>
                                    <li className={classes.iconsCard}>
                                    <div className={classes.div}>
                                    <img src={tweter} height='25' />
                                    </div>
                                    </li>
                                </ul>
                                
                            
                            
                        </ul>
                        
                    </div>

                </div>
                
            </div>
            <div className={classes.footer2}>
                <Grid container >
                    <Grid item md={12} xs={12} align="right" style={{margin:"2em 2em 0 0",color:"#fff",fontSize:"12px"}}>
                        Copyright © 2021 OECCI Tous droits réservés.
                    </Grid>
                </Grid>
                
            </div>

        </div>
    )
}
export default Footer;