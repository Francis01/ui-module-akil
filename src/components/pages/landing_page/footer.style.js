import { makeStyles } from '@material-ui/core/styles';

export const UseStylesFooter = makeStyles((theme)=>({
    root:{
        flexGrow:1,
        marginTop:theme.spacing(4)
    },
    footer1:{
        width:'100%',
        height: '254px',
        flexGrow: 1,
        display:'flex',
        flexDirection:'row-reverse',
        background:' #2F8B7B 0% 0% no-repeat padding-box',
        textAlign:'center',
        justifyItems:'center',
        alignItems:'center',
        alignContent:'center'
        
       
    },
    container:{
        display:'flex',
        justifyContent:'space-between',
        flexGrow: 0.1,
        marginRight:theme.spacing(8)
    },
   
    footer2:{
        width:'100%',
        height: '69px',
        background:' #4CAF50 0% 0% no-repeat padding-box',
        opacity: 1,
    },
    disp1:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between',
        textDecoration:'none',
        listStyle:'none',
        height:'88px',
        color:'#fff',
        textAlign:'justify'
    },
    disp2:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between',
        textDecoration:'none',
        listStyle:'none',
        height:'88px',
        color:'#fff',
        textAlign:'justify'
    },
    disp3:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between',
        textDecoration:'none',
        listStyle:'none',
        height:'88px',
        color:'#fff',
        textAlign:'justify'
    },
    title:{
        font: 'normal normal 600 14px/18px Montserrat',
        textTransform: 'uppercase',
        opacity: 1
    },
    contente:{
        font:' normal normal 300 14px/34px Poppins',
        opacity: 1,
        fontSize:'15px'
    },
    contente2:{
        font:' normal normal 300 14px/34px Poppins',
        opacity: 1,
        fontSize:'15px',
        marginRight:'20%',
        
    },
    icons:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        listStyle:'none',
        textAlign:'justify',
        

    },
    iconsCard:{
        width:'39px',
        height:'39px',
        background: '#4CAF50 0% 0% no-repeat padding-box',
        marginLeft:theme.spacing(1),
        textAlign:'center'
       

    },
    div:{
        justifySelf:'center',
        height:'50%',
        justifyContent:'center',
        marginTop:'25%',
        marginBottom:'25%'
    }
}))