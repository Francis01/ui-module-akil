import React from "react";
import { UseStyleNavBar } from "./navBar.style";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const NavBar = (props) => {
  const classes = UseStyleNavBar();
  const preventDefault = (event) => event.preventDefault();
  return (
    <div className={classes.root}>
      <AppBar position="static" color="inherit">
        <Toolbar>
          <div className={classes.container}>
            <ul className={classes.ul}>
            {/* <li><img src={props.logo} alt="oecci logo" width="50"/></li>
 */}              <li>{props.Accueil}</li>
              <li> <a href="#innovation" style={{color:"inherit",textDecoration:"none"}}>{props.Innovations}</a> </li>
              <li>
                <Link
                  to="/nous-contacter"
                  style={{
                    textDecorationLine: "none",
                    color: "inherit",
                  }}
                >
                  {props.Nous_contacter}
                </Link>
              </li>

              <li>
                <Link
                  to="/Login"
                  style={{
                    textDecorationLine: "none",
                    color: "inherit",
                  }}
                >
                  {props.bntSeConnecter}
                </Link>{" "}
              </li>
            </ul>
          </div>
          
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default NavBar;

NavBar.propTypes = {
  Accueil: PropTypes.string,
  Innivations: PropTypes.string,
  Nous_contacter: PropTypes.string,
  bntSeConnecter: PropTypes.string,
};
