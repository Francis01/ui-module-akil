import { makeStyles } from "@material-ui/core/styles";

export const UseStyleNavBar = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  container: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "row-reverse",
  },

  ul: {
    flexGrow: 0.2,
    display: "flex",
    justifyContent: "space-between",
    listStyle: "none",
    font:
      "var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-20)/26px Roboto Slab",
    color: "#757575",
    opacity: 1,
    alignItems: "center",
  },
}));
