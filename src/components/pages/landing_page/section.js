import { Grid } from '@material-ui/core';
import React from 'react';
import capture from '../../../assert/capture.png';
import capture2 from '../../../assert/capture2.png';
import capture3 from '../../../assert/capture3.png';
import {UseStylesSection} from './section.style';
import TestDeFonctionnalite from '../../../components/layouts/boutton/TestDeFonctionnalite';
import Declaration  from "../../../assert/landing-page/Groupe 13031.png";
import Arbitrage  from "../../../assert/landing-page/Groupe 13032.png";
import Authentification  from "../../../assert/landing-page/Groupe 13033.png";
import {Link} from "react-router-dom";

const Section = () =>{
    const classes = UseStylesSection()
    return(
        <div className={classes.root}>
            <Grid spacing={4} container className={classes.container} >
                <Grid item xs={6} className={classes.grid1}>
                    <img  src={Declaration} className={classes.image}/>
                </Grid>
                <Grid item xs={6} className={classes.grid2}>
                    <ul className={classes.ul}>
                        <li className={classes.t1}>homines quidam ignoti</li>
                        <li className={classes.t2}>Déclaration de dossier</li>
                        <li className={classes.t3}></li>
                        <li className={classes.t4 }>La fonctionnalité de déclaration de dossier permet à un expert comptable de déclarer les contribuables qu’il a à charge. Ce dernier a le choix entre faire une déclaration unitaire s’il n’a qu’un contribuable à déclarer ou faire une déclaration en masse qui consiste à importer un fichier au format excel contenant la liste des contribuables à charge. Ce fichier doit impérativement respecter le format spécifié. Un modèle téléchargeable via un bouton prévu à cet effet lui est mis à disposition dans le système.</li>
                        <li className={classes.t5}> <Link to="/login" style={{color:"inherit",textDecoration:"none"}}><TestDeFonctionnalite/> </Link> </li>
                    </ul>
                </Grid>
            </Grid>

            <Grid spacing={4} container className={classes.container} >
                
                <Grid item xs={6} className={classes.grid2}>
                    <ul className={classes.ul}>
                        <li className={classes.t1}>homines quidam ignoti</li>
                        <li className={classes.t2}>Gestion d'arbitrage</li>
                        <li className={classes.t3}></li>
                        <li className={classes.t4 }>L’arbitrage est une fonctionnalité mise en place pour la gestion des conflits sur les contribuables déclarés. En effet, lorsque deux experts comptables entrent en conflits sur un contribuable, l’un des deux peut initier une demande d’arbitrage auprès de l’administration de l’ordre afin que cette dernière puisse suivre la procédure interne définie et ainsi lever le conflit en attribuant le contribuable à qui de droit. Du point de vue de l’administrateur, cette fonctionnalité lui permet de voir la liste des demandes d’arbitrage et de les traiter (lever le conflit).</li>
                        <li className={classes.t5}> <Link to="/login" style={{color:"inherit",textDecoration:"none"}}><TestDeFonctionnalite/> </Link> </li>
                    </ul>
                </Grid>

                <Grid item xs={6} className={classes.grid1}>
                    <img src={Arbitrage} alt=' Arbitrage image' className={classes.image}/>
                </Grid>

            </Grid>


            <Grid spacing={4} container className={classes.container} >

                <Grid item xs={6} className={classes.grid1}>
                    <img src={Authentification} alt='Authentification image' className={classes.image}/>
                </Grid>
                
                <Grid item xs={6} className={classes.grid2}>
                    <ul className={classes.ul}>
                        <li className={classes.t1}>homines quidam ignoti</li>
                        <li className={classes.t2}>Authentification des états financiers</li>
                        <li className={classes.t3}></li>
                        <li className={classes.t4 }>Cette fonctionnalité permet de s’assurer que les états financiers de ses contribuables qui seront transmis à la direction générale des impôts ont bien été révisés et validés par un expert comptable inscrit au tableau de l’ordre des experts-comptables et ainsi éliminer les cas de fraude. Cette assurance est matérialisée par une signature électronique que l’expert-comptable appose sur les états financiers de ces contribuables.</li>
                        <li className={classes.t5}>  <Link to="/login" style={{color:"inherit",textDecoration:"none"}}><TestDeFonctionnalite/></Link> </li>
                    </ul>
                </Grid>

            </Grid>

        </div>
    )
}

export default Section;