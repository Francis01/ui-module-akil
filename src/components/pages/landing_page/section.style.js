import { makeStyles } from "@material-ui/core/styles";

export const UseStylesSection = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: "flex",
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: "auto",
    width: "100%",
    marginTop: theme.spacing(4),
    justifyContent: "center",
    alignContent: "center",
  },
  grid1: {
    textAlign: "center",
    width: "70%",
  },
  grid2: {
    textAlign: "center",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  ul: {
    display: "flex",
    flexDirection: "column",
    listStyle: "none",
    textDecoration: "none",
    justifyContent: "space-between",
    height: "80%",
    textAlign: "start",
    width: "80%",
  },
  t1: {
    color: "#FF7A42",
    fontSize: "11px",
  },
  t2: {
    color: "#909090",
    fontSize: "30px",
    fontWeight: "bold",
    width: "40%",
    margin: "1em 0px 0.5em 0px",
  },
  t3: {
    width: "6%",
    background: "#FF7A42",
    height: "1%",
  },
  t4: {
    color: "#909090",
    fontSize: "15px",
    width: "90%",
  },
  t5: {
    marginTop: theme.spacing(2),
  },
}));
