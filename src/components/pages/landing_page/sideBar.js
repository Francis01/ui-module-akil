import React from "react";
import { UseStylesSideBar } from "./sideBar.style";
import { Grid } from "@material-ui/core";
import Groupe30 from "../../../assert/Groupe 30.png";
import Groupe from "../../../assert/Groupe.png";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import SideBarChild from "./sideBarChild";
import Controle  from "../../../assert/landing-page/Groupe 30.png";
import Fiabilite  from "../../../assert/landing-page/Groupe 28.png";
import Mobilite  from "../../../assert/landing-page/Groupe 29.png";
const SideBar = () => {
  const classes = UseStylesSideBar();
  const title1 = "Contrôle et Décisionnelle";
  const contente1 =
    "La plateforme est un excellent outil permettant à l’ordre de mener à bien ses missions. En effet, elle lui offre des fonctionnalités à la fois puissantes et très intuitives lui permettant d’assurer le respect de ses règles établies et de prendre les  décisions favorables à la profession.";
  const title2 = "Fiabilité et Securité";
  const title3 = "Mobilité et Efficacité";

  return (
    <div className={classes.root} id="innovation">
      <div className={classes.containerDiv1}>
        <Grid spacing={4} className={classes.gridRoot}>
          <Grid item xs={12}>
            <SideBarChild
              icon={Controle}
              title1={title1}
              contente1={contente1}
            />
          </Grid>

          <Grid item xs={12}>
            <SideBarChild
              icon={Fiabilite}
              title1={title2}
              contente1={contente1}
            />
          </Grid>

          <Grid item xs={12}>
            <SideBarChild
              icon={Mobilite}
              title1={title3}
              contente1={contente1}
            />
          </Grid>
        </Grid>
      </div>

      <Grid container className={classes.innov}>
        <Grid xs={12} item>
          <div className={classes.innovTitle}>Innovations</div>
          <div className={classes.innovContent}>
            Hae duae provinciae bello quondam piratico catervis mixtae praedonum
            a Servilio pro consule missae sub iugum factae sunt vectigales. et
            hae quidem regiones velut in prominenti terrarum lingua positae ob
            orbe eoo monte Amano disparantur.
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default SideBar;
