import { makeStyles } from "@material-ui/core/styles";

export const UseStylesSideBar = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  containerDiv1: {
    width: "90%",
    height: "517px",
    boxShadow: "3px 0px 55px #0000000F",
    border: "1px solid #0000000F",
    marginLeft: "auto",
    marginRight: "auto",
    transform: `translateY(-50px)`,
    backgroundColor: "#fff",
  },
  gridRoot: {
    display: "flex",
    flexDirection: "row",
    marginTop: theme.spacing(4),
    marginLeft: "auto",
    marginRight: "auto",
  },
  innov: {
    textAlign: "center",
    marginTop: "2%",
    width: "50%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  innovTitle: {
    color: "#FF7A42",
    fontSize: "40px",
    fontWeight: "bold",
  },

  innovContent: {
    color: "#909090",
    fontSize: "15px",
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignContent:"center",
    alignItems:"center",
    width:"50%",
    margin:"auto"
  },
}));
