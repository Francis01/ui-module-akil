import { Grid } from '@material-ui/core';
import React from 'react';
import {UseStylesSideBarChild} from './sideBarChild.style';

    const SideBarChild=(props)=>{       
    const classes=UseStylesSideBarChild()
    return(
        <div className={classes.root} id="innovation">
            <Grid spacing={2}>
                
                        <div className={classes.img}><img src={props.icon}/></div>
                        <div className={classes.title}>{props.title1}</div>
                        <div className={classes.contente}>{props.contente1}</div>
            </Grid>
           

        </div>
    )
}

export default SideBarChild;