import { makeStyles } from '@material-ui/core/styles';
export const UseStylesSideBarChild=makeStyles((theme)=>({
    root:{
        flexGrow:1,
    
    },
    title:{
        color:'#909090',
        fontSize:'30px',
        fontWeight:'bold',
        width:'80%',
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:theme.spacing(2)
        
    },
    contente:{
        color:'#909090',
        fontSize:'15px',
        width:'80%',
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:theme.spacing(2)
    },
    img:{
    }

}))